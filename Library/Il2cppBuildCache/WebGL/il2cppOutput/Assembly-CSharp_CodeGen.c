﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AudioManager::Awake()
extern void AudioManager_Awake_mD7873A38A3ED577A313A05D24BF6511E59EDEC01 (void);
// 0x00000002 System.Void AudioManager::Start()
extern void AudioManager_Start_m54C0A7ACBAB2F38052C6B900BBBC3261339662FC (void);
// 0x00000003 System.Void AudioManager::Play(System.String)
extern void AudioManager_Play_mABE51D919CA26C8386CF6823417ECC0CD9000888 (void);
// 0x00000004 System.Void AudioManager::.ctor()
extern void AudioManager__ctor_m6C686441D1A1A223E4CF940A8EB0128535D603BD (void);
// 0x00000005 System.Void AudioManager/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m67B1BB5E29B49389EC0244292A697D9F0DA398C5 (void);
// 0x00000006 System.Boolean AudioManager/<>c__DisplayClass4_0::<Play>b__0(Sound)
extern void U3CU3Ec__DisplayClass4_0_U3CPlayU3Eb__0_m3638A911493D9A1EDF26B6B317DFE476AE17D16E (void);
// 0x00000007 System.Void npc::Start()
extern void npc_Start_mB1AFDA23363F546579A7E6873323A2D5C63B2126 (void);
// 0x00000008 System.Void npc::FixedUpdate()
extern void npc_FixedUpdate_mE97A4988AD6BD8AC38E6726020DE31AD1178E7A5 (void);
// 0x00000009 System.Void npc::Update()
extern void npc_Update_mC570B39F9953A22E3167F3A8F615F01682DD0F46 (void);
// 0x0000000A System.Void npc::CheckDistance()
extern void npc_CheckDistance_mDF6325952BE7354D41949D4F13F7A10F59E9DDDA (void);
// 0x0000000B System.Void npc::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void npc_OnTriggerEnter2D_mBF94814C116B513A9404C9A3F1845023DC3E742F (void);
// 0x0000000C System.Void npc::OnTriggerExit2D(UnityEngine.Collider2D)
extern void npc_OnTriggerExit2D_mFE6208B9D9419D9F566A533CF8A55AC809CDFA52 (void);
// 0x0000000D System.Void npc::TakeDamage(System.Int32)
extern void npc_TakeDamage_m5C3CCE6D6DAC8954A2E1E8270228816AC2D1C97B (void);
// 0x0000000E System.Void npc::Die()
extern void npc_Die_m7036D9B976055CEE21A68761C4840806336291D6 (void);
// 0x0000000F System.Void npc::.ctor()
extern void npc__ctor_mB9257C7E06300217C2CD78885E1E919DE6853075 (void);
// 0x00000010 System.Void PlayerMovement::Start()
extern void PlayerMovement_Start_mB585552228B1908E44D3A69496598FB485F608B6 (void);
// 0x00000011 System.Void PlayerMovement::Update()
extern void PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F (void);
// 0x00000012 System.Boolean PlayerMovement::isRestrictedState(GenericState)
extern void PlayerMovement_isRestrictedState_m0EA55B08E8A96C443BC31C0AC59A788A59508A4D (void);
// 0x00000013 System.Void PlayerMovement::SetState(GenericState)
extern void PlayerMovement_SetState_m64E859277CA239A4A4373B7FDEDD83A7D37909F8 (void);
// 0x00000014 System.Void PlayerMovement::Knock(System.Single,System.Single)
extern void PlayerMovement_Knock_mE574231AEE300149E504E871824DEEFABD785571 (void);
// 0x00000015 System.Void PlayerMovement::GetInput()
extern void PlayerMovement_GetInput_m96688F49609587CAE67CE5C36BA60AF2E0B935F5 (void);
// 0x00000016 System.Void PlayerMovement::SetAnimation()
extern void PlayerMovement_SetAnimation_m14F76EDA251DADFBEDBF145D29FBA6F3A0047458 (void);
// 0x00000017 System.Collections.IEnumerator PlayerMovement::WeaponCo()
extern void PlayerMovement_WeaponCo_m7E38E79787A651A1F512F9E0CF42E98A2398D396 (void);
// 0x00000018 System.Collections.IEnumerator PlayerMovement::AbilityCo(System.Single)
extern void PlayerMovement_AbilityCo_mB527E62AFDABEA91E50C6841F440F178C87D971D (void);
// 0x00000019 System.Collections.IEnumerator PlayerMovement::Knockco(System.Single)
extern void PlayerMovement_Knockco_m9FB0A3F5655B26872CF8F08DE922AAC0BA2CD9AB (void);
// 0x0000001A System.Collections.IEnumerator PlayerMovement::SecondAttackCo()
extern void PlayerMovement_SecondAttackCo_m35F4438A058472BE31F0537E3AA967A0F8E26349 (void);
// 0x0000001B System.Void PlayerMovement::MakeArrow()
extern void PlayerMovement_MakeArrow_m6C032DB935BA42EA2519E406D324D47F60CEB830 (void);
// 0x0000001C UnityEngine.Vector3 PlayerMovement::ChooseArrowDirection()
extern void PlayerMovement_ChooseArrowDirection_m55B2A6CDA1F6C27935AF11343EC1ECBFF7D87A97 (void);
// 0x0000001D System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F (void);
// 0x0000001E System.Void PlayerMovement/<WeaponCo>d__19::.ctor(System.Int32)
extern void U3CWeaponCoU3Ed__19__ctor_mF6774EDF96E29D4CF1DE6E59A385EF4733577499 (void);
// 0x0000001F System.Void PlayerMovement/<WeaponCo>d__19::System.IDisposable.Dispose()
extern void U3CWeaponCoU3Ed__19_System_IDisposable_Dispose_m336B4209188172D5D2E7DC91BA51B13E1B53AD56 (void);
// 0x00000020 System.Boolean PlayerMovement/<WeaponCo>d__19::MoveNext()
extern void U3CWeaponCoU3Ed__19_MoveNext_m86432A4AEB6DF8EFA0E5152202A29EB30998805B (void);
// 0x00000021 System.Object PlayerMovement/<WeaponCo>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWeaponCoU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE75159A118D1CCD0D21C44DA7A1BCCEFEED035B (void);
// 0x00000022 System.Void PlayerMovement/<WeaponCo>d__19::System.Collections.IEnumerator.Reset()
extern void U3CWeaponCoU3Ed__19_System_Collections_IEnumerator_Reset_m92D841A85EA7636FE60CE098FB0E509F112F57D3 (void);
// 0x00000023 System.Object PlayerMovement/<WeaponCo>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CWeaponCoU3Ed__19_System_Collections_IEnumerator_get_Current_mFA45A158917C1D00D0414546D330B5C50AF98810 (void);
// 0x00000024 System.Void PlayerMovement/<AbilityCo>d__20::.ctor(System.Int32)
extern void U3CAbilityCoU3Ed__20__ctor_m0176682625B409EBAF4204EB4659BDF4D41F4BA5 (void);
// 0x00000025 System.Void PlayerMovement/<AbilityCo>d__20::System.IDisposable.Dispose()
extern void U3CAbilityCoU3Ed__20_System_IDisposable_Dispose_m7077FA91B9383678A31E38756736A526CF59A3FC (void);
// 0x00000026 System.Boolean PlayerMovement/<AbilityCo>d__20::MoveNext()
extern void U3CAbilityCoU3Ed__20_MoveNext_mF7EE2A4FB49F66985E71CC997FC5CD96E78EE708 (void);
// 0x00000027 System.Object PlayerMovement/<AbilityCo>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAbilityCoU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1B45335C8F68A00353045AFA6267AE71F07182E1 (void);
// 0x00000028 System.Void PlayerMovement/<AbilityCo>d__20::System.Collections.IEnumerator.Reset()
extern void U3CAbilityCoU3Ed__20_System_Collections_IEnumerator_Reset_m8E3B2891A317FB08FA139BD950964EAD077B40C2 (void);
// 0x00000029 System.Object PlayerMovement/<AbilityCo>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CAbilityCoU3Ed__20_System_Collections_IEnumerator_get_Current_mE6CEEBAB1AB8EE0C1A7052E7E832277A232B1DD5 (void);
// 0x0000002A System.Void PlayerMovement/<Knockco>d__21::.ctor(System.Int32)
extern void U3CKnockcoU3Ed__21__ctor_m0683E9332889422FD23C62E94DCA9758C00552BA (void);
// 0x0000002B System.Void PlayerMovement/<Knockco>d__21::System.IDisposable.Dispose()
extern void U3CKnockcoU3Ed__21_System_IDisposable_Dispose_m3209EEDB656EB5486A1CF3DA9F24C350C4707499 (void);
// 0x0000002C System.Boolean PlayerMovement/<Knockco>d__21::MoveNext()
extern void U3CKnockcoU3Ed__21_MoveNext_m6021B528770FB5799F99505541DB9994402149B0 (void);
// 0x0000002D System.Object PlayerMovement/<Knockco>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKnockcoU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF02042846E4388A7D23AEE7B092211A67F45B9B (void);
// 0x0000002E System.Void PlayerMovement/<Knockco>d__21::System.Collections.IEnumerator.Reset()
extern void U3CKnockcoU3Ed__21_System_Collections_IEnumerator_Reset_mFB645C76CFD1ECF92A92F52F985A9D3459F302A5 (void);
// 0x0000002F System.Object PlayerMovement/<Knockco>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CKnockcoU3Ed__21_System_Collections_IEnumerator_get_Current_m54FA11747AF9038FFEE415DA8CB553F911104081 (void);
// 0x00000030 System.Void PlayerMovement/<SecondAttackCo>d__22::.ctor(System.Int32)
extern void U3CSecondAttackCoU3Ed__22__ctor_mF3BB48058B987D2323D3877A4B73BB38EF8FCA12 (void);
// 0x00000031 System.Void PlayerMovement/<SecondAttackCo>d__22::System.IDisposable.Dispose()
extern void U3CSecondAttackCoU3Ed__22_System_IDisposable_Dispose_mD085A289E795223BE81314ED9425C51047B43990 (void);
// 0x00000032 System.Boolean PlayerMovement/<SecondAttackCo>d__22::MoveNext()
extern void U3CSecondAttackCoU3Ed__22_MoveNext_mB407F42AA9D9E69717C17283F5017F368D8AC39A (void);
// 0x00000033 System.Object PlayerMovement/<SecondAttackCo>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSecondAttackCoU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m566C8E4E0AABFF749D23CB669FA3AE323A002871 (void);
// 0x00000034 System.Void PlayerMovement/<SecondAttackCo>d__22::System.Collections.IEnumerator.Reset()
extern void U3CSecondAttackCoU3Ed__22_System_Collections_IEnumerator_Reset_m6FAADB162A5C47DAB3951676C0EE2937468C2E00 (void);
// 0x00000035 System.Object PlayerMovement/<SecondAttackCo>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CSecondAttackCoU3Ed__22_System_Collections_IEnumerator_get_Current_m23AC4D1FAA25D82B0CE7ACFC49A43B7DC624AD7D (void);
// 0x00000036 System.Void roomTransfer::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void roomTransfer_OnTriggerEnter2D_mBE37A0ABB0DA00B385750A3D9A5DCFDFCC97CCE7 (void);
// 0x00000037 System.Void roomTransfer::.ctor()
extern void roomTransfer__ctor_mD18C81B18780C9B5D6FE088B56CA0D1388DAB9C3 (void);
// 0x00000038 System.Void CameraMovement::Start()
extern void CameraMovement_Start_mCBBF91D02D609BDE618205B77F07B1FFD53C0A41 (void);
// 0x00000039 System.Void CameraMovement::FixedUpdate()
extern void CameraMovement_FixedUpdate_mE86FEA62BC2F22AE9C4399706D27E422D2B0F95F (void);
// 0x0000003A System.Void CameraMovement::.ctor()
extern void CameraMovement__ctor_mD0F05084B2475AA8C0A35AFB6E4C88D0D6ACEAAA (void);
// 0x0000003B System.Void EnemyAI::Start()
extern void EnemyAI_Start_mC5494C97D0214D04302BAE4F82F48D13AD0D25BE (void);
// 0x0000003C System.Void EnemyAI::Update()
extern void EnemyAI_Update_mBB23B2D10604094D0D5BDD6D02A5C51DFCDFAA84 (void);
// 0x0000003D System.Void EnemyAI::FixedUpdate()
extern void EnemyAI_FixedUpdate_m06285F83619449289F3118F098D1DC94D8E7BAC3 (void);
// 0x0000003E System.Void EnemyAI::MoveCharacter(UnityEngine.Vector2)
extern void EnemyAI_MoveCharacter_mBDD642AA326776490AB68E704664A26DCED9A3E1 (void);
// 0x0000003F System.Void EnemyAI::.ctor()
extern void EnemyAI__ctor_mAF4FEC29EA4ADF864B4641448BFF55028ED4B3BC (void);
// 0x00000040 System.Void ManagerAudio::Start()
extern void ManagerAudio_Start_m58F53742043C6FB113A6FDBEF9AA8F695D5EA63C (void);
// 0x00000041 System.Void ManagerAudio::Update()
extern void ManagerAudio_Update_mEA1A1FD2AFE74F2DBDCDA1D97F2EC9CF71B3A0C2 (void);
// 0x00000042 System.Void ManagerAudio::.ctor()
extern void ManagerAudio__ctor_mBF0B8444CF326C468C1FD173AFB4445AED18CAA6 (void);
// 0x00000043 System.Void musicPlayer::OnCollisionEnter(UnityEngine.Collision)
extern void musicPlayer_OnCollisionEnter_m49EDD18644A75557DFF41063B174A6627B7C4491 (void);
// 0x00000044 System.Void musicPlayer::Update()
extern void musicPlayer_Update_m69D765DF35CF448EB8F53194E8DDA7A91379833A (void);
// 0x00000045 System.Void musicPlayer::.ctor()
extern void musicPlayer__ctor_m18CFE91C5BA1C322A4167D8AF57CC2328D047277 (void);
// 0x00000046 System.Void Sign::Start()
extern void Sign_Start_m28ACDB82DAFDB83009226314C5465C0581405830 (void);
// 0x00000047 System.Void Sign::Update()
extern void Sign_Update_mA0CA73670A2FEB8087F4A501CE3C717B26D35ADF (void);
// 0x00000048 System.Void Sign::Text()
extern void Sign_Text_m41A10CC054BB0346C4914062E4140A5A530768A6 (void);
// 0x00000049 System.Void Sign::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Sign_OnTriggerEnter2D_m5D5FF2EC7F3464DF2F203241888C63D312A4856D (void);
// 0x0000004A System.Void Sign::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Sign_OnTriggerExit2D_mB5C708ED74620FA94EAC25117C91327AE61840E1 (void);
// 0x0000004B System.Void Sign::.ctor()
extern void Sign__ctor_m348D33DD7033FBC7F73A77F8094FB8CCE1B1A2C2 (void);
// 0x0000004C System.Void Sound::.ctor()
extern void Sound__ctor_mEA0B0D2FBD514F91C21900B0BB8679CD78843FCD (void);
// 0x0000004D System.Void Text::.ctor()
extern void Text__ctor_m509388F43D08C8B93FED709EFC4DE8A9AFA57CA3 (void);
// 0x0000004E System.Void TextManager::Awake()
extern void TextManager_Awake_m2A88556742B867A4738A44434F4744B380CFD969 (void);
// 0x0000004F System.Void TextManager::Update()
extern void TextManager_Update_mAA5D92CFBF49490445ACC3178D4EAA55B29CC51F (void);
// 0x00000050 System.Void TextManager::.ctor()
extern void TextManager__ctor_mAA951AC010A74DD21641D5263C45DF7969B419AB (void);
// 0x00000051 System.Void TwoTextDialogue::Start()
extern void TwoTextDialogue_Start_mCE142EF7B83E90382C14BF4CE46BD7B36EF2F57C (void);
// 0x00000052 System.Void TwoTextDialogue::Update()
extern void TwoTextDialogue_Update_m74B8C9FE08AFFAB4B9AE3BBD20AFDDA1C269A39B (void);
// 0x00000053 System.Void TwoTextDialogue::Text()
extern void TwoTextDialogue_Text_mA491C82512C0DE8B6F1463CCB4EF9113ACCFD374 (void);
// 0x00000054 System.Void TwoTextDialogue::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void TwoTextDialogue_OnTriggerEnter2D_mF330CE95BF48C46E8BBAED6CEA6D08983C288401 (void);
// 0x00000055 System.Void TwoTextDialogue::OnTriggerExit2D(UnityEngine.Collider2D)
extern void TwoTextDialogue_OnTriggerExit2D_mEF9A90E1A34E79C1496E78F2D834EF3529985D27 (void);
// 0x00000056 System.Void TwoTextDialogue::.ctor()
extern void TwoTextDialogue__ctor_m77C5C9C18BA9FF5660C3BB514F166E618D612D02 (void);
// 0x00000057 System.Void Enemy::Awake()
extern void Enemy_Awake_mF268033197059561A4A0BC3E5F6B83B50D29C861 (void);
// 0x00000058 System.Void Enemy::TakeDamage(System.Single)
extern void Enemy_TakeDamage_mD2F5D25BECC021BB923DE3FF0DAE69800DA36463 (void);
// 0x00000059 System.Void Enemy::Knock(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern void Enemy_Knock_m37EA280E95C2E2F6E6D83080C96235C6404A2278 (void);
// 0x0000005A System.Collections.IEnumerator Enemy::Knockco(UnityEngine.Rigidbody2D,System.Single)
extern void Enemy_Knockco_mD90ED67E0DA4A148DE3CB5FA33EB2187AAFB2F7A (void);
// 0x0000005B System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x0000005C System.Void Enemy/<Knockco>d__9::.ctor(System.Int32)
extern void U3CKnockcoU3Ed__9__ctor_m2F874F51BAFBD6E0A502C95F49D0FDE4B00B3596 (void);
// 0x0000005D System.Void Enemy/<Knockco>d__9::System.IDisposable.Dispose()
extern void U3CKnockcoU3Ed__9_System_IDisposable_Dispose_mA9E234FEC09DB70E40A923A8EE315EE24373BBC3 (void);
// 0x0000005E System.Boolean Enemy/<Knockco>d__9::MoveNext()
extern void U3CKnockcoU3Ed__9_MoveNext_mF71F4401A13B5EFCC353D099092613955C94480A (void);
// 0x0000005F System.Object Enemy/<Knockco>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKnockcoU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC2DEAF9142B8BC177D269B901C8ACE2F35A5B64 (void);
// 0x00000060 System.Void Enemy/<Knockco>d__9::System.Collections.IEnumerator.Reset()
extern void U3CKnockcoU3Ed__9_System_Collections_IEnumerator_Reset_mB44AF6F2AE44CC8C9F91C86FFEF604A906770458 (void);
// 0x00000061 System.Object Enemy/<Knockco>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CKnockcoU3Ed__9_System_Collections_IEnumerator_get_Current_m3412A2D0FB64EECBA79C5949DCBD517BE46F9176 (void);
// 0x00000062 System.Void EnemyHealth::Damage(System.Int32)
extern void EnemyHealth_Damage_m16925CD39AEBFCF4C5C623935C47AB480ACC8D2D (void);
// 0x00000063 System.Void EnemyHealth::Start()
extern void EnemyHealth_Start_mD01F3D64A408764E38262C86167BA12B65D4A5D7 (void);
// 0x00000064 System.Void EnemyHealth::Update()
extern void EnemyHealth_Update_mBCBBEB935216A5E61B4F9A5F6833A09301C0E8DF (void);
// 0x00000065 System.Void EnemyHealth::Die()
extern void EnemyHealth_Die_mA8BDF6A041B21ADF726398BF92C35859405C552F (void);
// 0x00000066 System.Void EnemyHealth::.ctor()
extern void EnemyHealth__ctor_mF9FFC7A91A2AB12182655557BC05309E64E17AFE (void);
// 0x00000067 System.Void SimpleFollow::Start()
extern void SimpleFollow_Start_m849B7D8CBBA638D25272E9E9B12F7B36F50E4081 (void);
// 0x00000068 System.Void SimpleFollow::FixedUpdate()
extern void SimpleFollow_FixedUpdate_mCC8B6C95B31AC422B9737B2F1A13EB60B11FA9F7 (void);
// 0x00000069 System.Void SimpleFollow::OnDrawGizmos()
extern void SimpleFollow_OnDrawGizmos_m7298DDF5ED22E11AC91577F12B7970CDB2E49208 (void);
// 0x0000006A System.Void SimpleFollow::.ctor()
extern void SimpleFollow__ctor_mA4FB9BF2B0FA9E339FFAD7D397CE729582098BE0 (void);
// 0x0000006B System.Void Slime::Start()
extern void Slime_Start_m2E8A8239589581EEEA23A4C58CD61D7E7670CCD3 (void);
// 0x0000006C System.Void Slime::FixedUpdate()
extern void Slime_FixedUpdate_m8F95CD034596F164A1282C83973D5A2A9C2ECEE8 (void);
// 0x0000006D System.Void Slime::CheckDistance()
extern void Slime_CheckDistance_m5C6C5AFADDF51C43BF457D8DD1BFD3693318722F (void);
// 0x0000006E System.Void Slime::ChangeState(EnemyState)
extern void Slime_ChangeState_m7CF151F1F99A0527A15F0AC51116B20504F2C13F (void);
// 0x0000006F System.Void Slime::.ctor()
extern void Slime__ctor_m24052CCA4BC1BDA15F470CFA7CDC7B83EC726950 (void);
// 0x00000070 System.Void RoomTransfer::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void RoomTransfer_OnTriggerEnter2D_mA20E0353AFDE2F43936A15B606B1E97DF4692E48 (void);
// 0x00000071 System.Void RoomTransfer::LoadRoom()
extern void RoomTransfer_LoadRoom_mAAA9AF0BB9701FE7405EE6DDD59C9742463328D5 (void);
// 0x00000072 System.Void RoomTransfer::.ctor()
extern void RoomTransfer__ctor_m5C6193E95547CA97DFE50BEE194447D7EFCCB04D (void);
// 0x00000073 System.Void SignalListener::OnSignalRaised()
extern void SignalListener_OnSignalRaised_m1B74919A09DAFBA432D8A9C4E509DF3601FF5A13 (void);
// 0x00000074 System.Void SignalListener::OnEnable()
extern void SignalListener_OnEnable_m45048A70F0BE1DB10FDF07822E3C134CF67AEC1D (void);
// 0x00000075 System.Void SignalListener::OnDisable()
extern void SignalListener_OnDisable_m9AA18CB6C74989EFB0E1CDAF4F376A5A48C0A607 (void);
// 0x00000076 System.Void SignalListener::.ctor()
extern void SignalListener__ctor_m5138552B7DDD4913E17A3EE1FF6BF6FFC870B506 (void);
// 0x00000077 System.Void SOObserver::Awake()
extern void SOObserver_Awake_mD1AC644B102556C6B62DF8D5E3ABA78DF2A25D88 (void);
// 0x00000078 System.Void SOObserver::Update()
extern void SOObserver_Update_mD9AEB53FD048C6E4B61F0FC1EB7C2CB6E0829474 (void);
// 0x00000079 System.Void SOObserver::.ctor()
extern void SOObserver__ctor_m4B04E70165BD725C7387D95921FE08BB15C83499 (void);
// 0x0000007A System.Void AnimatorController::SetAnimParameter(System.String,System.Single)
extern void AnimatorController_SetAnimParameter_mC2C9C3AE4E5DA433CA1956DF1C4F665028F25010 (void);
// 0x0000007B System.Void AnimatorController::SetAnimParameter(System.String,System.Boolean)
extern void AnimatorController_SetAnimParameter_m1FD40940C21357C4297966166AD072DF06523572 (void);
// 0x0000007C System.Single AnimatorController::GetAnimFloat(System.String)
extern void AnimatorController_GetAnimFloat_m9F2B9538A13A88B77064AED89A279DEA23F59E18 (void);
// 0x0000007D System.Boolean AnimatorController::GetAnimBool(System.String)
extern void AnimatorController_GetAnimBool_m0E2BA364EAF255DC62470D05770500EDA7981D27 (void);
// 0x0000007E System.Void AnimatorController::.ctor()
extern void AnimatorController__ctor_m1E1680F05697619E47C9C78239B71FF981B4B85D (void);
// 0x0000007F System.Void Damage::ApplyDamage(Health,System.Int32)
extern void Damage_ApplyDamage_mD732480DDBBBC4F6FF50E444D2AF00517B31A1DD (void);
// 0x00000080 System.Void Damage::.ctor()
extern void Damage__ctor_m448736D790344E455BBE20282D3D73C91E6E2CAB (void);
// 0x00000081 System.Void DamageOnContact::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void DamageOnContact_OnTriggerEnter2D_m5483A2905317AC29E8F0BA39C9987EF9742A2061 (void);
// 0x00000082 System.Void DamageOnContact::.ctor()
extern void DamageOnContact__ctor_m287E9AFB1CD7DCA3E55E99B98A4E9989FAAD8AD1 (void);
// 0x00000083 System.Void DestroyOverTime::Update()
extern void DestroyOverTime_Update_m9BD3239A91EEC7E53820274885C47DEFFE663495 (void);
// 0x00000084 System.Void DestroyOverTime::.ctor()
extern void DestroyOverTime__ctor_mB8D8C27A30DE70D4609B2EF8B28E7C8C99EF7C14 (void);
// 0x00000085 System.Void DisableColliderForFixedTime::Start()
extern void DisableColliderForFixedTime_Start_m827F0060D32EB6D08169463022C88155F3A5EFBA (void);
// 0x00000086 System.Void DisableColliderForFixedTime::Update()
extern void DisableColliderForFixedTime_Update_m3538F964283ECE3D2D58BEF05BE1DC13E2033CAC (void);
// 0x00000087 System.Void DisableColliderForFixedTime::.ctor()
extern void DisableColliderForFixedTime__ctor_mA35F9E34BD782E00758317CA9A552E5E515BD33C (void);
// 0x00000088 System.Void FlashColor::StartFlash()
extern void FlashColor_StartFlash_m2C05E76FEF04171D863CE55398C4177B39081D4E (void);
// 0x00000089 System.Collections.IEnumerator FlashColor::FlashCo()
extern void FlashColor_FlashCo_m07901743C6FAEF83D1AB61EF87A1D1AC9BB8B387 (void);
// 0x0000008A System.Void FlashColor::.ctor()
extern void FlashColor__ctor_m0C58F482DF86F2C859420C213E24C2B4E9DEA9F1 (void);
// 0x0000008B System.Void FlashColor/<FlashCo>d__6::.ctor(System.Int32)
extern void U3CFlashCoU3Ed__6__ctor_m282505D8A6C582DC83B04A78C33D051B74AB39E1 (void);
// 0x0000008C System.Void FlashColor/<FlashCo>d__6::System.IDisposable.Dispose()
extern void U3CFlashCoU3Ed__6_System_IDisposable_Dispose_m88C743246933E0B24A5BB46D1E89032441C80EA7 (void);
// 0x0000008D System.Boolean FlashColor/<FlashCo>d__6::MoveNext()
extern void U3CFlashCoU3Ed__6_MoveNext_mE8FA91D54BA581D5D1FD36750824E95996BF8F3D (void);
// 0x0000008E System.Object FlashColor/<FlashCo>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFlashCoU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m842202646B2172656DBB62CC371E6D6665ADD6A5 (void);
// 0x0000008F System.Void FlashColor/<FlashCo>d__6::System.Collections.IEnumerator.Reset()
extern void U3CFlashCoU3Ed__6_System_Collections_IEnumerator_Reset_m4AC4B01410D3911850D0217901498DB1FD07A134 (void);
// 0x00000090 System.Object FlashColor/<FlashCo>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CFlashCoU3Ed__6_System_Collections_IEnumerator_get_Current_mC61263B1374CD2BE6E68CBC0148D574A19638B3E (void);
// 0x00000091 System.Void Health::SetHealth(System.Int32)
extern void Health_SetHealth_m1707169CFAD418E9F05096B773A75243DAE6CC5A (void);
// 0x00000092 System.Void Health::Damage(System.Int32)
extern void Health_Damage_m54CDE5CB048F523E73F8AAA8D20EFC6135EA6112 (void);
// 0x00000093 System.Void Health::Heal(System.Int32)
extern void Health_Heal_mEB16314F9C9C09E62B8BAFBFAB5A8CAF35F6284D (void);
// 0x00000094 System.Void Health::Kill()
extern void Health_Kill_m2FA3BE3A4FBAC904F1E6339223571D2B39BA3479 (void);
// 0x00000095 System.Void Health::FullHeal()
extern void Health_FullHeal_m65B1B1106C909C1EB5BFEA559CABC33A62C0EDF2 (void);
// 0x00000096 System.Void Health::.ctor()
extern void Health__ctor_mD50C73D87211EAE260B38B2936F01722E31B9416 (void);
// 0x00000097 System.Void Interactable::Start()
extern void Interactable_Start_mCD3DB76ECF5B01A9E9AE400DFC3CA7ED95B89844 (void);
// 0x00000098 System.Void Interactable::Update()
extern void Interactable_Update_m17566E103461BED9EAC7E69D2B6BF5396FCD392A (void);
// 0x00000099 System.Void Interactable::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Interactable_OnTriggerEnter2D_m404A970A7992481E01319B79EEE8B957A43A4CC8 (void);
// 0x0000009A System.Void Interactable::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Interactable_OnTriggerExit2D_m84E116A69EEEA06B7846C18D96F11A80403FEE0F (void);
// 0x0000009B System.Void Interactable::.ctor()
extern void Interactable__ctor_m14A127716816AD709C7F0004EEC27A89E8EAE8D3 (void);
// 0x0000009C System.Void Knockback::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Knockback_OnTriggerEnter2D_mF928BA361B80E1DE2D3146855D185F9F04B06D5C (void);
// 0x0000009D System.Void Knockback::OnTriggerEnter2D(UnityEngine.Collider)
extern void Knockback_OnTriggerEnter2D_m9642D05568467C647D0E961F355233E3676FF568 (void);
// 0x0000009E System.Void Knockback::.ctor()
extern void Knockback__ctor_m2B8B481F28FC4A33133BD970C8F4E86F809DFD40 (void);
// 0x0000009F System.Boolean Magic::CanUseMagic(System.Int32)
extern void Magic_CanUseMagic_mDAF8F3311138150B1D38DF83BF1D276ED7E69E3C (void);
// 0x000000A0 System.Void Magic::UseMagic(System.Int32)
extern void Magic_UseMagic_mF35835D040DFE07561DA069EDFE2AEA6820FA849 (void);
// 0x000000A1 System.Void Magic::UseAllMagic()
extern void Magic_UseAllMagic_m1F8E5492972F0AA0AD2578C936B1B0D9845DEA2C (void);
// 0x000000A2 System.Void Magic::FillMagic()
extern void Magic_FillMagic_m2C2D39EA8224F22050C35B486267A31C5CDE64AC (void);
// 0x000000A3 System.Void Magic::AddMagic(System.Int32)
extern void Magic_AddMagic_mD024C5774B28AF2B444A2626FBD3513E6D7AC008 (void);
// 0x000000A4 System.Void Magic::.ctor()
extern void Magic__ctor_m886D6B87C69D82E60EB1B81D082A87F220F13697 (void);
// 0x000000A5 System.Void Movement::Motion(UnityEngine.Vector2)
extern void Movement_Motion_m0F1ADF4AD143A45A80F9033D6C319E1002B06277 (void);
// 0x000000A6 System.Void Movement::.ctor()
extern void Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB (void);
// 0x000000A7 System.Void ResetToPosition::ResetPosition()
extern void ResetToPosition_ResetPosition_mD4AACB593379DF026517754BD2A197DE9067F7F7 (void);
// 0x000000A8 System.Void ResetToPosition::.ctor()
extern void ResetToPosition__ctor_mFE9F0AB787C347159391FBF38AB58A137C985EBA (void);
// 0x000000A9 System.Void StateMachine::ChangeState(GenericState)
extern void StateMachine_ChangeState_m44BB5B80BA266F2B761B2C76E19DABEC1A855628 (void);
// 0x000000AA System.Void StateMachine::.ctor()
extern void StateMachine__ctor_m386AC4CE9F753E4EF79C5C6AEDD6A01F8E696F86 (void);
// 0x000000AB System.Void Arrow::Start()
extern void Arrow_Start_mD5806C542999F8C5FAB6FE9CD4CC6CFF772277FC (void);
// 0x000000AC System.Void Arrow::Setup(UnityEngine.Vector2,UnityEngine.Vector3)
extern void Arrow_Setup_m17A3F0749031623EE7FDC13CACBB0C6A87C0FC8F (void);
// 0x000000AD System.Void Arrow::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Arrow_OnTriggerEnter2D_m10D1F66E752F11F60C8E464781BEBB7B861B8511 (void);
// 0x000000AE System.Void Arrow::.ctor()
extern void Arrow__ctor_m5142CD3440C6AF4782993D2409B9FD785577892C (void);
// 0x000000AF System.Void Chest::Start()
extern void Chest_Start_mA640C6F089CDEB1A6E0EE8F487A12BE16790D136 (void);
// 0x000000B0 System.Void Chest::Update()
extern void Chest_Update_m4FA8556D3034321A01625325CE1F0612CB333866 (void);
// 0x000000B1 System.Void Chest::DisplayContents()
extern void Chest_DisplayContents_m5EA75D812183C2E50A02A0B7E1556AA878F19DC2 (void);
// 0x000000B2 System.Void Chest::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Chest_OnTriggerEnter2D_mEB3B4F967EB4C6DCED8B9CD315B3370791CB1E90 (void);
// 0x000000B3 System.Void Chest::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Chest_OnTriggerExit2D_mBFD1A87076CF7A2BDB4626FFF761CF4991B4B0B4 (void);
// 0x000000B4 System.Void Chest::.ctor()
extern void Chest__ctor_m107B91B4F8435DE9E7C45BC7C0707B14BE3632D2 (void);
// 0x000000B5 System.Void DungeonRoom::Start()
extern void DungeonRoom_Start_m33E39953C7E1887D67864AEC93CB2832F62A8185 (void);
// 0x000000B6 System.Void DungeonRoom::Update()
extern void DungeonRoom_Update_mAC54F9FA7061DA02F3436777C000E9C765566D0F (void);
// 0x000000B7 System.Void DungeonRoom::.ctor()
extern void DungeonRoom__ctor_mF5CC98038DC803795AA6F4E6E8695C3AB5817878 (void);
// 0x000000B8 System.Void LockedDoor::Start()
extern void LockedDoor_Start_mF9E231D813384C7B593A8DE7A9232C68A90A5B80 (void);
// 0x000000B9 System.Void LockedDoor::Update()
extern void LockedDoor_Update_mD37290D1B43BEE1A0150AABEB80E85B73B161B1C (void);
// 0x000000BA System.Boolean LockedDoor::PlayerHasKey()
extern void LockedDoor_PlayerHasKey_m099ADFA2363CD73DE88FEB456752377FC5616390 (void);
// 0x000000BB System.Void LockedDoor::Close()
extern void LockedDoor_Close_mAFA89F895038F685544D6332FB1930FF164D71D8 (void);
// 0x000000BC System.Void LockedDoor::Open()
extern void LockedDoor_Open_mBF053DF9B651BD4C0361D96947279E903E332256 (void);
// 0x000000BD System.Void LockedDoor::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void LockedDoor_OnTriggerEnter2D_m5A50B7A13897A610C54FAC1E3A4581161F0DC66E (void);
// 0x000000BE System.Void LockedDoor::OnTriggerExit2D(UnityEngine.Collider2D)
extern void LockedDoor_OnTriggerExit2D_mFB4DFEC33AE0CCE45615B64EE0C7FE5C94C04896 (void);
// 0x000000BF System.Void LockedDoor::.ctor()
extern void LockedDoor__ctor_mE91B7B4C2A537472B90A08E48DA07C13465015DE (void);
// 0x000000C0 System.Void pot::Start()
extern void pot_Start_mA148AB2581B13941B179F0751480A5990E57ADA5 (void);
// 0x000000C1 System.Void pot::Update()
extern void pot_Update_m9ED02433892E66A6444D61903B88AD689D2DC0C2 (void);
// 0x000000C2 System.Void pot::Smash()
extern void pot_Smash_m14F36003257FD59A4875086B575FC3D2C33D0F9B (void);
// 0x000000C3 System.Collections.IEnumerator pot::breakCo()
extern void pot_breakCo_m136505C681494DEFE0A2FD7B11005EFF45467773 (void);
// 0x000000C4 System.Void pot::.ctor()
extern void pot__ctor_m6D7B09EC649D55BFA44E817B638A7081C23F9703 (void);
// 0x000000C5 System.Void pot/<breakCo>d__4::.ctor(System.Int32)
extern void U3CbreakCoU3Ed__4__ctor_m4C5D4FA0324868E76681FE2DD7569F941DE6ED2C (void);
// 0x000000C6 System.Void pot/<breakCo>d__4::System.IDisposable.Dispose()
extern void U3CbreakCoU3Ed__4_System_IDisposable_Dispose_mBE51471B28DFE63D563EB50A7557E492221EBCB7 (void);
// 0x000000C7 System.Boolean pot/<breakCo>d__4::MoveNext()
extern void U3CbreakCoU3Ed__4_MoveNext_m6E0B48E1D5E17484ED26691C762B248AC8A502E7 (void);
// 0x000000C8 System.Object pot/<breakCo>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CbreakCoU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4532DAB64070BD2B8B9F4D6D27B2E56D29695783 (void);
// 0x000000C9 System.Void pot/<breakCo>d__4::System.Collections.IEnumerator.Reset()
extern void U3CbreakCoU3Ed__4_System_Collections_IEnumerator_Reset_m3907F5C4D0C4E2A92262694229C6B6FC3A64A405 (void);
// 0x000000CA System.Object pot/<breakCo>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CbreakCoU3Ed__4_System_Collections_IEnumerator_get_Current_m1077B64857782AE047E326DB80336EF7C7D3215B (void);
// 0x000000CB System.Void Room::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Room_OnTriggerEnter2D_mF4A96BCD7F1DF9BBEAE2DB4CAC5CF7F7FD414CFF (void);
// 0x000000CC System.Void Room::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Room_OnTriggerExit2D_m6838867165439A4FFC1C8ABAE423E2CA8D0A5659 (void);
// 0x000000CD System.Void Room::RespawnObjects()
extern void Room_RespawnObjects_m396725255D07E240F28D95675DDADA03BB26C5A0 (void);
// 0x000000CE System.Void Room::DespawnObjects()
extern void Room_DespawnObjects_m890A4BD4501CFCA8951DA55DA450DB101B390A63 (void);
// 0x000000CF System.Void Room::.ctor()
extern void Room__ctor_m970729D9D91E49A7173A5AFD56CC3D9047A7BC8C (void);
// 0x000000D0 System.Void SecretTree::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void SecretTree_OnTriggerEnter2D_m0F7895AB99D452626E01A2329C0B3E5351BFF686 (void);
// 0x000000D1 System.Void SecretTree::TakeDamage(System.Int32)
extern void SecretTree_TakeDamage_mA29F0E7A03476429236BF0D11199CD201E85F5D1 (void);
// 0x000000D2 System.Void SecretTree::Die()
extern void SecretTree_Die_mC21BCA6CD4D49DE6C0926AE7A96263385B94CBDF (void);
// 0x000000D3 System.Void SecretTree::.ctor()
extern void SecretTree__ctor_m1F3E6FF89EA258E4A9368FFE46C61F057614D053 (void);
// 0x000000D4 System.Void ContextClue::ChangeClue()
extern void ContextClue_ChangeClue_m88DCC5066385F2ECB090AEC7F5ACC75032DE6E14 (void);
// 0x000000D5 System.Void ContextClue::.ctor()
extern void ContextClue__ctor_mF7026A5A6FA6EF75187C49F2E0D936B6688FE916 (void);
// 0x000000D6 System.Void HeartManager::Start()
extern void HeartManager_Start_m485EB9FCB71F8681C7647CFC9518AA2CF065A673 (void);
// 0x000000D7 System.Void HeartManager::Update()
extern void HeartManager_Update_mDC975DE00FAC15324F6221006E3980B96A0C91A9 (void);
// 0x000000D8 System.Void HeartManager::InitHearts()
extern void HeartManager_InitHearts_mEB02789C4112E74523358260B78129E5C784623B (void);
// 0x000000D9 System.Void HeartManager::UpdateHearts()
extern void HeartManager_UpdateHearts_m030A4653A9C088D21045EA686833D124A2F6B4B7 (void);
// 0x000000DA System.Void HeartManager::.ctor()
extern void HeartManager__ctor_mDB60E8246EC34BAD5C9C8629B609FFACA518A1E0 (void);
// 0x000000DB System.Void PlayerHealth::Start()
extern void PlayerHealth_Start_m78FD812EF2B87E9EC7A405A1BBB6ECB27BFF3589 (void);
// 0x000000DC System.Void PlayerHealth::Update()
extern void PlayerHealth_Update_m4ACD2FDDEBE8DC21C71BB853A975D03DD65061B3 (void);
// 0x000000DD System.Void PlayerHealth::Damage(System.Int32)
extern void PlayerHealth_Damage_mF01D9007EC116CE828798CE6EE3AA2F67DC2AA09 (void);
// 0x000000DE System.Void PlayerHealth::FixedUpdate()
extern void PlayerHealth_FixedUpdate_m021918574F91205FF7F06F34FC6EE3CBD44A3B91 (void);
// 0x000000DF System.Void PlayerHealth::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PlayerHealth_OnTriggerEnter2D_m5D21A0886DDA60146BA4B5ED0C2A0C4EE0D20766 (void);
// 0x000000E0 System.Void PlayerHealth::TakeDamage(System.Int32)
extern void PlayerHealth_TakeDamage_m21F4EA70549D145406E078F0543A934DE498FDB7 (void);
// 0x000000E1 System.Void PlayerHealth::Die()
extern void PlayerHealth_Die_mBF97B09DFB17C4BBA27719DD2FFA1F4221E8CAB3 (void);
// 0x000000E2 System.Void PlayerHealth::.ctor()
extern void PlayerHealth__ctor_mE9AF3CA69205909E44287664BEAE503EC43875F1 (void);
// 0x000000E3 System.Void PlayerHit::Start()
extern void PlayerHit_Start_mAADC6279A20EF90C76AD46C93087C1BD5917A775 (void);
// 0x000000E4 System.Void PlayerHit::Update()
extern void PlayerHit_Update_m77F7F12F7AF4EB3207D97B55D37A9785A7F17623 (void);
// 0x000000E5 System.Void PlayerHit::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PlayerHit_OnTriggerEnter2D_mAA5CC82FB0294234AFF12BA7EA8B10C870008A6E (void);
// 0x000000E6 System.Void PlayerHit::.ctor()
extern void PlayerHit__ctor_mAC6AED11C78C91DEFC68D28B2166810A437712CA (void);
// 0x000000E7 System.Void PlayerMagic::.ctor()
extern void PlayerMagic__ctor_mB570018C5F46BE31069F30B3575CAFB5A860B771 (void);
// 0x000000E8 System.Void PlayerMoney::AddMoney(System.Int32)
extern void PlayerMoney_AddMoney_mBBECA2F4BC35A1F2D633A86A8C851CFA08353F94 (void);
// 0x000000E9 System.Boolean PlayerMoney::CanAfford(System.Int32)
extern void PlayerMoney_CanAfford_mF27A39CA6F85E0DF1B9770CDCB4125831BF1040F (void);
// 0x000000EA System.Void PlayerMoney::SubtractMoney(System.Int32)
extern void PlayerMoney_SubtractMoney_mA0AA22DF3025C7227D75EC9205934336F40833B5 (void);
// 0x000000EB System.Void PlayerMoney::.ctor()
extern void PlayerMoney__ctor_mCB6939DD39E376FBA7297453EE4469A38FF23FBB (void);
// 0x000000EC System.Void ReceiveItem::Start()
extern void ReceiveItem_Start_m36C94AD06AAD471AE04C0C331FA21354B7D719DF (void);
// 0x000000ED System.Void ReceiveItem::ChangeSpriteState()
extern void ReceiveItem_ChangeSpriteState_mD7895EC426CFD6CE6C13889875DDEEAC77400A5E (void);
// 0x000000EE System.Void ReceiveItem::DisplaySprite()
extern void ReceiveItem_DisplaySprite_mAA991D24E7EDA86833632C2F659390211AEF448F (void);
// 0x000000EF System.Void ReceiveItem::DisableSprite()
extern void ReceiveItem_DisableSprite_m250A49E1DC0073E7F32627AD536CC86D0CD443C0 (void);
// 0x000000F0 System.Void ReceiveItem::.ctor()
extern void ReceiveItem__ctor_m5DF06C5377F562FE12E670D203439B1FC1A897C2 (void);
// 0x000000F1 System.Void ResetPlayerPosition::Start()
extern void ResetPlayerPosition_Start_m1E81F0FC838BBF264705265BD4104697979B722E (void);
// 0x000000F2 System.Void ResetPlayerPosition::.ctor()
extern void ResetPlayerPosition__ctor_m559A10FF1AD24B8BB648925D3EAC192B00810E3C (void);
// 0x000000F3 System.Void DashAbility::Ability(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Animator,UnityEngine.Rigidbody2D)
extern void DashAbility_Ability_m6E9AB9604E22095FB88BA3CC374373244F721AEB (void);
// 0x000000F4 System.Void DashAbility::.ctor()
extern void DashAbility__ctor_m0180D7F255A74C2CED77A67B083B2663BAD1CFB9 (void);
// 0x000000F5 System.Void GenericAbility::Ability(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Animator,UnityEngine.Rigidbody2D)
extern void GenericAbility_Ability_mBDE8CB83299782CD949851F8FB7120974CEB5424 (void);
// 0x000000F6 System.Void GenericAbility::.ctor()
extern void GenericAbility__ctor_m5025FD2EBE6F90A67261660C64E5B73A21920A19 (void);
// 0x000000F7 System.Void BoolValue::OnEnable()
extern void BoolValue_OnEnable_mC68A4CE5236752E9E4A48123D060159ECCF0E4A1 (void);
// 0x000000F8 System.Void BoolValue::.ctor()
extern void BoolValue__ctor_mCD5FC467EF725EA43475F255C89E7261F52C331C (void);
// 0x000000F9 System.Void FloatValue::OnAfterDeserialize()
extern void FloatValue_OnAfterDeserialize_m2880B383D890B10CCC287EBE19F71DF59627B9F8 (void);
// 0x000000FA System.Void FloatValue::OnBeforeSerialize()
extern void FloatValue_OnBeforeSerialize_m7FE75573E854C9D12B0B268713C8EC68537A165F (void);
// 0x000000FB System.Void FloatValue::OnEnable()
extern void FloatValue_OnEnable_m764380CE53BF298A66BADDE935C008BEA2AC9D2B (void);
// 0x000000FC System.Void FloatValue::.ctor()
extern void FloatValue__ctor_mFF3D035592CC458A8182C574A2B60C04BD3A0F83 (void);
// 0x000000FD System.Void Inventory::AddItem(InventoryItem)
extern void Inventory_AddItem_mB6001585F2C522F10D30A42956CE8DC9230626D8 (void);
// 0x000000FE System.Void Inventory::RemoveItem(InventoryItem)
extern void Inventory_RemoveItem_m7E9029F971D3EDB4966EA7474A5A9D6A445FE2F1 (void);
// 0x000000FF System.Void Inventory::UseItem(InventoryItem)
extern void Inventory_UseItem_mC08A762F9C9DECD654C916E327C0B264C44321B6 (void);
// 0x00000100 System.Boolean Inventory::IsItemInInventory(InventoryItem)
extern void Inventory_IsItemInInventory_mDE01027E651A29396F0DA0AB93DE45C934F8E5B8 (void);
// 0x00000101 System.Boolean Inventory::canUseItem(InventoryItem)
extern void Inventory_canUseItem_m4A1006D08FC7549F59893A29EB0D725E959FBB60 (void);
// 0x00000102 System.Void Inventory::.ctor()
extern void Inventory__ctor_mF2ACBF005FF40F23F68AE8E9E416A4870EC4B27C (void);
// 0x00000103 System.Void InventoryItem::.ctor()
extern void InventoryItem__ctor_mDA2BEAE4ED6F565E6B7762D8A33C6D12BB7FD133 (void);
// 0x00000104 System.Void Notification::Raise()
extern void Notification_Raise_m2180F845CF6C9122CFA662C7755BC8BB4AA1D49B (void);
// 0x00000105 System.Void Notification::RegisterListener(NotificationListener)
extern void Notification_RegisterListener_m2894970C218C125F6D53721954B4827B7C9DA6FD (void);
// 0x00000106 System.Void Notification::DeregisterListener(NotificationListener)
extern void Notification_DeregisterListener_mA5B365A057F340F76AA801FF6DEC1F4EB8EB4B89 (void);
// 0x00000107 System.Void Notification::.ctor()
extern void Notification__ctor_m244DF5A7BD448937CC8718BB814EDF8E83068306 (void);
// 0x00000108 System.Void NotificationListener::OnEnable()
extern void NotificationListener_OnEnable_mF482FC12885D30C9FC3B475177486EFE85416656 (void);
// 0x00000109 System.Void NotificationListener::OnDisable()
extern void NotificationListener_OnDisable_m27A0F21D7F0CCA70D0CA9102FBB5B2C84B9FFAE4 (void);
// 0x0000010A System.Void NotificationListener::Raise()
extern void NotificationListener_Raise_m6865C5819AB4C0DEDD81A1FA7263F94ED25310E6 (void);
// 0x0000010B System.Void NotificationListener::.ctor()
extern void NotificationListener__ctor_m4CBC45D129DE2652E3AAA8A89698175C099C315C (void);
// 0x0000010C System.Void SignalSender::Raise()
extern void SignalSender_Raise_m360EC164A64F84E8CC7E49DDD41BD6400FB6BB71 (void);
// 0x0000010D System.Void SignalSender::RegisterListener(SignalListener)
extern void SignalSender_RegisterListener_m2F9F47F5F5404E9752F2C0A72108DFEF4BD8C009 (void);
// 0x0000010E System.Void SignalSender::DeRegisterListener(SignalListener)
extern void SignalSender_DeRegisterListener_mA4BD75C6227B06C903AA31EC3A8829521786AA63 (void);
// 0x0000010F System.Void SignalSender::.ctor()
extern void SignalSender__ctor_mA64E0F931DCEF44D3575861DAE1E06B8B6AFB9B8 (void);
// 0x00000110 System.Void SpriteValue::.ctor()
extern void SpriteValue__ctor_m45BE209A5D3F0E115A0B102A999DD816C0794580 (void);
// 0x00000111 System.Void StringValue::.ctor()
extern void StringValue__ctor_m52E5D1DE2D80FA25997E21578C2B6DF3765FFF23 (void);
// 0x00000112 System.Void VectorValue::OnEnable()
extern void VectorValue_OnEnable_m23398C0A9C5CCCCD9770A9B94AC764610D1D8693 (void);
// 0x00000113 System.Void VectorValue::.ctor()
extern void VectorValue__ctor_mF49A40A277536A52D56535D6A3C7BD9B039E9EDD (void);
// 0x00000114 System.Void Dialogue::.ctor()
extern void Dialogue__ctor_m76C11FD80AFD4F6BE87E30C737879EBB5A1D724E (void);
// 0x00000115 System.Void DialogueManager::Start()
extern void DialogueManager_Start_m612B88A606E0F326778C59A3207CBF0548F5C6A5 (void);
// 0x00000116 System.Void DialogueManager::StartDialogue(Dialogue)
extern void DialogueManager_StartDialogue_m2674465637758F34587FAD6CFF453059F6835B0D (void);
// 0x00000117 System.Void DialogueManager::DisplayNextSentence()
extern void DialogueManager_DisplayNextSentence_m14A583FBA1E346E5B4D772A9E022D6F78FE6A383 (void);
// 0x00000118 System.Collections.IEnumerator DialogueManager::TypeSentence(System.String)
extern void DialogueManager_TypeSentence_m74B6A598B53C2B9725C6343F801FD272F6F60CE4 (void);
// 0x00000119 System.Void DialogueManager::EndDialogue()
extern void DialogueManager_EndDialogue_mFF5F59FF3A5EC3A9B41AA3B92580371831DC80FB (void);
// 0x0000011A System.Void DialogueManager::.ctor()
extern void DialogueManager__ctor_mAA8FF2D1E586DBF3472DF58D3B1BF8F4FA03EE1E (void);
// 0x0000011B System.Void DialogueManager/<TypeSentence>d__7::.ctor(System.Int32)
extern void U3CTypeSentenceU3Ed__7__ctor_m9DA545853895CDAE646DBA7EFF77CAB01870125A (void);
// 0x0000011C System.Void DialogueManager/<TypeSentence>d__7::System.IDisposable.Dispose()
extern void U3CTypeSentenceU3Ed__7_System_IDisposable_Dispose_mBB525A4327EF1046B3FECA28648E6B831051994A (void);
// 0x0000011D System.Boolean DialogueManager/<TypeSentence>d__7::MoveNext()
extern void U3CTypeSentenceU3Ed__7_MoveNext_m08234224360D24514091B3424CAB12840B46203B (void);
// 0x0000011E System.Object DialogueManager/<TypeSentence>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTypeSentenceU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33A483BEE1B98A623207D8191EDA472659750BED (void);
// 0x0000011F System.Void DialogueManager/<TypeSentence>d__7::System.Collections.IEnumerator.Reset()
extern void U3CTypeSentenceU3Ed__7_System_Collections_IEnumerator_Reset_mFAE38B6F7B55A3EAA896B235FC31ECE3D8A63A01 (void);
// 0x00000120 System.Object DialogueManager/<TypeSentence>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CTypeSentenceU3Ed__7_System_Collections_IEnumerator_get_Current_mDF5ED2D5A6FBBBA1248735893AD2A53E944050BD (void);
// 0x00000121 System.Void DialogueTrigger::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void DialogueTrigger_OnTriggerEnter2D_m8588DF9556738DD26CE50F7A21B2ECADC9391C73 (void);
// 0x00000122 System.Void DialogueTrigger::Update()
extern void DialogueTrigger_Update_mAC78DCF0C9F328F1620AFA0848780E76899B58DA (void);
// 0x00000123 System.Void DialogueTrigger::TriggerDialogue()
extern void DialogueTrigger_TriggerDialogue_mF018F9EE2910BD5138B9D3C6D993713E0B2A271E (void);
// 0x00000124 System.Void DialogueTrigger::.ctor()
extern void DialogueTrigger__ctor_mE5F20AA50727E901ED31CC812C825F74369068F3 (void);
// 0x00000125 System.Void NPC::Start()
extern void NPC_Start_m20C2D17950CB234A91905C91CCEADA0870B913B9 (void);
// 0x00000126 System.Void NPC::FixedUpdate()
extern void NPC_FixedUpdate_mC4F9FFB1FAE5068B9E3C89D101F9CF3DB2790E50 (void);
// 0x00000127 System.Void NPC::Update()
extern void NPC_Update_m385369B628E634D1BB4300956637169CBAFDDEAE (void);
// 0x00000128 System.Void NPC::CheckDistance()
extern void NPC_CheckDistance_m3ED78ACAFD6CFC277DE06B2574677601656F0EE6 (void);
// 0x00000129 System.Void NPC::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void NPC_OnTriggerEnter2D_m44B8BDE947AAA30903A6336C7B8C49331EB49F4D (void);
// 0x0000012A System.Void NPC::TakeDamage(System.Int32)
extern void NPC_TakeDamage_m7CCBD2CED11B8A59C6826B7FD04D10E3E4F08DEF (void);
// 0x0000012B System.Void NPC::Die()
extern void NPC_Die_m20C03EAC0EB880EACB2D23E7EFE2BBF82E6B969E (void);
// 0x0000012C System.Void NPC::.ctor()
extern void NPC__ctor_mC9AA4F3CBCBADCB1866559D68B31E231A879CACE (void);
// 0x0000012D System.Void testdamagescript::Start()
extern void testdamagescript_Start_mF235A713B9A7330FAB7916E2564EB11557025E4E (void);
// 0x0000012E System.Void testdamagescript::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void testdamagescript_OnTriggerEnter2D_mB6D3771B834C32B001B03425869EDA04279734E6 (void);
// 0x0000012F System.Void testdamagescript::.ctor()
extern void testdamagescript__ctor_m3C05CECDFF2EB8AC2652E85907AA10675D9D5968 (void);
// 0x00000130 System.Void testenemyscript::Start()
extern void testenemyscript_Start_mE5A52C67068652D3B5CACFF6E0D600C0D8677593 (void);
// 0x00000131 System.Void testenemyscript::FixedUpdate()
extern void testenemyscript_FixedUpdate_m5AA2136C7BAAA6D856B15E85990040517CAA78CC (void);
// 0x00000132 System.Void testenemyscript::CheckDistance()
extern void testenemyscript_CheckDistance_mB76CCAC7CB8168BBDEE9CFBADDF2C75115217C5D (void);
// 0x00000133 System.Void testenemyscript::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void testenemyscript_OnTriggerEnter2D_m0F4D100CB7532A42EF3CA8008C1881B9E669DBC1 (void);
// 0x00000134 System.Void testenemyscript::TakeDamage(System.Int32)
extern void testenemyscript_TakeDamage_mFC446BB9ABBB0ECAB59CF10988CA42C1DF3DE3A1 (void);
// 0x00000135 System.Void testenemyscript::Die()
extern void testenemyscript_Die_m9DBDFB8986A262BB7A3B7B7444F97C6FDE3C6930 (void);
// 0x00000136 System.Void testenemyscript::.ctor()
extern void testenemyscript__ctor_m5DE8EFECBB62360491721050B27686950E683DB1 (void);
// 0x00000137 System.Void AreaNameController::ActivateText()
extern void AreaNameController_ActivateText_mFE39B39F19D3DA53720FD5C948D210D88D9F85EE (void);
// 0x00000138 System.Collections.IEnumerator AreaNameController::NameCo()
extern void AreaNameController_NameCo_mCB01D701882E7B4DE795086037035E22C1310D2C (void);
// 0x00000139 System.Void AreaNameController::SetText(System.String)
extern void AreaNameController_SetText_m8D3B5462FE9F09AB1A22A6CD25F1455137A87CC0 (void);
// 0x0000013A System.Void AreaNameController::.ctor()
extern void AreaNameController__ctor_mEDD56E43DDE2D8513C1BA2D0593D9A5B2B5024CF (void);
// 0x0000013B System.Void AreaNameController/<NameCo>d__4::.ctor(System.Int32)
extern void U3CNameCoU3Ed__4__ctor_m0295877D75B0907AB6E571ACF7EDFBD4C62C510A (void);
// 0x0000013C System.Void AreaNameController/<NameCo>d__4::System.IDisposable.Dispose()
extern void U3CNameCoU3Ed__4_System_IDisposable_Dispose_mA2CE24FE422BB68D8D07A65F547562F8A0C4CCDC (void);
// 0x0000013D System.Boolean AreaNameController/<NameCo>d__4::MoveNext()
extern void U3CNameCoU3Ed__4_MoveNext_m12759D644E9217F5D73D9BC1C7E45A2E03E91A65 (void);
// 0x0000013E System.Object AreaNameController/<NameCo>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNameCoU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF84216422D657C1B0CA4B2D9DB13DC626D5632A5 (void);
// 0x0000013F System.Void AreaNameController/<NameCo>d__4::System.Collections.IEnumerator.Reset()
extern void U3CNameCoU3Ed__4_System_Collections_IEnumerator_Reset_m466BA511DD3CA1F9BF5F805759C3A2351DFA8667 (void);
// 0x00000140 System.Object AreaNameController/<NameCo>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CNameCoU3Ed__4_System_Collections_IEnumerator_get_Current_m334CB1DAD5F575544F8E8E9F879F7A42DF38CDDF (void);
// 0x00000141 System.Void DialogController::ActivateDialog()
extern void DialogController_ActivateDialog_mED76DD3396FAFEAEBC9599EC84B88D1E4C831F6B (void);
// 0x00000142 System.Void DialogController::SetDialog()
extern void DialogController_SetDialog_m2A4B2A42BCF34B6935EB940A27823727BFBEF6C8 (void);
// 0x00000143 System.Void DialogController::DeactivateDialog()
extern void DialogController_DeactivateDialog_mBC8E5901012CD480BAD3251C37DDE3F1113E445D (void);
// 0x00000144 System.Void DialogController::.ctor()
extern void DialogController__ctor_mEA038FA1BA7F61E62633EFD61524922F13021A8B (void);
// 0x00000145 System.Void GameMenu::Play()
extern void GameMenu_Play_m9A8D1B17564609C24D569CF15090387D1064F05E (void);
// 0x00000146 System.Void GameMenu::Quit()
extern void GameMenu_Quit_m789B754A520ABA9AB03273E6A3F5D2378CD40CC3 (void);
// 0x00000147 System.Void GameMenu::Credits()
extern void GameMenu_Credits_m81C707BB36BF2FC1A5B2879FA7E800EB00269787 (void);
// 0x00000148 System.Void GameMenu::Back()
extern void GameMenu_Back_m533ABED6145D47488CBE1F585CCEA2642868F385 (void);
// 0x00000149 System.Void GameMenu::.ctor()
extern void GameMenu__ctor_m9D09CB0FB4C667FFE84AA5353BB107A3D4DCA47F (void);
// 0x0000014A System.Void PauseManager::Start()
extern void PauseManager_Start_mCC82D1C26D42EE03E43E38CFDD89ED3B69433F06 (void);
// 0x0000014B System.Void PauseManager::Update()
extern void PauseManager_Update_m2C800AF811359C3E56E97FC30FF2CAA99995E337 (void);
// 0x0000014C System.Void PauseManager::ChangePauseValue()
extern void PauseManager_ChangePauseValue_m0957F6A751D07AAD686FE7636AE8C0CB6684F270 (void);
// 0x0000014D System.Void PauseManager::ResumeButton()
extern void PauseManager_ResumeButton_mFACC8973E966F822E4E0A33E3B887A59A538A58E (void);
// 0x0000014E System.Void PauseManager::QuitButton()
extern void PauseManager_QuitButton_mB47492FD397629AA540D735EEB1B15BE28D32E43 (void);
// 0x0000014F System.Void PauseManager::.ctor()
extern void PauseManager__ctor_m545310BC21D8C0D70A026984679E2439BC4B92FF (void);
// 0x00000150 UnityEngine.Vector2[] SuperTiled2Unity.CollisionObject::get_Points()
extern void CollisionObject_get_Points_m6CF7BD37D0508F232C68B0AC035CD4E69CB82BB3 (void);
// 0x00000151 System.Boolean SuperTiled2Unity.CollisionObject::get_IsClosed()
extern void CollisionObject_get_IsClosed_m580920705D3198F13115F60BF59EEC712DA87A15 (void);
// 0x00000152 SuperTiled2Unity.CollisionShapeType SuperTiled2Unity.CollisionObject::get_CollisionShapeType()
extern void CollisionObject_get_CollisionShapeType_m47C2F59DD1A4C0CF3CDCEE73507B66D95C83ABEE (void);
// 0x00000153 System.Void SuperTiled2Unity.CollisionObject::MakePointsFromRectangle()
extern void CollisionObject_MakePointsFromRectangle_m796381DB2CC9F74BED762982856F1D58BC88A8C0 (void);
// 0x00000154 System.Void SuperTiled2Unity.CollisionObject::MakePointsFromEllipse(System.Int32)
extern void CollisionObject_MakePointsFromEllipse_mD0CDE342967AB9961D5DD10A1F5F98E0D408BC8C (void);
// 0x00000155 System.Void SuperTiled2Unity.CollisionObject::MakePointsFromPolygon(UnityEngine.Vector2[])
extern void CollisionObject_MakePointsFromPolygon_mCBFDABDCB0FA51C5338FC9A3356330F148B28CA5 (void);
// 0x00000156 System.Void SuperTiled2Unity.CollisionObject::MakePointsFromPolyline(UnityEngine.Vector2[])
extern void CollisionObject_MakePointsFromPolyline_mC5D6EE4A9B33DA76CB4CCF4F7E287359F8B58589 (void);
// 0x00000157 System.Void SuperTiled2Unity.CollisionObject::RenderPoints(SuperTiled2Unity.SuperTile,SuperTiled2Unity.GridOrientation,UnityEngine.Vector2)
extern void CollisionObject_RenderPoints_m3898C52CD076ACA30F62EE4EBAD968FB8D26D802 (void);
// 0x00000158 UnityEngine.Vector2 SuperTiled2Unity.CollisionObject::IsometricTransform(UnityEngine.Vector2,SuperTiled2Unity.SuperTile,UnityEngine.Vector2)
extern void CollisionObject_IsometricTransform_m71A1212C43A79D571A9223047185D5C757F9D1AD (void);
// 0x00000159 UnityEngine.Vector2 SuperTiled2Unity.CollisionObject::LocalTransform(UnityEngine.Vector2,SuperTiled2Unity.SuperTile)
extern void CollisionObject_LocalTransform_mF3343FC234107796B33E602FA4AFEC1D2E475F35 (void);
// 0x0000015A System.Void SuperTiled2Unity.CollisionObject::ApplyRotationToPoints()
extern void CollisionObject_ApplyRotationToPoints_m18C718583597A2287CBD079A02DBF8F306AD2F3D (void);
// 0x0000015B System.Void SuperTiled2Unity.CollisionObject::.ctor()
extern void CollisionObject__ctor_mF8D1D18A71C70EAB4AA711E94F09BC79E21ADED2 (void);
// 0x0000015C System.Void SuperTiled2Unity.CollisionObject/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_mCEAD773FAAE0268FD86A984B99A2624E2C4C17B1 (void);
// 0x0000015D UnityEngine.Vector2 SuperTiled2Unity.CollisionObject/<>c__DisplayClass22_0::<RenderPoints>b__0(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CRenderPointsU3Eb__0_m04DF1A4EE79361390CC9C5B389FF5768A7F628AC (void);
// 0x0000015E UnityEngine.Vector2 SuperTiled2Unity.CollisionObject/<>c__DisplayClass22_0::<RenderPoints>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CRenderPointsU3Eb__1_mF55BB46C57BEAF902DD1384B61949F94488094A9 (void);
// 0x0000015F System.Void SuperTiled2Unity.CollisionObject/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mA796A7F0E743361BDDD1E65CF70CE4665CDD9C3A (void);
// 0x00000160 UnityEngine.Vector3 SuperTiled2Unity.CollisionObject/<>c__DisplayClass25_0::<ApplyRotationToPoints>b__0(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass25_0_U3CApplyRotationToPointsU3Eb__0_m56E27B156516C338BA4F198F45A233FBF069ABAC (void);
// 0x00000161 System.Void SuperTiled2Unity.CollisionObject/<>c::.cctor()
extern void U3CU3Ec__cctor_m21E7EAB3B1683DE75A5E68A7C1771CC076294FD8 (void);
// 0x00000162 System.Void SuperTiled2Unity.CollisionObject/<>c::.ctor()
extern void U3CU3Ec__ctor_m54FCC71CE2FC22C9840D5E1B9283F775AA12D420 (void);
// 0x00000163 UnityEngine.Vector2 SuperTiled2Unity.CollisionObject/<>c::<ApplyRotationToPoints>b__25_1(UnityEngine.Vector3)
extern void U3CU3Ec_U3CApplyRotationToPointsU3Eb__25_1_mDE5729996558DD5930C818770E43E1BABE390D9F (void);
// 0x00000164 System.Boolean SuperTiled2Unity.CustomProperty::get_IsEmpty()
extern void CustomProperty_get_IsEmpty_m978AB75E34F297B1CA82E5E5AA42316868C74EE0 (void);
// 0x00000165 System.Void SuperTiled2Unity.CustomProperty::.ctor()
extern void CustomProperty__ctor_mEB3E18A1CEF4F7EF340B0CA3D964263149A6137C (void);
// 0x00000166 System.Boolean SuperTiled2Unity.CustomPropertyListExtensions::TryGetProperty(System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty>,System.String,SuperTiled2Unity.CustomProperty&)
extern void CustomPropertyListExtensions_TryGetProperty_m610D3ACC325AB44067452C515DD10062DC01F9AA (void);
// 0x00000167 System.Void SuperTiled2Unity.CustomPropertyListExtensions/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m10A05D207F27190442736DF43F8293E105C95F6B (void);
// 0x00000168 System.Boolean SuperTiled2Unity.CustomPropertyListExtensions/<>c__DisplayClass0_0::<TryGetProperty>b__0(SuperTiled2Unity.CustomProperty)
extern void U3CU3Ec__DisplayClass0_0_U3CTryGetPropertyU3Eb__0_m0BD32B119517C2B1A3EAD83EC692BCA1A763C14B (void);
// 0x00000169 System.String SuperTiled2Unity.CustomPropertyExtensions::GetValueAsString(SuperTiled2Unity.CustomProperty)
extern void CustomPropertyExtensions_GetValueAsString_mE3789EE7A117E66D8FE55E977B31F18FB7480346 (void);
// 0x0000016A UnityEngine.Color SuperTiled2Unity.CustomPropertyExtensions::GetValueAsColor(SuperTiled2Unity.CustomProperty)
extern void CustomPropertyExtensions_GetValueAsColor_mC872F887E4560B76E54E12FC539BB7AF618B4E37 (void);
// 0x0000016B System.Int32 SuperTiled2Unity.CustomPropertyExtensions::GetValueAsInt(SuperTiled2Unity.CustomProperty)
extern void CustomPropertyExtensions_GetValueAsInt_mCA843CA55CE6C08B5C1621B71EDC8010E32A9499 (void);
// 0x0000016C System.Single SuperTiled2Unity.CustomPropertyExtensions::GetValueAsFloat(SuperTiled2Unity.CustomProperty)
extern void CustomPropertyExtensions_GetValueAsFloat_m3183E8A5D90D817BEE59EF0F5BA438B532D63540 (void);
// 0x0000016D System.Boolean SuperTiled2Unity.CustomPropertyExtensions::GetValueAsBool(SuperTiled2Unity.CustomProperty)
extern void CustomPropertyExtensions_GetValueAsBool_mFACA71E29D4012D9680FCD905CE064F0441CA334 (void);
// 0x0000016E T SuperTiled2Unity.CustomPropertyExtensions::GetValueAsEnum(SuperTiled2Unity.CustomProperty)
// 0x0000016F System.Boolean SuperTiled2Unity.EnumerableExtensions::IsEmpty(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000170 T SuperTiled2Unity.GameObjectExtensions::GetComponentInAncestor(UnityEngine.MonoBehaviour)
// 0x00000171 T SuperTiled2Unity.GameObjectExtensions::GetComponentInAncestor(UnityEngine.GameObject)
// 0x00000172 System.Boolean SuperTiled2Unity.GameObjectExtensions::TryGetCustomPropertySafe(UnityEngine.GameObject,System.String,SuperTiled2Unity.CustomProperty&)
extern void GameObjectExtensions_TryGetCustomPropertySafe_m5F5E5234B522E79EF3DC4BF7EF336F04A64E774D (void);
// 0x00000173 UnityEngine.Color SuperTiled2Unity.StringExtensions::ToColor(System.String)
extern void StringExtensions_ToColor_mC4A382B8EBE0EB3814C58E82CBA3C9A29556AB13 (void);
// 0x00000174 T SuperTiled2Unity.StringExtensions::ToEnum(System.String)
// 0x00000175 System.Single SuperTiled2Unity.StringExtensions::ToFloat(System.String)
extern void StringExtensions_ToFloat_m43913E60F4BC0CB3F25B5F58E76F99FD262FE170 (void);
// 0x00000176 System.Int32 SuperTiled2Unity.StringExtensions::ToInt(System.String)
extern void StringExtensions_ToInt_m6D40614BFE9C46CCAA31108B53F17E0C5B00A6EB (void);
// 0x00000177 System.Boolean SuperTiled2Unity.StringExtensions::ToBool(System.String)
extern void StringExtensions_ToBool_m87339C631EFBEDFEF969558D94869FFE57A9B6F5 (void);
// 0x00000178 System.Boolean SuperTiled2Unity.SuperTileExtensions::TryGetProperty(SuperTiled2Unity.SuperTile,System.String,SuperTiled2Unity.CustomProperty&)
extern void SuperTileExtensions_TryGetProperty_mDAF7C872C57BFAAD0ADAC8D1E441A6CFF717ECBD (void);
// 0x00000179 System.String SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsString(SuperTiled2Unity.SuperTile,System.String)
extern void SuperTileExtensions_GetPropertyValueAsString_m83D3B395862333224EAFB46EBF43FB8C2C828EAF (void);
// 0x0000017A System.String SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsString(SuperTiled2Unity.SuperTile,System.String,System.String)
extern void SuperTileExtensions_GetPropertyValueAsString_m643DC10D0EA82D87CA2E0DEE4C2FB0C652D0CBF2 (void);
// 0x0000017B System.Boolean SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsBool(SuperTiled2Unity.SuperTile,System.String)
extern void SuperTileExtensions_GetPropertyValueAsBool_m37C447EF97E56A3D152BD60D7F948CF666971847 (void);
// 0x0000017C System.Boolean SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsBool(SuperTiled2Unity.SuperTile,System.String,System.Boolean)
extern void SuperTileExtensions_GetPropertyValueAsBool_m69DBF9E59581BDF7FC26AFD307E139B22BAA0E64 (void);
// 0x0000017D System.Int32 SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsInt(SuperTiled2Unity.SuperTile,System.String)
extern void SuperTileExtensions_GetPropertyValueAsInt_m3D59F7D85ABF8C86A01982D46A31913CB8AC4096 (void);
// 0x0000017E System.Int32 SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsInt(SuperTiled2Unity.SuperTile,System.String,System.Int32)
extern void SuperTileExtensions_GetPropertyValueAsInt_m1168F2B3C96A451AB03EDFA45AF55FED065FB4A1 (void);
// 0x0000017F System.Single SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsFloat(SuperTiled2Unity.SuperTile,System.String)
extern void SuperTileExtensions_GetPropertyValueAsFloat_m87A33A3D138C5E9D28AEA46C33A9D0C578772424 (void);
// 0x00000180 System.Single SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsFloat(SuperTiled2Unity.SuperTile,System.String,System.Single)
extern void SuperTileExtensions_GetPropertyValueAsFloat_mCF862373A55454F0D2CD9F1DA72419E096B51D2F (void);
// 0x00000181 UnityEngine.Color SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsColor(SuperTiled2Unity.SuperTile,System.String)
extern void SuperTileExtensions_GetPropertyValueAsColor_m8E2692F364FDB1E5B322CA17704ECA066E7CB9D5 (void);
// 0x00000182 UnityEngine.Color SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsColor(SuperTiled2Unity.SuperTile,System.String,UnityEngine.Color)
extern void SuperTileExtensions_GetPropertyValueAsColor_mA472B0F8E5E6EB492DE9921DAD17FD714E2560F1 (void);
// 0x00000183 T SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsEnum(SuperTiled2Unity.SuperTile,System.String)
// 0x00000184 T SuperTiled2Unity.SuperTileExtensions::GetPropertyValueAsEnum(SuperTiled2Unity.SuperTile,System.String,T)
// 0x00000185 UnityEngine.Tilemaps.TilemapRenderer/SortOrder SuperTiled2Unity.MapRenderConverter::Tiled2Unity(SuperTiled2Unity.MapRenderOrder)
extern void MapRenderConverter_Tiled2Unity_m7A9904C0D7EFCE569A4E86D6C6B760FA8C929593 (void);
// 0x00000186 UnityEngine.Matrix4x4 SuperTiled2Unity.MatrixUtils::Rotate2d(System.Single,System.Single,System.Single,System.Single)
extern void MatrixUtils_Rotate2d_m2BA57D837815B31A53E7270B965D6977A251B8CC (void);
// 0x00000187 System.Void SuperTiled2Unity.ReadOnlyAttribute::.ctor()
extern void ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE (void);
// 0x00000188 System.Void SuperTiled2Unity.SuperColliderComponent::.ctor()
extern void SuperColliderComponent__ctor_mC915B9E11EB80888419A76FBA479B85FAFDC0FD1 (void);
// 0x00000189 System.Boolean SuperTiled2Unity.SuperCustomProperties::TryGetCustomProperty(System.String,SuperTiled2Unity.CustomProperty&)
extern void SuperCustomProperties_TryGetCustomProperty_m24FE230AF0F3E6F2573A77F2A224FE026670568A (void);
// 0x0000018A System.Void SuperTiled2Unity.SuperCustomProperties::.ctor()
extern void SuperCustomProperties__ctor_m269C8765BFEA3EBF9D3087D7EEF45620DCDCC7F8 (void);
// 0x0000018B System.Void SuperTiled2Unity.SuperGroupLayer::.ctor()
extern void SuperGroupLayer__ctor_m9617323A2B80A1C819223A955CDEB5257A89DB79 (void);
// 0x0000018C System.Void SuperTiled2Unity.SuperImageLayer::.ctor()
extern void SuperImageLayer__ctor_m95BF905B148F2D12B0BC99DFF22479FEA948A2C8 (void);
// 0x0000018D System.Single SuperTiled2Unity.SuperLayer::CalculateOpacity()
extern void SuperLayer_CalculateOpacity_mB02C4E20B46CD52E3AAC1D2F09797614FD7AFA55 (void);
// 0x0000018E System.Void SuperTiled2Unity.SuperLayer::.ctor()
extern void SuperLayer__ctor_m5429123751D9D8AF27F930F0EB9CA4F59AE2FFA9 (void);
// 0x0000018F System.Void SuperTiled2Unity.SuperMap::Start()
extern void SuperMap_Start_m2A10A5DB11AFAB5EEFDF8A00447DAE80627C3B36 (void);
// 0x00000190 UnityEngine.Vector3Int SuperTiled2Unity.SuperMap::TiledIndexToGridCell(System.Int32,System.Int32,System.Int32,System.Int32)
extern void SuperMap_TiledIndexToGridCell_mEF64968094BECE48AEA9BBA482CBA48B3AC5EBEC (void);
// 0x00000191 UnityEngine.Vector3Int SuperTiled2Unity.SuperMap::TiledCellToGridCell(System.Int32,System.Int32)
extern void SuperMap_TiledCellToGridCell_m6B259F9DA048369AFA034470205ACE1B5AA8E1D4 (void);
// 0x00000192 System.Void SuperTiled2Unity.SuperMap::.ctor()
extern void SuperMap__ctor_mF1BD27DB45AA4DD1CF44DDD0D558992E82CCB316 (void);
// 0x00000193 System.Single SuperTiled2Unity.SuperObject::CalculateOpacity()
extern void SuperObject_CalculateOpacity_mAC216FF8FAD9B563547310E2C1C0FCC249BAD70F (void);
// 0x00000194 System.Void SuperTiled2Unity.SuperObject::.ctor()
extern void SuperObject__ctor_m571889300581489EB24B8304D2D55B3542228AFE (void);
// 0x00000195 System.Void SuperTiled2Unity.SuperObjectLayer::.ctor()
extern void SuperObjectLayer__ctor_m9CA54CDA7914FCE43E5A7AACB0CD6DF4B994BE20 (void);
// 0x00000196 UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile::GetTransformMatrix(SuperTiled2Unity.FlipFlags)
extern void SuperTile_GetTransformMatrix_mF8405E4DD8313F9E173FBB53BA1ADA0AE52B0967 (void);
// 0x00000197 System.Void SuperTiled2Unity.SuperTile::GetTRS(SuperTiled2Unity.FlipFlags,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void SuperTile_GetTRS_m9719104E370A2E207B93D4EFA2CBBF9E6A36A35C (void);
// 0x00000198 System.Void SuperTiled2Unity.SuperTile::GetTileData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileData&)
extern void SuperTile_GetTileData_m99B915CBDBB237DE32E87CE6554C125F30319646 (void);
// 0x00000199 System.Boolean SuperTiled2Unity.SuperTile::GetTileAnimationData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileAnimationData&)
extern void SuperTile_GetTileAnimationData_mEB156AE03ADF33FE9BF57DB22F8275A8AFB96B74 (void);
// 0x0000019A System.Void SuperTiled2Unity.SuperTile::.ctor()
extern void SuperTile__ctor_mB8480BC1EA19AA09C949341F9FA403C0D29DD317 (void);
// 0x0000019B System.Void SuperTiled2Unity.SuperTile::.cctor()
extern void SuperTile__cctor_mA40AD16A4EDAEB43F909F31DB3B5E8877D12C98F (void);
// 0x0000019C System.Void SuperTiled2Unity.SuperTileLayer::.ctor()
extern void SuperTileLayer__ctor_m6E696A06A988C2D9F8DE09E7DB27252ECA06E0FE (void);
// 0x0000019D System.Void SuperTiled2Unity.TileObjectAnimator::Update()
extern void TileObjectAnimator_Update_mD778E059F81276E92D5AC642D4B14421B6E9609F (void);
// 0x0000019E System.Void SuperTiled2Unity.TileObjectAnimator::.ctor()
extern void TileObjectAnimator__ctor_m416095FC7C6B02376EBCA73C07C86A7E4A5F7FB9 (void);
// 0x0000019F System.Void MegaDad.OverheadMegaDadController::Awake()
extern void OverheadMegaDadController_Awake_m24410793EB9FBC093B3C8469AC8ECEF909E97871 (void);
// 0x000001A0 System.Void MegaDad.OverheadMegaDadController::Start()
extern void OverheadMegaDadController_Start_m770D1AEE9943CEE902FC332D179384D25CE1F8F5 (void);
// 0x000001A1 System.Void MegaDad.OverheadMegaDadController::Update()
extern void OverheadMegaDadController_Update_m39AFCF1B0B01C5FB451806BD576D31924CF79448 (void);
// 0x000001A2 System.Void MegaDad.OverheadMegaDadController::LateUpdate()
extern void OverheadMegaDadController_LateUpdate_mCFDCD7122F7E0C0F9EA98BC6CCFB96F4AA9886D6 (void);
// 0x000001A3 System.Void MegaDad.OverheadMegaDadController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void OverheadMegaDadController_OnTriggerEnter2D_m4C37AA2126418375C2A58A0FEDAF5A06AB0C06C9 (void);
// 0x000001A4 System.Void MegaDad.OverheadMegaDadController::MoveUpdate()
extern void OverheadMegaDadController_MoveUpdate_mAE3AC7646E13F7DECCF224649F9BF5F057DD1C27 (void);
// 0x000001A5 System.Void MegaDad.OverheadMegaDadController::InputUpdate()
extern void OverheadMegaDadController_InputUpdate_m60FB810B6895C73FA20F5B76D3FF2088C481C4E8 (void);
// 0x000001A6 System.Int32 MegaDad.OverheadMegaDadController::RoundToGrid(System.Single)
extern void OverheadMegaDadController_RoundToGrid_m60E95416B938AAE4561D8BFB66303AB2E4FB80FB (void);
// 0x000001A7 System.Collections.IEnumerator MegaDad.OverheadMegaDadController::Drowned()
extern void OverheadMegaDadController_Drowned_mDB9569C7C77F6BE33B8B56A17356BB10476F36C6 (void);
// 0x000001A8 System.Void MegaDad.OverheadMegaDadController::SetOverheadCamera()
extern void OverheadMegaDadController_SetOverheadCamera_m66970B0E6B857B3D32C6F97DC71BA0DFE6E42384 (void);
// 0x000001A9 System.Void MegaDad.OverheadMegaDadController::.ctor()
extern void OverheadMegaDadController__ctor_mE913FF722254106AA853A4CFC85CA996D9FE4599 (void);
// 0x000001AA System.Void MegaDad.OverheadMegaDadController/<>c::.cctor()
extern void U3CU3Ec__cctor_mFFF75958A51651C3450FE092D5A73782A44ABCC8 (void);
// 0x000001AB System.Void MegaDad.OverheadMegaDadController/<>c::.ctor()
extern void U3CU3Ec__ctor_mC759ED9422CA4128D1563A1E47F882E9B3B3692B (void);
// 0x000001AC System.Boolean MegaDad.OverheadMegaDadController/<>c::<Start>b__14_0(SuperTiled2Unity.SuperObject)
extern void U3CU3Ec_U3CStartU3Eb__14_0_m4446E28C4FD9515B3AEF9CFC441747D0E53D4A41 (void);
// 0x000001AD System.Void MegaDad.OverheadMegaDadController/<Drowned>d__21::.ctor(System.Int32)
extern void U3CDrownedU3Ed__21__ctor_m9B1840C68AA02443082D4BB4593B8C2DDBC9C2AD (void);
// 0x000001AE System.Void MegaDad.OverheadMegaDadController/<Drowned>d__21::System.IDisposable.Dispose()
extern void U3CDrownedU3Ed__21_System_IDisposable_Dispose_m4356657D60E5593B70E506308D2484CCC65E9031 (void);
// 0x000001AF System.Boolean MegaDad.OverheadMegaDadController/<Drowned>d__21::MoveNext()
extern void U3CDrownedU3Ed__21_MoveNext_m5EB6A86333207B100483CC125B65CB0D18F4C192 (void);
// 0x000001B0 System.Object MegaDad.OverheadMegaDadController/<Drowned>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDrownedU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD91C1CB91A7DEB29CFD48D50B5BC5E486A12F7E2 (void);
// 0x000001B1 System.Void MegaDad.OverheadMegaDadController/<Drowned>d__21::System.Collections.IEnumerator.Reset()
extern void U3CDrownedU3Ed__21_System_Collections_IEnumerator_Reset_mCA29722363B203EF658DFC6D6D82610E82821BD9 (void);
// 0x000001B2 System.Object MegaDad.OverheadMegaDadController/<Drowned>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CDrownedU3Ed__21_System_Collections_IEnumerator_get_Current_m060EBCDCC94B24D967E22B5569D5BF7564394C0C (void);
static Il2CppMethodPointer s_methodPointers[434] = 
{
	AudioManager_Awake_mD7873A38A3ED577A313A05D24BF6511E59EDEC01,
	AudioManager_Start_m54C0A7ACBAB2F38052C6B900BBBC3261339662FC,
	AudioManager_Play_mABE51D919CA26C8386CF6823417ECC0CD9000888,
	AudioManager__ctor_m6C686441D1A1A223E4CF940A8EB0128535D603BD,
	U3CU3Ec__DisplayClass4_0__ctor_m67B1BB5E29B49389EC0244292A697D9F0DA398C5,
	U3CU3Ec__DisplayClass4_0_U3CPlayU3Eb__0_m3638A911493D9A1EDF26B6B317DFE476AE17D16E,
	npc_Start_mB1AFDA23363F546579A7E6873323A2D5C63B2126,
	npc_FixedUpdate_mE97A4988AD6BD8AC38E6726020DE31AD1178E7A5,
	npc_Update_mC570B39F9953A22E3167F3A8F615F01682DD0F46,
	npc_CheckDistance_mDF6325952BE7354D41949D4F13F7A10F59E9DDDA,
	npc_OnTriggerEnter2D_mBF94814C116B513A9404C9A3F1845023DC3E742F,
	npc_OnTriggerExit2D_mFE6208B9D9419D9F566A533CF8A55AC809CDFA52,
	npc_TakeDamage_m5C3CCE6D6DAC8954A2E1E8270228816AC2D1C97B,
	npc_Die_m7036D9B976055CEE21A68761C4840806336291D6,
	npc__ctor_mB9257C7E06300217C2CD78885E1E919DE6853075,
	PlayerMovement_Start_mB585552228B1908E44D3A69496598FB485F608B6,
	PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F,
	PlayerMovement_isRestrictedState_m0EA55B08E8A96C443BC31C0AC59A788A59508A4D,
	PlayerMovement_SetState_m64E859277CA239A4A4373B7FDEDD83A7D37909F8,
	PlayerMovement_Knock_mE574231AEE300149E504E871824DEEFABD785571,
	PlayerMovement_GetInput_m96688F49609587CAE67CE5C36BA60AF2E0B935F5,
	PlayerMovement_SetAnimation_m14F76EDA251DADFBEDBF145D29FBA6F3A0047458,
	PlayerMovement_WeaponCo_m7E38E79787A651A1F512F9E0CF42E98A2398D396,
	PlayerMovement_AbilityCo_mB527E62AFDABEA91E50C6841F440F178C87D971D,
	PlayerMovement_Knockco_m9FB0A3F5655B26872CF8F08DE922AAC0BA2CD9AB,
	PlayerMovement_SecondAttackCo_m35F4438A058472BE31F0537E3AA967A0F8E26349,
	PlayerMovement_MakeArrow_m6C032DB935BA42EA2519E406D324D47F60CEB830,
	PlayerMovement_ChooseArrowDirection_m55B2A6CDA1F6C27935AF11343EC1ECBFF7D87A97,
	PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F,
	U3CWeaponCoU3Ed__19__ctor_mF6774EDF96E29D4CF1DE6E59A385EF4733577499,
	U3CWeaponCoU3Ed__19_System_IDisposable_Dispose_m336B4209188172D5D2E7DC91BA51B13E1B53AD56,
	U3CWeaponCoU3Ed__19_MoveNext_m86432A4AEB6DF8EFA0E5152202A29EB30998805B,
	U3CWeaponCoU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE75159A118D1CCD0D21C44DA7A1BCCEFEED035B,
	U3CWeaponCoU3Ed__19_System_Collections_IEnumerator_Reset_m92D841A85EA7636FE60CE098FB0E509F112F57D3,
	U3CWeaponCoU3Ed__19_System_Collections_IEnumerator_get_Current_mFA45A158917C1D00D0414546D330B5C50AF98810,
	U3CAbilityCoU3Ed__20__ctor_m0176682625B409EBAF4204EB4659BDF4D41F4BA5,
	U3CAbilityCoU3Ed__20_System_IDisposable_Dispose_m7077FA91B9383678A31E38756736A526CF59A3FC,
	U3CAbilityCoU3Ed__20_MoveNext_mF7EE2A4FB49F66985E71CC997FC5CD96E78EE708,
	U3CAbilityCoU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1B45335C8F68A00353045AFA6267AE71F07182E1,
	U3CAbilityCoU3Ed__20_System_Collections_IEnumerator_Reset_m8E3B2891A317FB08FA139BD950964EAD077B40C2,
	U3CAbilityCoU3Ed__20_System_Collections_IEnumerator_get_Current_mE6CEEBAB1AB8EE0C1A7052E7E832277A232B1DD5,
	U3CKnockcoU3Ed__21__ctor_m0683E9332889422FD23C62E94DCA9758C00552BA,
	U3CKnockcoU3Ed__21_System_IDisposable_Dispose_m3209EEDB656EB5486A1CF3DA9F24C350C4707499,
	U3CKnockcoU3Ed__21_MoveNext_m6021B528770FB5799F99505541DB9994402149B0,
	U3CKnockcoU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF02042846E4388A7D23AEE7B092211A67F45B9B,
	U3CKnockcoU3Ed__21_System_Collections_IEnumerator_Reset_mFB645C76CFD1ECF92A92F52F985A9D3459F302A5,
	U3CKnockcoU3Ed__21_System_Collections_IEnumerator_get_Current_m54FA11747AF9038FFEE415DA8CB553F911104081,
	U3CSecondAttackCoU3Ed__22__ctor_mF3BB48058B987D2323D3877A4B73BB38EF8FCA12,
	U3CSecondAttackCoU3Ed__22_System_IDisposable_Dispose_mD085A289E795223BE81314ED9425C51047B43990,
	U3CSecondAttackCoU3Ed__22_MoveNext_mB407F42AA9D9E69717C17283F5017F368D8AC39A,
	U3CSecondAttackCoU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m566C8E4E0AABFF749D23CB669FA3AE323A002871,
	U3CSecondAttackCoU3Ed__22_System_Collections_IEnumerator_Reset_m6FAADB162A5C47DAB3951676C0EE2937468C2E00,
	U3CSecondAttackCoU3Ed__22_System_Collections_IEnumerator_get_Current_m23AC4D1FAA25D82B0CE7ACFC49A43B7DC624AD7D,
	roomTransfer_OnTriggerEnter2D_mBE37A0ABB0DA00B385750A3D9A5DCFDFCC97CCE7,
	roomTransfer__ctor_mD18C81B18780C9B5D6FE088B56CA0D1388DAB9C3,
	CameraMovement_Start_mCBBF91D02D609BDE618205B77F07B1FFD53C0A41,
	CameraMovement_FixedUpdate_mE86FEA62BC2F22AE9C4399706D27E422D2B0F95F,
	CameraMovement__ctor_mD0F05084B2475AA8C0A35AFB6E4C88D0D6ACEAAA,
	EnemyAI_Start_mC5494C97D0214D04302BAE4F82F48D13AD0D25BE,
	EnemyAI_Update_mBB23B2D10604094D0D5BDD6D02A5C51DFCDFAA84,
	EnemyAI_FixedUpdate_m06285F83619449289F3118F098D1DC94D8E7BAC3,
	EnemyAI_MoveCharacter_mBDD642AA326776490AB68E704664A26DCED9A3E1,
	EnemyAI__ctor_mAF4FEC29EA4ADF864B4641448BFF55028ED4B3BC,
	ManagerAudio_Start_m58F53742043C6FB113A6FDBEF9AA8F695D5EA63C,
	ManagerAudio_Update_mEA1A1FD2AFE74F2DBDCDA1D97F2EC9CF71B3A0C2,
	ManagerAudio__ctor_mBF0B8444CF326C468C1FD173AFB4445AED18CAA6,
	musicPlayer_OnCollisionEnter_m49EDD18644A75557DFF41063B174A6627B7C4491,
	musicPlayer_Update_m69D765DF35CF448EB8F53194E8DDA7A91379833A,
	musicPlayer__ctor_m18CFE91C5BA1C322A4167D8AF57CC2328D047277,
	Sign_Start_m28ACDB82DAFDB83009226314C5465C0581405830,
	Sign_Update_mA0CA73670A2FEB8087F4A501CE3C717B26D35ADF,
	Sign_Text_m41A10CC054BB0346C4914062E4140A5A530768A6,
	Sign_OnTriggerEnter2D_m5D5FF2EC7F3464DF2F203241888C63D312A4856D,
	Sign_OnTriggerExit2D_mB5C708ED74620FA94EAC25117C91327AE61840E1,
	Sign__ctor_m348D33DD7033FBC7F73A77F8094FB8CCE1B1A2C2,
	Sound__ctor_mEA0B0D2FBD514F91C21900B0BB8679CD78843FCD,
	Text__ctor_m509388F43D08C8B93FED709EFC4DE8A9AFA57CA3,
	TextManager_Awake_m2A88556742B867A4738A44434F4744B380CFD969,
	TextManager_Update_mAA5D92CFBF49490445ACC3178D4EAA55B29CC51F,
	TextManager__ctor_mAA951AC010A74DD21641D5263C45DF7969B419AB,
	TwoTextDialogue_Start_mCE142EF7B83E90382C14BF4CE46BD7B36EF2F57C,
	TwoTextDialogue_Update_m74B8C9FE08AFFAB4B9AE3BBD20AFDDA1C269A39B,
	TwoTextDialogue_Text_mA491C82512C0DE8B6F1463CCB4EF9113ACCFD374,
	TwoTextDialogue_OnTriggerEnter2D_mF330CE95BF48C46E8BBAED6CEA6D08983C288401,
	TwoTextDialogue_OnTriggerExit2D_mEF9A90E1A34E79C1496E78F2D834EF3529985D27,
	TwoTextDialogue__ctor_m77C5C9C18BA9FF5660C3BB514F166E618D612D02,
	Enemy_Awake_mF268033197059561A4A0BC3E5F6B83B50D29C861,
	Enemy_TakeDamage_mD2F5D25BECC021BB923DE3FF0DAE69800DA36463,
	Enemy_Knock_m37EA280E95C2E2F6E6D83080C96235C6404A2278,
	Enemy_Knockco_mD90ED67E0DA4A148DE3CB5FA33EB2187AAFB2F7A,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	U3CKnockcoU3Ed__9__ctor_m2F874F51BAFBD6E0A502C95F49D0FDE4B00B3596,
	U3CKnockcoU3Ed__9_System_IDisposable_Dispose_mA9E234FEC09DB70E40A923A8EE315EE24373BBC3,
	U3CKnockcoU3Ed__9_MoveNext_mF71F4401A13B5EFCC353D099092613955C94480A,
	U3CKnockcoU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC2DEAF9142B8BC177D269B901C8ACE2F35A5B64,
	U3CKnockcoU3Ed__9_System_Collections_IEnumerator_Reset_mB44AF6F2AE44CC8C9F91C86FFEF604A906770458,
	U3CKnockcoU3Ed__9_System_Collections_IEnumerator_get_Current_m3412A2D0FB64EECBA79C5949DCBD517BE46F9176,
	EnemyHealth_Damage_m16925CD39AEBFCF4C5C623935C47AB480ACC8D2D,
	EnemyHealth_Start_mD01F3D64A408764E38262C86167BA12B65D4A5D7,
	EnemyHealth_Update_mBCBBEB935216A5E61B4F9A5F6833A09301C0E8DF,
	EnemyHealth_Die_mA8BDF6A041B21ADF726398BF92C35859405C552F,
	EnemyHealth__ctor_mF9FFC7A91A2AB12182655557BC05309E64E17AFE,
	SimpleFollow_Start_m849B7D8CBBA638D25272E9E9B12F7B36F50E4081,
	SimpleFollow_FixedUpdate_mCC8B6C95B31AC422B9737B2F1A13EB60B11FA9F7,
	SimpleFollow_OnDrawGizmos_m7298DDF5ED22E11AC91577F12B7970CDB2E49208,
	SimpleFollow__ctor_mA4FB9BF2B0FA9E339FFAD7D397CE729582098BE0,
	Slime_Start_m2E8A8239589581EEEA23A4C58CD61D7E7670CCD3,
	Slime_FixedUpdate_m8F95CD034596F164A1282C83973D5A2A9C2ECEE8,
	Slime_CheckDistance_m5C6C5AFADDF51C43BF457D8DD1BFD3693318722F,
	Slime_ChangeState_m7CF151F1F99A0527A15F0AC51116B20504F2C13F,
	Slime__ctor_m24052CCA4BC1BDA15F470CFA7CDC7B83EC726950,
	RoomTransfer_OnTriggerEnter2D_mA20E0353AFDE2F43936A15B606B1E97DF4692E48,
	RoomTransfer_LoadRoom_mAAA9AF0BB9701FE7405EE6DDD59C9742463328D5,
	RoomTransfer__ctor_m5C6193E95547CA97DFE50BEE194447D7EFCCB04D,
	SignalListener_OnSignalRaised_m1B74919A09DAFBA432D8A9C4E509DF3601FF5A13,
	SignalListener_OnEnable_m45048A70F0BE1DB10FDF07822E3C134CF67AEC1D,
	SignalListener_OnDisable_m9AA18CB6C74989EFB0E1CDAF4F376A5A48C0A607,
	SignalListener__ctor_m5138552B7DDD4913E17A3EE1FF6BF6FFC870B506,
	SOObserver_Awake_mD1AC644B102556C6B62DF8D5E3ABA78DF2A25D88,
	SOObserver_Update_mD9AEB53FD048C6E4B61F0FC1EB7C2CB6E0829474,
	SOObserver__ctor_m4B04E70165BD725C7387D95921FE08BB15C83499,
	AnimatorController_SetAnimParameter_mC2C9C3AE4E5DA433CA1956DF1C4F665028F25010,
	AnimatorController_SetAnimParameter_m1FD40940C21357C4297966166AD072DF06523572,
	AnimatorController_GetAnimFloat_m9F2B9538A13A88B77064AED89A279DEA23F59E18,
	AnimatorController_GetAnimBool_m0E2BA364EAF255DC62470D05770500EDA7981D27,
	AnimatorController__ctor_m1E1680F05697619E47C9C78239B71FF981B4B85D,
	Damage_ApplyDamage_mD732480DDBBBC4F6FF50E444D2AF00517B31A1DD,
	Damage__ctor_m448736D790344E455BBE20282D3D73C91E6E2CAB,
	DamageOnContact_OnTriggerEnter2D_m5483A2905317AC29E8F0BA39C9987EF9742A2061,
	DamageOnContact__ctor_m287E9AFB1CD7DCA3E55E99B98A4E9989FAAD8AD1,
	DestroyOverTime_Update_m9BD3239A91EEC7E53820274885C47DEFFE663495,
	DestroyOverTime__ctor_mB8D8C27A30DE70D4609B2EF8B28E7C8C99EF7C14,
	DisableColliderForFixedTime_Start_m827F0060D32EB6D08169463022C88155F3A5EFBA,
	DisableColliderForFixedTime_Update_m3538F964283ECE3D2D58BEF05BE1DC13E2033CAC,
	DisableColliderForFixedTime__ctor_mA35F9E34BD782E00758317CA9A552E5E515BD33C,
	FlashColor_StartFlash_m2C05E76FEF04171D863CE55398C4177B39081D4E,
	FlashColor_FlashCo_m07901743C6FAEF83D1AB61EF87A1D1AC9BB8B387,
	FlashColor__ctor_m0C58F482DF86F2C859420C213E24C2B4E9DEA9F1,
	U3CFlashCoU3Ed__6__ctor_m282505D8A6C582DC83B04A78C33D051B74AB39E1,
	U3CFlashCoU3Ed__6_System_IDisposable_Dispose_m88C743246933E0B24A5BB46D1E89032441C80EA7,
	U3CFlashCoU3Ed__6_MoveNext_mE8FA91D54BA581D5D1FD36750824E95996BF8F3D,
	U3CFlashCoU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m842202646B2172656DBB62CC371E6D6665ADD6A5,
	U3CFlashCoU3Ed__6_System_Collections_IEnumerator_Reset_m4AC4B01410D3911850D0217901498DB1FD07A134,
	U3CFlashCoU3Ed__6_System_Collections_IEnumerator_get_Current_mC61263B1374CD2BE6E68CBC0148D574A19638B3E,
	Health_SetHealth_m1707169CFAD418E9F05096B773A75243DAE6CC5A,
	Health_Damage_m54CDE5CB048F523E73F8AAA8D20EFC6135EA6112,
	Health_Heal_mEB16314F9C9C09E62B8BAFBFAB5A8CAF35F6284D,
	Health_Kill_m2FA3BE3A4FBAC904F1E6339223571D2B39BA3479,
	Health_FullHeal_m65B1B1106C909C1EB5BFEA559CABC33A62C0EDF2,
	Health__ctor_mD50C73D87211EAE260B38B2936F01722E31B9416,
	Interactable_Start_mCD3DB76ECF5B01A9E9AE400DFC3CA7ED95B89844,
	Interactable_Update_m17566E103461BED9EAC7E69D2B6BF5396FCD392A,
	Interactable_OnTriggerEnter2D_m404A970A7992481E01319B79EEE8B957A43A4CC8,
	Interactable_OnTriggerExit2D_m84E116A69EEEA06B7846C18D96F11A80403FEE0F,
	Interactable__ctor_m14A127716816AD709C7F0004EEC27A89E8EAE8D3,
	Knockback_OnTriggerEnter2D_mF928BA361B80E1DE2D3146855D185F9F04B06D5C,
	Knockback_OnTriggerEnter2D_m9642D05568467C647D0E961F355233E3676FF568,
	Knockback__ctor_m2B8B481F28FC4A33133BD970C8F4E86F809DFD40,
	Magic_CanUseMagic_mDAF8F3311138150B1D38DF83BF1D276ED7E69E3C,
	Magic_UseMagic_mF35835D040DFE07561DA069EDFE2AEA6820FA849,
	Magic_UseAllMagic_m1F8E5492972F0AA0AD2578C936B1B0D9845DEA2C,
	Magic_FillMagic_m2C2D39EA8224F22050C35B486267A31C5CDE64AC,
	Magic_AddMagic_mD024C5774B28AF2B444A2626FBD3513E6D7AC008,
	Magic__ctor_m886D6B87C69D82E60EB1B81D082A87F220F13697,
	Movement_Motion_m0F1ADF4AD143A45A80F9033D6C319E1002B06277,
	Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB,
	ResetToPosition_ResetPosition_mD4AACB593379DF026517754BD2A197DE9067F7F7,
	ResetToPosition__ctor_mFE9F0AB787C347159391FBF38AB58A137C985EBA,
	StateMachine_ChangeState_m44BB5B80BA266F2B761B2C76E19DABEC1A855628,
	StateMachine__ctor_m386AC4CE9F753E4EF79C5C6AEDD6A01F8E696F86,
	Arrow_Start_mD5806C542999F8C5FAB6FE9CD4CC6CFF772277FC,
	Arrow_Setup_m17A3F0749031623EE7FDC13CACBB0C6A87C0FC8F,
	Arrow_OnTriggerEnter2D_m10D1F66E752F11F60C8E464781BEBB7B861B8511,
	Arrow__ctor_m5142CD3440C6AF4782993D2409B9FD785577892C,
	Chest_Start_mA640C6F089CDEB1A6E0EE8F487A12BE16790D136,
	Chest_Update_m4FA8556D3034321A01625325CE1F0612CB333866,
	Chest_DisplayContents_m5EA75D812183C2E50A02A0B7E1556AA878F19DC2,
	Chest_OnTriggerEnter2D_mEB3B4F967EB4C6DCED8B9CD315B3370791CB1E90,
	Chest_OnTriggerExit2D_mBFD1A87076CF7A2BDB4626FFF761CF4991B4B0B4,
	Chest__ctor_m107B91B4F8435DE9E7C45BC7C0707B14BE3632D2,
	DungeonRoom_Start_m33E39953C7E1887D67864AEC93CB2832F62A8185,
	DungeonRoom_Update_mAC54F9FA7061DA02F3436777C000E9C765566D0F,
	DungeonRoom__ctor_mF5CC98038DC803795AA6F4E6E8695C3AB5817878,
	LockedDoor_Start_mF9E231D813384C7B593A8DE7A9232C68A90A5B80,
	LockedDoor_Update_mD37290D1B43BEE1A0150AABEB80E85B73B161B1C,
	LockedDoor_PlayerHasKey_m099ADFA2363CD73DE88FEB456752377FC5616390,
	LockedDoor_Close_mAFA89F895038F685544D6332FB1930FF164D71D8,
	LockedDoor_Open_mBF053DF9B651BD4C0361D96947279E903E332256,
	LockedDoor_OnTriggerEnter2D_m5A50B7A13897A610C54FAC1E3A4581161F0DC66E,
	LockedDoor_OnTriggerExit2D_mFB4DFEC33AE0CCE45615B64EE0C7FE5C94C04896,
	LockedDoor__ctor_mE91B7B4C2A537472B90A08E48DA07C13465015DE,
	pot_Start_mA148AB2581B13941B179F0751480A5990E57ADA5,
	pot_Update_m9ED02433892E66A6444D61903B88AD689D2DC0C2,
	pot_Smash_m14F36003257FD59A4875086B575FC3D2C33D0F9B,
	pot_breakCo_m136505C681494DEFE0A2FD7B11005EFF45467773,
	pot__ctor_m6D7B09EC649D55BFA44E817B638A7081C23F9703,
	U3CbreakCoU3Ed__4__ctor_m4C5D4FA0324868E76681FE2DD7569F941DE6ED2C,
	U3CbreakCoU3Ed__4_System_IDisposable_Dispose_mBE51471B28DFE63D563EB50A7557E492221EBCB7,
	U3CbreakCoU3Ed__4_MoveNext_m6E0B48E1D5E17484ED26691C762B248AC8A502E7,
	U3CbreakCoU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4532DAB64070BD2B8B9F4D6D27B2E56D29695783,
	U3CbreakCoU3Ed__4_System_Collections_IEnumerator_Reset_m3907F5C4D0C4E2A92262694229C6B6FC3A64A405,
	U3CbreakCoU3Ed__4_System_Collections_IEnumerator_get_Current_m1077B64857782AE047E326DB80336EF7C7D3215B,
	Room_OnTriggerEnter2D_mF4A96BCD7F1DF9BBEAE2DB4CAC5CF7F7FD414CFF,
	Room_OnTriggerExit2D_m6838867165439A4FFC1C8ABAE423E2CA8D0A5659,
	Room_RespawnObjects_m396725255D07E240F28D95675DDADA03BB26C5A0,
	Room_DespawnObjects_m890A4BD4501CFCA8951DA55DA450DB101B390A63,
	Room__ctor_m970729D9D91E49A7173A5AFD56CC3D9047A7BC8C,
	SecretTree_OnTriggerEnter2D_m0F7895AB99D452626E01A2329C0B3E5351BFF686,
	SecretTree_TakeDamage_mA29F0E7A03476429236BF0D11199CD201E85F5D1,
	SecretTree_Die_mC21BCA6CD4D49DE6C0926AE7A96263385B94CBDF,
	SecretTree__ctor_m1F3E6FF89EA258E4A9368FFE46C61F057614D053,
	ContextClue_ChangeClue_m88DCC5066385F2ECB090AEC7F5ACC75032DE6E14,
	ContextClue__ctor_mF7026A5A6FA6EF75187C49F2E0D936B6688FE916,
	HeartManager_Start_m485EB9FCB71F8681C7647CFC9518AA2CF065A673,
	HeartManager_Update_mDC975DE00FAC15324F6221006E3980B96A0C91A9,
	HeartManager_InitHearts_mEB02789C4112E74523358260B78129E5C784623B,
	HeartManager_UpdateHearts_m030A4653A9C088D21045EA686833D124A2F6B4B7,
	HeartManager__ctor_mDB60E8246EC34BAD5C9C8629B609FFACA518A1E0,
	PlayerHealth_Start_m78FD812EF2B87E9EC7A405A1BBB6ECB27BFF3589,
	PlayerHealth_Update_m4ACD2FDDEBE8DC21C71BB853A975D03DD65061B3,
	PlayerHealth_Damage_mF01D9007EC116CE828798CE6EE3AA2F67DC2AA09,
	PlayerHealth_FixedUpdate_m021918574F91205FF7F06F34FC6EE3CBD44A3B91,
	PlayerHealth_OnTriggerEnter2D_m5D21A0886DDA60146BA4B5ED0C2A0C4EE0D20766,
	PlayerHealth_TakeDamage_m21F4EA70549D145406E078F0543A934DE498FDB7,
	PlayerHealth_Die_mBF97B09DFB17C4BBA27719DD2FFA1F4221E8CAB3,
	PlayerHealth__ctor_mE9AF3CA69205909E44287664BEAE503EC43875F1,
	PlayerHit_Start_mAADC6279A20EF90C76AD46C93087C1BD5917A775,
	PlayerHit_Update_m77F7F12F7AF4EB3207D97B55D37A9785A7F17623,
	PlayerHit_OnTriggerEnter2D_mAA5CC82FB0294234AFF12BA7EA8B10C870008A6E,
	PlayerHit__ctor_mAC6AED11C78C91DEFC68D28B2166810A437712CA,
	PlayerMagic__ctor_mB570018C5F46BE31069F30B3575CAFB5A860B771,
	PlayerMoney_AddMoney_mBBECA2F4BC35A1F2D633A86A8C851CFA08353F94,
	PlayerMoney_CanAfford_mF27A39CA6F85E0DF1B9770CDCB4125831BF1040F,
	PlayerMoney_SubtractMoney_mA0AA22DF3025C7227D75EC9205934336F40833B5,
	PlayerMoney__ctor_mCB6939DD39E376FBA7297453EE4469A38FF23FBB,
	ReceiveItem_Start_m36C94AD06AAD471AE04C0C331FA21354B7D719DF,
	ReceiveItem_ChangeSpriteState_mD7895EC426CFD6CE6C13889875DDEEAC77400A5E,
	ReceiveItem_DisplaySprite_mAA991D24E7EDA86833632C2F659390211AEF448F,
	ReceiveItem_DisableSprite_m250A49E1DC0073E7F32627AD536CC86D0CD443C0,
	ReceiveItem__ctor_m5DF06C5377F562FE12E670D203439B1FC1A897C2,
	ResetPlayerPosition_Start_m1E81F0FC838BBF264705265BD4104697979B722E,
	ResetPlayerPosition__ctor_m559A10FF1AD24B8BB648925D3EAC192B00810E3C,
	DashAbility_Ability_m6E9AB9604E22095FB88BA3CC374373244F721AEB,
	DashAbility__ctor_m0180D7F255A74C2CED77A67B083B2663BAD1CFB9,
	GenericAbility_Ability_mBDE8CB83299782CD949851F8FB7120974CEB5424,
	GenericAbility__ctor_m5025FD2EBE6F90A67261660C64E5B73A21920A19,
	BoolValue_OnEnable_mC68A4CE5236752E9E4A48123D060159ECCF0E4A1,
	BoolValue__ctor_mCD5FC467EF725EA43475F255C89E7261F52C331C,
	FloatValue_OnAfterDeserialize_m2880B383D890B10CCC287EBE19F71DF59627B9F8,
	FloatValue_OnBeforeSerialize_m7FE75573E854C9D12B0B268713C8EC68537A165F,
	FloatValue_OnEnable_m764380CE53BF298A66BADDE935C008BEA2AC9D2B,
	FloatValue__ctor_mFF3D035592CC458A8182C574A2B60C04BD3A0F83,
	Inventory_AddItem_mB6001585F2C522F10D30A42956CE8DC9230626D8,
	Inventory_RemoveItem_m7E9029F971D3EDB4966EA7474A5A9D6A445FE2F1,
	Inventory_UseItem_mC08A762F9C9DECD654C916E327C0B264C44321B6,
	Inventory_IsItemInInventory_mDE01027E651A29396F0DA0AB93DE45C934F8E5B8,
	Inventory_canUseItem_m4A1006D08FC7549F59893A29EB0D725E959FBB60,
	Inventory__ctor_mF2ACBF005FF40F23F68AE8E9E416A4870EC4B27C,
	InventoryItem__ctor_mDA2BEAE4ED6F565E6B7762D8A33C6D12BB7FD133,
	Notification_Raise_m2180F845CF6C9122CFA662C7755BC8BB4AA1D49B,
	Notification_RegisterListener_m2894970C218C125F6D53721954B4827B7C9DA6FD,
	Notification_DeregisterListener_mA5B365A057F340F76AA801FF6DEC1F4EB8EB4B89,
	Notification__ctor_m244DF5A7BD448937CC8718BB814EDF8E83068306,
	NotificationListener_OnEnable_mF482FC12885D30C9FC3B475177486EFE85416656,
	NotificationListener_OnDisable_m27A0F21D7F0CCA70D0CA9102FBB5B2C84B9FFAE4,
	NotificationListener_Raise_m6865C5819AB4C0DEDD81A1FA7263F94ED25310E6,
	NotificationListener__ctor_m4CBC45D129DE2652E3AAA8A89698175C099C315C,
	SignalSender_Raise_m360EC164A64F84E8CC7E49DDD41BD6400FB6BB71,
	SignalSender_RegisterListener_m2F9F47F5F5404E9752F2C0A72108DFEF4BD8C009,
	SignalSender_DeRegisterListener_mA4BD75C6227B06C903AA31EC3A8829521786AA63,
	SignalSender__ctor_mA64E0F931DCEF44D3575861DAE1E06B8B6AFB9B8,
	SpriteValue__ctor_m45BE209A5D3F0E115A0B102A999DD816C0794580,
	StringValue__ctor_m52E5D1DE2D80FA25997E21578C2B6DF3765FFF23,
	VectorValue_OnEnable_m23398C0A9C5CCCCD9770A9B94AC764610D1D8693,
	VectorValue__ctor_mF49A40A277536A52D56535D6A3C7BD9B039E9EDD,
	Dialogue__ctor_m76C11FD80AFD4F6BE87E30C737879EBB5A1D724E,
	DialogueManager_Start_m612B88A606E0F326778C59A3207CBF0548F5C6A5,
	DialogueManager_StartDialogue_m2674465637758F34587FAD6CFF453059F6835B0D,
	DialogueManager_DisplayNextSentence_m14A583FBA1E346E5B4D772A9E022D6F78FE6A383,
	DialogueManager_TypeSentence_m74B6A598B53C2B9725C6343F801FD272F6F60CE4,
	DialogueManager_EndDialogue_mFF5F59FF3A5EC3A9B41AA3B92580371831DC80FB,
	DialogueManager__ctor_mAA8FF2D1E586DBF3472DF58D3B1BF8F4FA03EE1E,
	U3CTypeSentenceU3Ed__7__ctor_m9DA545853895CDAE646DBA7EFF77CAB01870125A,
	U3CTypeSentenceU3Ed__7_System_IDisposable_Dispose_mBB525A4327EF1046B3FECA28648E6B831051994A,
	U3CTypeSentenceU3Ed__7_MoveNext_m08234224360D24514091B3424CAB12840B46203B,
	U3CTypeSentenceU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33A483BEE1B98A623207D8191EDA472659750BED,
	U3CTypeSentenceU3Ed__7_System_Collections_IEnumerator_Reset_mFAE38B6F7B55A3EAA896B235FC31ECE3D8A63A01,
	U3CTypeSentenceU3Ed__7_System_Collections_IEnumerator_get_Current_mDF5ED2D5A6FBBBA1248735893AD2A53E944050BD,
	DialogueTrigger_OnTriggerEnter2D_m8588DF9556738DD26CE50F7A21B2ECADC9391C73,
	DialogueTrigger_Update_mAC78DCF0C9F328F1620AFA0848780E76899B58DA,
	DialogueTrigger_TriggerDialogue_mF018F9EE2910BD5138B9D3C6D993713E0B2A271E,
	DialogueTrigger__ctor_mE5F20AA50727E901ED31CC812C825F74369068F3,
	NPC_Start_m20C2D17950CB234A91905C91CCEADA0870B913B9,
	NPC_FixedUpdate_mC4F9FFB1FAE5068B9E3C89D101F9CF3DB2790E50,
	NPC_Update_m385369B628E634D1BB4300956637169CBAFDDEAE,
	NPC_CheckDistance_m3ED78ACAFD6CFC277DE06B2574677601656F0EE6,
	NPC_OnTriggerEnter2D_m44B8BDE947AAA30903A6336C7B8C49331EB49F4D,
	NPC_TakeDamage_m7CCBD2CED11B8A59C6826B7FD04D10E3E4F08DEF,
	NPC_Die_m20C03EAC0EB880EACB2D23E7EFE2BBF82E6B969E,
	NPC__ctor_mC9AA4F3CBCBADCB1866559D68B31E231A879CACE,
	testdamagescript_Start_mF235A713B9A7330FAB7916E2564EB11557025E4E,
	testdamagescript_OnTriggerEnter2D_mB6D3771B834C32B001B03425869EDA04279734E6,
	testdamagescript__ctor_m3C05CECDFF2EB8AC2652E85907AA10675D9D5968,
	testenemyscript_Start_mE5A52C67068652D3B5CACFF6E0D600C0D8677593,
	testenemyscript_FixedUpdate_m5AA2136C7BAAA6D856B15E85990040517CAA78CC,
	testenemyscript_CheckDistance_mB76CCAC7CB8168BBDEE9CFBADDF2C75115217C5D,
	testenemyscript_OnTriggerEnter2D_m0F4D100CB7532A42EF3CA8008C1881B9E669DBC1,
	testenemyscript_TakeDamage_mFC446BB9ABBB0ECAB59CF10988CA42C1DF3DE3A1,
	testenemyscript_Die_m9DBDFB8986A262BB7A3B7B7444F97C6FDE3C6930,
	testenemyscript__ctor_m5DE8EFECBB62360491721050B27686950E683DB1,
	AreaNameController_ActivateText_mFE39B39F19D3DA53720FD5C948D210D88D9F85EE,
	AreaNameController_NameCo_mCB01D701882E7B4DE795086037035E22C1310D2C,
	AreaNameController_SetText_m8D3B5462FE9F09AB1A22A6CD25F1455137A87CC0,
	AreaNameController__ctor_mEDD56E43DDE2D8513C1BA2D0593D9A5B2B5024CF,
	U3CNameCoU3Ed__4__ctor_m0295877D75B0907AB6E571ACF7EDFBD4C62C510A,
	U3CNameCoU3Ed__4_System_IDisposable_Dispose_mA2CE24FE422BB68D8D07A65F547562F8A0C4CCDC,
	U3CNameCoU3Ed__4_MoveNext_m12759D644E9217F5D73D9BC1C7E45A2E03E91A65,
	U3CNameCoU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF84216422D657C1B0CA4B2D9DB13DC626D5632A5,
	U3CNameCoU3Ed__4_System_Collections_IEnumerator_Reset_m466BA511DD3CA1F9BF5F805759C3A2351DFA8667,
	U3CNameCoU3Ed__4_System_Collections_IEnumerator_get_Current_m334CB1DAD5F575544F8E8E9F879F7A42DF38CDDF,
	DialogController_ActivateDialog_mED76DD3396FAFEAEBC9599EC84B88D1E4C831F6B,
	DialogController_SetDialog_m2A4B2A42BCF34B6935EB940A27823727BFBEF6C8,
	DialogController_DeactivateDialog_mBC8E5901012CD480BAD3251C37DDE3F1113E445D,
	DialogController__ctor_mEA038FA1BA7F61E62633EFD61524922F13021A8B,
	GameMenu_Play_m9A8D1B17564609C24D569CF15090387D1064F05E,
	GameMenu_Quit_m789B754A520ABA9AB03273E6A3F5D2378CD40CC3,
	GameMenu_Credits_m81C707BB36BF2FC1A5B2879FA7E800EB00269787,
	GameMenu_Back_m533ABED6145D47488CBE1F585CCEA2642868F385,
	GameMenu__ctor_m9D09CB0FB4C667FFE84AA5353BB107A3D4DCA47F,
	PauseManager_Start_mCC82D1C26D42EE03E43E38CFDD89ED3B69433F06,
	PauseManager_Update_m2C800AF811359C3E56E97FC30FF2CAA99995E337,
	PauseManager_ChangePauseValue_m0957F6A751D07AAD686FE7636AE8C0CB6684F270,
	PauseManager_ResumeButton_mFACC8973E966F822E4E0A33E3B887A59A538A58E,
	PauseManager_QuitButton_mB47492FD397629AA540D735EEB1B15BE28D32E43,
	PauseManager__ctor_m545310BC21D8C0D70A026984679E2439BC4B92FF,
	CollisionObject_get_Points_m6CF7BD37D0508F232C68B0AC035CD4E69CB82BB3,
	CollisionObject_get_IsClosed_m580920705D3198F13115F60BF59EEC712DA87A15,
	CollisionObject_get_CollisionShapeType_m47C2F59DD1A4C0CF3CDCEE73507B66D95C83ABEE,
	CollisionObject_MakePointsFromRectangle_m796381DB2CC9F74BED762982856F1D58BC88A8C0,
	CollisionObject_MakePointsFromEllipse_mD0CDE342967AB9961D5DD10A1F5F98E0D408BC8C,
	CollisionObject_MakePointsFromPolygon_mCBFDABDCB0FA51C5338FC9A3356330F148B28CA5,
	CollisionObject_MakePointsFromPolyline_mC5D6EE4A9B33DA76CB4CCF4F7E287359F8B58589,
	CollisionObject_RenderPoints_m3898C52CD076ACA30F62EE4EBAD968FB8D26D802,
	CollisionObject_IsometricTransform_m71A1212C43A79D571A9223047185D5C757F9D1AD,
	CollisionObject_LocalTransform_mF3343FC234107796B33E602FA4AFEC1D2E475F35,
	CollisionObject_ApplyRotationToPoints_m18C718583597A2287CBD079A02DBF8F306AD2F3D,
	CollisionObject__ctor_mF8D1D18A71C70EAB4AA711E94F09BC79E21ADED2,
	U3CU3Ec__DisplayClass22_0__ctor_mCEAD773FAAE0268FD86A984B99A2624E2C4C17B1,
	U3CU3Ec__DisplayClass22_0_U3CRenderPointsU3Eb__0_m04DF1A4EE79361390CC9C5B389FF5768A7F628AC,
	U3CU3Ec__DisplayClass22_0_U3CRenderPointsU3Eb__1_mF55BB46C57BEAF902DD1384B61949F94488094A9,
	U3CU3Ec__DisplayClass25_0__ctor_mA796A7F0E743361BDDD1E65CF70CE4665CDD9C3A,
	U3CU3Ec__DisplayClass25_0_U3CApplyRotationToPointsU3Eb__0_m56E27B156516C338BA4F198F45A233FBF069ABAC,
	U3CU3Ec__cctor_m21E7EAB3B1683DE75A5E68A7C1771CC076294FD8,
	U3CU3Ec__ctor_m54FCC71CE2FC22C9840D5E1B9283F775AA12D420,
	U3CU3Ec_U3CApplyRotationToPointsU3Eb__25_1_mDE5729996558DD5930C818770E43E1BABE390D9F,
	CustomProperty_get_IsEmpty_m978AB75E34F297B1CA82E5E5AA42316868C74EE0,
	CustomProperty__ctor_mEB3E18A1CEF4F7EF340B0CA3D964263149A6137C,
	CustomPropertyListExtensions_TryGetProperty_m610D3ACC325AB44067452C515DD10062DC01F9AA,
	U3CU3Ec__DisplayClass0_0__ctor_m10A05D207F27190442736DF43F8293E105C95F6B,
	U3CU3Ec__DisplayClass0_0_U3CTryGetPropertyU3Eb__0_m0BD32B119517C2B1A3EAD83EC692BCA1A763C14B,
	CustomPropertyExtensions_GetValueAsString_mE3789EE7A117E66D8FE55E977B31F18FB7480346,
	CustomPropertyExtensions_GetValueAsColor_mC872F887E4560B76E54E12FC539BB7AF618B4E37,
	CustomPropertyExtensions_GetValueAsInt_mCA843CA55CE6C08B5C1621B71EDC8010E32A9499,
	CustomPropertyExtensions_GetValueAsFloat_m3183E8A5D90D817BEE59EF0F5BA438B532D63540,
	CustomPropertyExtensions_GetValueAsBool_mFACA71E29D4012D9680FCD905CE064F0441CA334,
	NULL,
	NULL,
	NULL,
	NULL,
	GameObjectExtensions_TryGetCustomPropertySafe_m5F5E5234B522E79EF3DC4BF7EF336F04A64E774D,
	StringExtensions_ToColor_mC4A382B8EBE0EB3814C58E82CBA3C9A29556AB13,
	NULL,
	StringExtensions_ToFloat_m43913E60F4BC0CB3F25B5F58E76F99FD262FE170,
	StringExtensions_ToInt_m6D40614BFE9C46CCAA31108B53F17E0C5B00A6EB,
	StringExtensions_ToBool_m87339C631EFBEDFEF969558D94869FFE57A9B6F5,
	SuperTileExtensions_TryGetProperty_mDAF7C872C57BFAAD0ADAC8D1E441A6CFF717ECBD,
	SuperTileExtensions_GetPropertyValueAsString_m83D3B395862333224EAFB46EBF43FB8C2C828EAF,
	SuperTileExtensions_GetPropertyValueAsString_m643DC10D0EA82D87CA2E0DEE4C2FB0C652D0CBF2,
	SuperTileExtensions_GetPropertyValueAsBool_m37C447EF97E56A3D152BD60D7F948CF666971847,
	SuperTileExtensions_GetPropertyValueAsBool_m69DBF9E59581BDF7FC26AFD307E139B22BAA0E64,
	SuperTileExtensions_GetPropertyValueAsInt_m3D59F7D85ABF8C86A01982D46A31913CB8AC4096,
	SuperTileExtensions_GetPropertyValueAsInt_m1168F2B3C96A451AB03EDFA45AF55FED065FB4A1,
	SuperTileExtensions_GetPropertyValueAsFloat_m87A33A3D138C5E9D28AEA46C33A9D0C578772424,
	SuperTileExtensions_GetPropertyValueAsFloat_mCF862373A55454F0D2CD9F1DA72419E096B51D2F,
	SuperTileExtensions_GetPropertyValueAsColor_m8E2692F364FDB1E5B322CA17704ECA066E7CB9D5,
	SuperTileExtensions_GetPropertyValueAsColor_mA472B0F8E5E6EB492DE9921DAD17FD714E2560F1,
	NULL,
	NULL,
	MapRenderConverter_Tiled2Unity_m7A9904C0D7EFCE569A4E86D6C6B760FA8C929593,
	MatrixUtils_Rotate2d_m2BA57D837815B31A53E7270B965D6977A251B8CC,
	ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE,
	SuperColliderComponent__ctor_mC915B9E11EB80888419A76FBA479B85FAFDC0FD1,
	SuperCustomProperties_TryGetCustomProperty_m24FE230AF0F3E6F2573A77F2A224FE026670568A,
	SuperCustomProperties__ctor_m269C8765BFEA3EBF9D3087D7EEF45620DCDCC7F8,
	SuperGroupLayer__ctor_m9617323A2B80A1C819223A955CDEB5257A89DB79,
	SuperImageLayer__ctor_m95BF905B148F2D12B0BC99DFF22479FEA948A2C8,
	SuperLayer_CalculateOpacity_mB02C4E20B46CD52E3AAC1D2F09797614FD7AFA55,
	SuperLayer__ctor_m5429123751D9D8AF27F930F0EB9CA4F59AE2FFA9,
	SuperMap_Start_m2A10A5DB11AFAB5EEFDF8A00447DAE80627C3B36,
	SuperMap_TiledIndexToGridCell_mEF64968094BECE48AEA9BBA482CBA48B3AC5EBEC,
	SuperMap_TiledCellToGridCell_m6B259F9DA048369AFA034470205ACE1B5AA8E1D4,
	SuperMap__ctor_mF1BD27DB45AA4DD1CF44DDD0D558992E82CCB316,
	SuperObject_CalculateOpacity_mAC216FF8FAD9B563547310E2C1C0FCC249BAD70F,
	SuperObject__ctor_m571889300581489EB24B8304D2D55B3542228AFE,
	SuperObjectLayer__ctor_m9CA54CDA7914FCE43E5A7AACB0CD6DF4B994BE20,
	SuperTile_GetTransformMatrix_mF8405E4DD8313F9E173FBB53BA1ADA0AE52B0967,
	SuperTile_GetTRS_m9719104E370A2E207B93D4EFA2CBBF9E6A36A35C,
	SuperTile_GetTileData_m99B915CBDBB237DE32E87CE6554C125F30319646,
	SuperTile_GetTileAnimationData_mEB156AE03ADF33FE9BF57DB22F8275A8AFB96B74,
	SuperTile__ctor_mB8480BC1EA19AA09C949341F9FA403C0D29DD317,
	SuperTile__cctor_mA40AD16A4EDAEB43F909F31DB3B5E8877D12C98F,
	SuperTileLayer__ctor_m6E696A06A988C2D9F8DE09E7DB27252ECA06E0FE,
	TileObjectAnimator_Update_mD778E059F81276E92D5AC642D4B14421B6E9609F,
	TileObjectAnimator__ctor_m416095FC7C6B02376EBCA73C07C86A7E4A5F7FB9,
	OverheadMegaDadController_Awake_m24410793EB9FBC093B3C8469AC8ECEF909E97871,
	OverheadMegaDadController_Start_m770D1AEE9943CEE902FC332D179384D25CE1F8F5,
	OverheadMegaDadController_Update_m39AFCF1B0B01C5FB451806BD576D31924CF79448,
	OverheadMegaDadController_LateUpdate_mCFDCD7122F7E0C0F9EA98BC6CCFB96F4AA9886D6,
	OverheadMegaDadController_OnTriggerEnter2D_m4C37AA2126418375C2A58A0FEDAF5A06AB0C06C9,
	OverheadMegaDadController_MoveUpdate_mAE3AC7646E13F7DECCF224649F9BF5F057DD1C27,
	OverheadMegaDadController_InputUpdate_m60FB810B6895C73FA20F5B76D3FF2088C481C4E8,
	OverheadMegaDadController_RoundToGrid_m60E95416B938AAE4561D8BFB66303AB2E4FB80FB,
	OverheadMegaDadController_Drowned_mDB9569C7C77F6BE33B8B56A17356BB10476F36C6,
	OverheadMegaDadController_SetOverheadCamera_m66970B0E6B857B3D32C6F97DC71BA0DFE6E42384,
	OverheadMegaDadController__ctor_mE913FF722254106AA853A4CFC85CA996D9FE4599,
	U3CU3Ec__cctor_mFFF75958A51651C3450FE092D5A73782A44ABCC8,
	U3CU3Ec__ctor_mC759ED9422CA4128D1563A1E47F882E9B3B3692B,
	U3CU3Ec_U3CStartU3Eb__14_0_m4446E28C4FD9515B3AEF9CFC441747D0E53D4A41,
	U3CDrownedU3Ed__21__ctor_m9B1840C68AA02443082D4BB4593B8C2DDBC9C2AD,
	U3CDrownedU3Ed__21_System_IDisposable_Dispose_m4356657D60E5593B70E506308D2484CCC65E9031,
	U3CDrownedU3Ed__21_MoveNext_m5EB6A86333207B100483CC125B65CB0D18F4C192,
	U3CDrownedU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD91C1CB91A7DEB29CFD48D50B5BC5E486A12F7E2,
	U3CDrownedU3Ed__21_System_Collections_IEnumerator_Reset_mCA29722363B203EF658DFC6D6D82610E82821BD9,
	U3CDrownedU3Ed__21_System_Collections_IEnumerator_get_Current_m060EBCDCC94B24D967E22B5569D5BF7564394C0C,
};
static const int32_t s_InvokerIndices[434] = 
{
	2151,
	2151,
	1748,
	2151,
	2151,
	1488,
	2151,
	2151,
	2151,
	2151,
	1748,
	1748,
	1734,
	2151,
	2151,
	2151,
	2151,
	1472,
	1734,
	1037,
	2151,
	2151,
	2092,
	1337,
	1337,
	2092,
	2151,
	2146,
	2151,
	1734,
	2151,
	2117,
	2092,
	2151,
	2092,
	1734,
	2151,
	2117,
	2092,
	2151,
	2092,
	1734,
	2151,
	2117,
	2092,
	2151,
	2092,
	1734,
	2151,
	2117,
	2092,
	2151,
	2092,
	1748,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	1794,
	2151,
	2151,
	2151,
	2151,
	1748,
	2151,
	2151,
	2151,
	2151,
	2151,
	1748,
	1748,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	1748,
	1748,
	2151,
	2151,
	1770,
	580,
	697,
	2151,
	1734,
	2151,
	2117,
	2092,
	2151,
	2092,
	1734,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	1734,
	2151,
	1748,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	1017,
	1016,
	1590,
	1488,
	2151,
	1009,
	2151,
	1748,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2092,
	2151,
	1734,
	2151,
	2117,
	2092,
	2151,
	2092,
	1734,
	1734,
	1734,
	2151,
	2151,
	2151,
	2151,
	2151,
	1748,
	1748,
	2151,
	1748,
	1748,
	2151,
	1472,
	1734,
	2151,
	2151,
	1734,
	2151,
	1794,
	2151,
	2151,
	2151,
	1734,
	2151,
	2151,
	1044,
	1748,
	2151,
	2151,
	2151,
	2151,
	1748,
	1748,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2117,
	2151,
	2151,
	1748,
	1748,
	2151,
	2151,
	2151,
	2151,
	2092,
	2151,
	1734,
	2151,
	2117,
	2092,
	2151,
	2092,
	1748,
	1748,
	2151,
	2151,
	2151,
	1748,
	1734,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	1734,
	2151,
	1748,
	1734,
	2151,
	2151,
	2151,
	2151,
	1748,
	2151,
	2151,
	1734,
	1472,
	1734,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	397,
	2151,
	397,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	1748,
	1748,
	1748,
	1488,
	1488,
	2151,
	2151,
	2151,
	1748,
	1748,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	1748,
	1748,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	1748,
	2151,
	1335,
	2151,
	2151,
	1734,
	2151,
	2117,
	2092,
	2151,
	2092,
	1748,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	1748,
	1734,
	2151,
	2151,
	2151,
	1748,
	2151,
	2151,
	2151,
	2151,
	1748,
	1734,
	2151,
	2151,
	2151,
	2092,
	1748,
	2151,
	1734,
	2151,
	2117,
	2092,
	2151,
	2092,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2092,
	2117,
	2077,
	2151,
	1734,
	1748,
	1748,
	562,
	512,
	807,
	2151,
	2151,
	2151,
	1632,
	1632,
	2151,
	1638,
	3356,
	2151,
	1633,
	2117,
	2151,
	2727,
	2151,
	1488,
	3216,
	3127,
	3176,
	3274,
	3255,
	-1,
	-1,
	-1,
	-1,
	2727,
	3127,
	-1,
	3274,
	3176,
	3255,
	2727,
	2914,
	2687,
	2989,
	2730,
	2879,
	2645,
	3018,
	2750,
	2833,
	2623,
	-1,
	-1,
	3172,
	2495,
	2151,
	2151,
	743,
	2151,
	2151,
	2151,
	2120,
	2151,
	2151,
	318,
	816,
	2151,
	2120,
	2151,
	2151,
	1325,
	331,
	603,
	500,
	2151,
	3356,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	2151,
	1748,
	2151,
	2151,
	1249,
	2092,
	2151,
	2151,
	3356,
	2151,
	1488,
	1734,
	2151,
	2117,
	2092,
	2151,
	2092,
};
static const Il2CppTokenRangePair s_rgctxIndices[7] = 
{
	{ 0x0600016E, { 0, 1 } },
	{ 0x0600016F, { 1, 1 } },
	{ 0x06000170, { 2, 1 } },
	{ 0x06000171, { 3, 1 } },
	{ 0x06000174, { 4, 2 } },
	{ 0x06000183, { 6, 1 } },
	{ 0x06000184, { 7, 1 } },
};
extern const uint32_t g_rgctx_StringExtensions_ToEnum_TisT_tE7089F8DC02CD5436D846AD4CC39833605CD0587_mC6F4DB58751077FE3A87848E17AA16509AA3B9E0;
extern const uint32_t g_rgctx_Enumerable_Any_TisT_t06CFE6F31052DA1754F24E4136333C927DB154C7_mBEBAA98B9E6A133573D9DB595AB8C48CF1C4A2D8;
extern const uint32_t g_rgctx_GameObjectExtensions_GetComponentInAncestor_TisT_tDAF47A8D3A8B92CB48A92ADD6CCE2B60121A0DD5_m2F15A29EF2D90B8B3399DC0B9E36076C9B2B27B4;
extern const uint32_t g_rgctx_Component_GetComponentInParent_TisT_t81365A011ABDB5D6C852F2807E76D08C3DD8EDC1_m564C5A2D7F215180000D28E0E839792CF417021E;
extern const uint32_t g_rgctx_T_t1EF2FBB4C2D730E0F7BA1A21066EE744A2AF9D0A;
extern const uint32_t g_rgctx_T_t1EF2FBB4C2D730E0F7BA1A21066EE744A2AF9D0A;
extern const uint32_t g_rgctx_SuperTileExtensions_GetPropertyValueAsEnum_TisT_tF115F71CF274E2EA69C5D847B823B4B3F56A29F2_m3D76243964A1E32FCA83922831EDAC74385AF271;
extern const uint32_t g_rgctx_CustomPropertyExtensions_GetValueAsEnum_TisT_t9B2742D035D34016951E8AFC5EAF72ED6032A847_m7514D84E77B778E81B22B4017638D5A95704AF83;
static const Il2CppRGCTXDefinition s_rgctxValues[8] = 
{
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_StringExtensions_ToEnum_TisT_tE7089F8DC02CD5436D846AD4CC39833605CD0587_mC6F4DB58751077FE3A87848E17AA16509AA3B9E0 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Enumerable_Any_TisT_t06CFE6F31052DA1754F24E4136333C927DB154C7_mBEBAA98B9E6A133573D9DB595AB8C48CF1C4A2D8 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_GameObjectExtensions_GetComponentInAncestor_TisT_tDAF47A8D3A8B92CB48A92ADD6CCE2B60121A0DD5_m2F15A29EF2D90B8B3399DC0B9E36076C9B2B27B4 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_Component_GetComponentInParent_TisT_t81365A011ABDB5D6C852F2807E76D08C3DD8EDC1_m564C5A2D7F215180000D28E0E839792CF417021E },
	{ (Il2CppRGCTXDataType)1, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t1EF2FBB4C2D730E0F7BA1A21066EE744A2AF9D0A },
	{ (Il2CppRGCTXDataType)2, (const Il2CppRGCTXDefinitionData *)&g_rgctx_T_t1EF2FBB4C2D730E0F7BA1A21066EE744A2AF9D0A },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_SuperTileExtensions_GetPropertyValueAsEnum_TisT_tF115F71CF274E2EA69C5D847B823B4B3F56A29F2_m3D76243964A1E32FCA83922831EDAC74385AF271 },
	{ (Il2CppRGCTXDataType)3, (const Il2CppRGCTXDefinitionData *)&g_rgctx_CustomPropertyExtensions_GetValueAsEnum_TisT_t9B2742D035D34016951E8AFC5EAF72ED6032A847_m7514D84E77B778E81B22B4017638D5A95704AF83 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	434,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	7,
	s_rgctxIndices,
	8,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
