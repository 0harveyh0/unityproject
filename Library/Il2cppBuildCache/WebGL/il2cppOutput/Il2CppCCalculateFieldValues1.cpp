﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Func`2<SuperTiled2Unity.SuperObject,System.Boolean>
struct Func_2_tE6D4D8DBCF2D140AE341A548D23C3EFE31BC8917;
// System.Func`2<UnityEngine.Vector3,UnityEngine.Vector2>
struct Func_2_t0B3E86DB3AB148706592A60313E005DEDD1E00A4;
// System.Collections.Generic.List`1<SuperTiled2Unity.CollisionObject>
struct List_1_t29D2968766F8C3877BDB3964EE1C98A08FEF900E;
// System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty>
struct List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73;
// System.Collections.Generic.List`1<InventoryItem>
struct List_1_tA1E3C55F496F4C3948C06B79F3A45AA85F43A3CA;
// System.Collections.Generic.List`1<NotificationListener>
struct List_1_tA951AEEA7C288514370BB3190424D0B133E8E9D5;
// System.Collections.Generic.List`1<UnityEngine.ScriptableObject>
struct List_1_tEB4537E121ED7128292F5E49486823EB846576FE;
// System.Collections.Generic.List`1<SignalListener>
struct List_1_t5F51915CE9F42C02C19349C9ABE34A9EA6FAD2A6;
// System.Collections.Generic.Queue`1<System.String>
struct Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// AnimatorController
struct AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01;
// AreaNameController
struct AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61;
// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B;
// BoolValue
struct BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370;
// UnityEngine.Collider2D
struct Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722;
// SuperTiled2Unity.CollisionObject
struct CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D;
// Dialogue
struct Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3;
// DialogueManager
struct DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D;
// FlashColor
struct FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866;
// FloatValue
struct FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// Inventory
struct Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805;
// InventoryItem
struct InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3;
// Notification
struct Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F;
// MegaDad.OverheadMegaDadController
struct OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5;
// SignalSender
struct SignalSender_t0075E93BFF2201C4C328193746B0F29E902AACCA;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// SpriteValue
struct SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C;
// StateMachine
struct StateMachine_t823E5CFDC9E27E10A2C57DB9303909CA2D8BC49A;
// System.String
struct String_t;
// StringValue
struct StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760;
// SuperTiled2Unity.SuperTile
struct SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// VectorValue
struct VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// pot
struct pot_tBC07980977D1BF32444BED885CFF2F3BC7FB875D;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// SuperTiled2Unity.CustomProperty
struct CustomProperty_t40EF9CDA2A043DA7BCEA9D002CC1828000C6C847  : public RuntimeObject
{
public:
	// System.String SuperTiled2Unity.CustomProperty::m_Name
	String_t* ___m_Name_0;
	// System.String SuperTiled2Unity.CustomProperty::m_Type
	String_t* ___m_Type_1;
	// System.String SuperTiled2Unity.CustomProperty::m_Value
	String_t* ___m_Value_2;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(CustomProperty_t40EF9CDA2A043DA7BCEA9D002CC1828000C6C847, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(CustomProperty_t40EF9CDA2A043DA7BCEA9D002CC1828000C6C847, ___m_Type_1)); }
	inline String_t* get_m_Type_1() const { return ___m_Type_1; }
	inline String_t** get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(String_t* value)
	{
		___m_Type_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(CustomProperty_t40EF9CDA2A043DA7BCEA9D002CC1828000C6C847, ___m_Value_2)); }
	inline String_t* get_m_Value_2() const { return ___m_Value_2; }
	inline String_t** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(String_t* value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Value_2), (void*)value);
	}
};


// SuperTiled2Unity.CustomPropertyExtensions
struct CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4  : public RuntimeObject
{
public:

public:
};


// SuperTiled2Unity.CustomPropertyListExtensions
struct CustomPropertyListExtensions_t6D2E5CC1B16B9EB6B57704EF873A5C12550BF269  : public RuntimeObject
{
public:

public:
};


// Dialogue
struct Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3  : public RuntimeObject
{
public:
	// System.String Dialogue::name
	String_t* ___name_0;
	// System.String[] Dialogue::sentences
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___sentences_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_sentences_1() { return static_cast<int32_t>(offsetof(Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3, ___sentences_1)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_sentences_1() const { return ___sentences_1; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_sentences_1() { return &___sentences_1; }
	inline void set_sentences_1(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___sentences_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sentences_1), (void*)value);
	}
};


// SuperTiled2Unity.EnumerableExtensions
struct EnumerableExtensions_tD5F4F8A027DFBE97FAEFED9B93132CBC30CF92C3  : public RuntimeObject
{
public:

public:
};


// SuperTiled2Unity.GameObjectExtensions
struct GameObjectExtensions_tCE982D9EF1C18C5AEA8B9B5186D90F62E9B27DE1  : public RuntimeObject
{
public:

public:
};


// SuperTiled2Unity.MapRenderConverter
struct MapRenderConverter_t6E5EBC7C2C1D7DAAF4571A551C49D8F6DC60F65F  : public RuntimeObject
{
public:

public:
};


// SuperTiled2Unity.MatrixUtils
struct MatrixUtils_tC8A49B46DE6CB803ED5F3E912E20501F7752C591  : public RuntimeObject
{
public:

public:
};


// SuperTiled2Unity.StringExtensions
struct StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826  : public RuntimeObject
{
public:

public:
};


// SuperTiled2Unity.SuperTileExtensions
struct SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// AreaNameController/<NameCo>d__4
struct U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268  : public RuntimeObject
{
public:
	// System.Int32 AreaNameController/<NameCo>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AreaNameController/<NameCo>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AreaNameController AreaNameController/<NameCo>d__4::<>4__this
	AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268, ___U3CU3E4__this_2)); }
	inline AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// SuperTiled2Unity.CollisionObject/<>c
struct U3CU3Ec_t0C7724567A46A1BEEDB72095B8FD8DB98CF36CCB  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0C7724567A46A1BEEDB72095B8FD8DB98CF36CCB_StaticFields
{
public:
	// SuperTiled2Unity.CollisionObject/<>c SuperTiled2Unity.CollisionObject/<>c::<>9
	U3CU3Ec_t0C7724567A46A1BEEDB72095B8FD8DB98CF36CCB * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Vector3,UnityEngine.Vector2> SuperTiled2Unity.CollisionObject/<>c::<>9__25_1
	Func_2_t0B3E86DB3AB148706592A60313E005DEDD1E00A4 * ___U3CU3E9__25_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0C7724567A46A1BEEDB72095B8FD8DB98CF36CCB_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0C7724567A46A1BEEDB72095B8FD8DB98CF36CCB * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0C7724567A46A1BEEDB72095B8FD8DB98CF36CCB ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0C7724567A46A1BEEDB72095B8FD8DB98CF36CCB * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0C7724567A46A1BEEDB72095B8FD8DB98CF36CCB_StaticFields, ___U3CU3E9__25_1_1)); }
	inline Func_2_t0B3E86DB3AB148706592A60313E005DEDD1E00A4 * get_U3CU3E9__25_1_1() const { return ___U3CU3E9__25_1_1; }
	inline Func_2_t0B3E86DB3AB148706592A60313E005DEDD1E00A4 ** get_address_of_U3CU3E9__25_1_1() { return &___U3CU3E9__25_1_1; }
	inline void set_U3CU3E9__25_1_1(Func_2_t0B3E86DB3AB148706592A60313E005DEDD1E00A4 * value)
	{
		___U3CU3E9__25_1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__25_1_1), (void*)value);
	}
};


// SuperTiled2Unity.CollisionObject/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_tB40012BE63C6AF48A235BC43CC8750930461F581  : public RuntimeObject
{
public:
	// SuperTiled2Unity.CollisionObject SuperTiled2Unity.CollisionObject/<>c__DisplayClass22_0::<>4__this
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D * ___U3CU3E4__this_0;
	// SuperTiled2Unity.SuperTile SuperTiled2Unity.CollisionObject/<>c__DisplayClass22_0::tile
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14 * ___tile_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_tB40012BE63C6AF48A235BC43CC8750930461F581, ___U3CU3E4__this_0)); }
	inline CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_tile_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_tB40012BE63C6AF48A235BC43CC8750930461F581, ___tile_1)); }
	inline SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14 * get_tile_1() const { return ___tile_1; }
	inline SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14 ** get_address_of_tile_1() { return &___tile_1; }
	inline void set_tile_1(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14 * value)
	{
		___tile_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tile_1), (void*)value);
	}
};


// SuperTiled2Unity.CustomPropertyListExtensions/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_t4F7D9550667533CDDCC7B1CD0856AC36267DCADA  : public RuntimeObject
{
public:
	// System.String SuperTiled2Unity.CustomPropertyListExtensions/<>c__DisplayClass0_0::propertyName
	String_t* ___propertyName_0;

public:
	inline static int32_t get_offset_of_propertyName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t4F7D9550667533CDDCC7B1CD0856AC36267DCADA, ___propertyName_0)); }
	inline String_t* get_propertyName_0() const { return ___propertyName_0; }
	inline String_t** get_address_of_propertyName_0() { return &___propertyName_0; }
	inline void set_propertyName_0(String_t* value)
	{
		___propertyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___propertyName_0), (void*)value);
	}
};


// DialogueManager/<TypeSentence>d__7
struct U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104  : public RuntimeObject
{
public:
	// System.Int32 DialogueManager/<TypeSentence>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DialogueManager/<TypeSentence>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DialogueManager DialogueManager/<TypeSentence>d__7::<>4__this
	DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D * ___U3CU3E4__this_2;
	// System.String DialogueManager/<TypeSentence>d__7::sentence
	String_t* ___sentence_3;
	// System.Char[] DialogueManager/<TypeSentence>d__7::<>7__wrap1
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___U3CU3E7__wrap1_4;
	// System.Int32 DialogueManager/<TypeSentence>d__7::<>7__wrap2
	int32_t ___U3CU3E7__wrap2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104, ___U3CU3E4__this_2)); }
	inline DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_sentence_3() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104, ___sentence_3)); }
	inline String_t* get_sentence_3() const { return ___sentence_3; }
	inline String_t** get_address_of_sentence_3() { return &___sentence_3; }
	inline void set_sentence_3(String_t* value)
	{
		___sentence_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sentence_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104, ___U3CU3E7__wrap1_4)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E7__wrap1_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_5() { return static_cast<int32_t>(offsetof(U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104, ___U3CU3E7__wrap2_5)); }
	inline int32_t get_U3CU3E7__wrap2_5() const { return ___U3CU3E7__wrap2_5; }
	inline int32_t* get_address_of_U3CU3E7__wrap2_5() { return &___U3CU3E7__wrap2_5; }
	inline void set_U3CU3E7__wrap2_5(int32_t value)
	{
		___U3CU3E7__wrap2_5 = value;
	}
};


// FlashColor/<FlashCo>d__6
struct U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46  : public RuntimeObject
{
public:
	// System.Int32 FlashColor/<FlashCo>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object FlashColor/<FlashCo>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// FlashColor FlashColor/<FlashCo>d__6::<>4__this
	FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866 * ___U3CU3E4__this_2;
	// System.Int32 FlashColor/<FlashCo>d__6::<i>5__2
	int32_t ___U3CiU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46, ___U3CU3E4__this_2)); }
	inline FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46, ___U3CiU3E5__2_3)); }
	inline int32_t get_U3CiU3E5__2_3() const { return ___U3CiU3E5__2_3; }
	inline int32_t* get_address_of_U3CiU3E5__2_3() { return &___U3CiU3E5__2_3; }
	inline void set_U3CiU3E5__2_3(int32_t value)
	{
		___U3CiU3E5__2_3 = value;
	}
};


// MegaDad.OverheadMegaDadController/<>c
struct U3CU3Ec_t069EA630E146EBD7BB9CE47F83C828A52666D434  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t069EA630E146EBD7BB9CE47F83C828A52666D434_StaticFields
{
public:
	// MegaDad.OverheadMegaDadController/<>c MegaDad.OverheadMegaDadController/<>c::<>9
	U3CU3Ec_t069EA630E146EBD7BB9CE47F83C828A52666D434 * ___U3CU3E9_0;
	// System.Func`2<SuperTiled2Unity.SuperObject,System.Boolean> MegaDad.OverheadMegaDadController/<>c::<>9__14_0
	Func_2_tE6D4D8DBCF2D140AE341A548D23C3EFE31BC8917 * ___U3CU3E9__14_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t069EA630E146EBD7BB9CE47F83C828A52666D434_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t069EA630E146EBD7BB9CE47F83C828A52666D434 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t069EA630E146EBD7BB9CE47F83C828A52666D434 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t069EA630E146EBD7BB9CE47F83C828A52666D434 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t069EA630E146EBD7BB9CE47F83C828A52666D434_StaticFields, ___U3CU3E9__14_0_1)); }
	inline Func_2_tE6D4D8DBCF2D140AE341A548D23C3EFE31BC8917 * get_U3CU3E9__14_0_1() const { return ___U3CU3E9__14_0_1; }
	inline Func_2_tE6D4D8DBCF2D140AE341A548D23C3EFE31BC8917 ** get_address_of_U3CU3E9__14_0_1() { return &___U3CU3E9__14_0_1; }
	inline void set_U3CU3E9__14_0_1(Func_2_tE6D4D8DBCF2D140AE341A548D23C3EFE31BC8917 * value)
	{
		___U3CU3E9__14_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__14_0_1), (void*)value);
	}
};


// pot/<breakCo>d__4
struct U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42  : public RuntimeObject
{
public:
	// System.Int32 pot/<breakCo>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object pot/<breakCo>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// pot pot/<breakCo>d__4::<>4__this
	pot_tBC07980977D1BF32444BED885CFF2F3BC7FB875D * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42, ___U3CU3E4__this_2)); }
	inline pot_tBC07980977D1BF32444BED885CFF2F3BC7FB875D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline pot_tBC07980977D1BF32444BED885CFF2F3BC7FB875D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(pot_tBC07980977D1BF32444BED885CFF2F3BC7FB875D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Matrix4x4
struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// SuperTiled2Unity.CollisionShapeType
struct CollisionShapeType_t34C44C75A983937AB6627ECC5D8320190959B81F 
{
public:
	// System.Int32 SuperTiled2Unity.CollisionShapeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CollisionShapeType_t34C44C75A983937AB6627ECC5D8320190959B81F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// EnemyState
struct EnemyState_tE31755D45035069FEC61A9165AE1A7025D6986A9 
{
public:
	// System.Int32 EnemyState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EnemyState_tE31755D45035069FEC61A9165AE1A7025D6986A9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SuperTiled2Unity.FlipFlags
struct FlipFlags_tDDEF8286047324FD6813C734F022AD7E4F092CF3 
{
public:
	// System.Int32 SuperTiled2Unity.FlipFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FlipFlags_tDDEF8286047324FD6813C734F022AD7E4F092CF3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GenericState
struct GenericState_t6EE1FC6A2106ACCE59A16AF258C56AE11C51BA6E 
{
public:
	// System.Int32 GenericState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GenericState_t6EE1FC6A2106ACCE59A16AF258C56AE11C51BA6E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SuperTiled2Unity.GridOrientation
struct GridOrientation_tF9D49F7BE3EDD94E3DD1A3484B562BEF1242161F 
{
public:
	// System.Int32 SuperTiled2Unity.GridOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GridOrientation_tF9D49F7BE3EDD94E3DD1A3484B562BEF1242161F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SuperTiled2Unity.MapOrientation
struct MapOrientation_t7480808E4755AB116CCCD60DE53E0D64874DF328 
{
public:
	// System.Int32 SuperTiled2Unity.MapOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MapOrientation_t7480808E4755AB116CCCD60DE53E0D64874DF328, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SuperTiled2Unity.MapRenderOrder
struct MapRenderOrder_tF57E92C1303B5D80078A07DA3D34CE54BDE3B2C4 
{
public:
	// System.Int32 SuperTiled2Unity.MapRenderOrder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MapRenderOrder_tF57E92C1303B5D80078A07DA3D34CE54BDE3B2C4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// SuperTiled2Unity.ReadOnlyAttribute
struct ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// SuperTiled2Unity.StaggerAxis
struct StaggerAxis_t10EC3EF8051D955A4D3189BA79E3D2EA479DFA7D 
{
public:
	// System.Int32 SuperTiled2Unity.StaggerAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StaggerAxis_t10EC3EF8051D955A4D3189BA79E3D2EA479DFA7D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SuperTiled2Unity.StaggerIndex
struct StaggerIndex_tD65C5729BADB7D3368D075B554927BB766A744DD 
{
public:
	// System.Int32 SuperTiled2Unity.StaggerIndex::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StaggerIndex_tD65C5729BADB7D3368D075B554927BB766A744DD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SuperTiled2Unity.CollisionObject/<>c__DisplayClass25_0
struct U3CU3Ec__DisplayClass25_0_tE31A76DF44246018C15E08B2B2A89A39A82EF09C  : public RuntimeObject
{
public:
	// UnityEngine.Matrix4x4 SuperTiled2Unity.CollisionObject/<>c__DisplayClass25_0::rotate
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___rotate_0;

public:
	inline static int32_t get_offset_of_rotate_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_tE31A76DF44246018C15E08B2B2A89A39A82EF09C, ___rotate_0)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_rotate_0() const { return ___rotate_0; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_rotate_0() { return &___rotate_0; }
	inline void set_rotate_0(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___rotate_0 = value;
	}
};


// MegaDad.OverheadMegaDadController/<Drowned>d__21
struct U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C  : public RuntimeObject
{
public:
	// System.Int32 MegaDad.OverheadMegaDadController/<Drowned>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MegaDad.OverheadMegaDadController/<Drowned>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MegaDad.OverheadMegaDadController MegaDad.OverheadMegaDadController/<Drowned>d__21::<>4__this
	OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571 * ___U3CU3E4__this_2;
	// UnityEngine.Vector2 MegaDad.OverheadMegaDadController/<Drowned>d__21::<pos>5__2
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CposU3E5__2_3;
	// System.Int32 MegaDad.OverheadMegaDadController/<Drowned>d__21::<i>5__3
	int32_t ___U3CiU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C, ___U3CU3E4__this_2)); }
	inline OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CposU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C, ___U3CposU3E5__2_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CposU3E5__2_3() const { return ___U3CposU3E5__2_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CposU3E5__2_3() { return &___U3CposU3E5__2_3; }
	inline void set_U3CposU3E5__2_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CposU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C, ___U3CiU3E5__3_4)); }
	inline int32_t get_U3CiU3E5__3_4() const { return ___U3CiU3E5__3_4; }
	inline int32_t* get_address_of_U3CiU3E5__3_4() { return &___U3CiU3E5__3_4; }
	inline void set_U3CiU3E5__3_4(int32_t value)
	{
		___U3CiU3E5__3_4 = value;
	}
};


// MegaDad.OverheadMegaDadController/State
struct State_tA675A320CDE12DA6B4F48FB0632C8C2D639D6A30 
{
public:
	// System.Int32 MegaDad.OverheadMegaDadController/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_tA675A320CDE12DA6B4F48FB0632C8C2D639D6A30, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SuperTiled2Unity.CollisionObject
struct CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D  : public RuntimeObject
{
public:
	// System.Int32 SuperTiled2Unity.CollisionObject::m_ObjectId
	int32_t ___m_ObjectId_0;
	// System.String SuperTiled2Unity.CollisionObject::m_ObjectName
	String_t* ___m_ObjectName_1;
	// System.String SuperTiled2Unity.CollisionObject::m_ObjectType
	String_t* ___m_ObjectType_2;
	// UnityEngine.Vector2 SuperTiled2Unity.CollisionObject::m_Position
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Position_3;
	// UnityEngine.Vector2 SuperTiled2Unity.CollisionObject::m_Size
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Size_4;
	// System.Single SuperTiled2Unity.CollisionObject::m_Rotation
	float ___m_Rotation_5;
	// System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty> SuperTiled2Unity.CollisionObject::m_CustomProperties
	List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73 * ___m_CustomProperties_6;
	// System.String SuperTiled2Unity.CollisionObject::m_PhysicsLayer
	String_t* ___m_PhysicsLayer_7;
	// System.Boolean SuperTiled2Unity.CollisionObject::m_IsTrigger
	bool ___m_IsTrigger_8;
	// UnityEngine.Vector2[] SuperTiled2Unity.CollisionObject::m_Points
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_Points_9;
	// System.Boolean SuperTiled2Unity.CollisionObject::m_IsClosed
	bool ___m_IsClosed_10;
	// SuperTiled2Unity.CollisionShapeType SuperTiled2Unity.CollisionObject::m_CollisionShapeType
	int32_t ___m_CollisionShapeType_11;

public:
	inline static int32_t get_offset_of_m_ObjectId_0() { return static_cast<int32_t>(offsetof(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D, ___m_ObjectId_0)); }
	inline int32_t get_m_ObjectId_0() const { return ___m_ObjectId_0; }
	inline int32_t* get_address_of_m_ObjectId_0() { return &___m_ObjectId_0; }
	inline void set_m_ObjectId_0(int32_t value)
	{
		___m_ObjectId_0 = value;
	}

	inline static int32_t get_offset_of_m_ObjectName_1() { return static_cast<int32_t>(offsetof(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D, ___m_ObjectName_1)); }
	inline String_t* get_m_ObjectName_1() const { return ___m_ObjectName_1; }
	inline String_t** get_address_of_m_ObjectName_1() { return &___m_ObjectName_1; }
	inline void set_m_ObjectName_1(String_t* value)
	{
		___m_ObjectName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ObjectName_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ObjectType_2() { return static_cast<int32_t>(offsetof(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D, ___m_ObjectType_2)); }
	inline String_t* get_m_ObjectType_2() const { return ___m_ObjectType_2; }
	inline String_t** get_address_of_m_ObjectType_2() { return &___m_ObjectType_2; }
	inline void set_m_ObjectType_2(String_t* value)
	{
		___m_ObjectType_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ObjectType_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Position_3() { return static_cast<int32_t>(offsetof(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D, ___m_Position_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Position_3() const { return ___m_Position_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Position_3() { return &___m_Position_3; }
	inline void set_m_Position_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Position_3 = value;
	}

	inline static int32_t get_offset_of_m_Size_4() { return static_cast<int32_t>(offsetof(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D, ___m_Size_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Size_4() const { return ___m_Size_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Size_4() { return &___m_Size_4; }
	inline void set_m_Size_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Size_4 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_5() { return static_cast<int32_t>(offsetof(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D, ___m_Rotation_5)); }
	inline float get_m_Rotation_5() const { return ___m_Rotation_5; }
	inline float* get_address_of_m_Rotation_5() { return &___m_Rotation_5; }
	inline void set_m_Rotation_5(float value)
	{
		___m_Rotation_5 = value;
	}

	inline static int32_t get_offset_of_m_CustomProperties_6() { return static_cast<int32_t>(offsetof(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D, ___m_CustomProperties_6)); }
	inline List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73 * get_m_CustomProperties_6() const { return ___m_CustomProperties_6; }
	inline List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73 ** get_address_of_m_CustomProperties_6() { return &___m_CustomProperties_6; }
	inline void set_m_CustomProperties_6(List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73 * value)
	{
		___m_CustomProperties_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CustomProperties_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_PhysicsLayer_7() { return static_cast<int32_t>(offsetof(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D, ___m_PhysicsLayer_7)); }
	inline String_t* get_m_PhysicsLayer_7() const { return ___m_PhysicsLayer_7; }
	inline String_t** get_address_of_m_PhysicsLayer_7() { return &___m_PhysicsLayer_7; }
	inline void set_m_PhysicsLayer_7(String_t* value)
	{
		___m_PhysicsLayer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PhysicsLayer_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_IsTrigger_8() { return static_cast<int32_t>(offsetof(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D, ___m_IsTrigger_8)); }
	inline bool get_m_IsTrigger_8() const { return ___m_IsTrigger_8; }
	inline bool* get_address_of_m_IsTrigger_8() { return &___m_IsTrigger_8; }
	inline void set_m_IsTrigger_8(bool value)
	{
		___m_IsTrigger_8 = value;
	}

	inline static int32_t get_offset_of_m_Points_9() { return static_cast<int32_t>(offsetof(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D, ___m_Points_9)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_Points_9() const { return ___m_Points_9; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_Points_9() { return &___m_Points_9; }
	inline void set_m_Points_9(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_Points_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Points_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_IsClosed_10() { return static_cast<int32_t>(offsetof(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D, ___m_IsClosed_10)); }
	inline bool get_m_IsClosed_10() const { return ___m_IsClosed_10; }
	inline bool* get_address_of_m_IsClosed_10() { return &___m_IsClosed_10; }
	inline void set_m_IsClosed_10(bool value)
	{
		___m_IsClosed_10 = value;
	}

	inline static int32_t get_offset_of_m_CollisionShapeType_11() { return static_cast<int32_t>(offsetof(CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D, ___m_CollisionShapeType_11)); }
	inline int32_t get_m_CollisionShapeType_11() const { return ___m_CollisionShapeType_11; }
	inline int32_t* get_address_of_m_CollisionShapeType_11() { return &___m_CollisionShapeType_11; }
	inline void set_m_CollisionShapeType_11(int32_t value)
	{
		___m_CollisionShapeType_11 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// BoolValue
struct BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Boolean BoolValue::value
	bool ___value_4;
	// System.Boolean BoolValue::resetValue
	bool ___resetValue_5;

public:
	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370, ___value_4)); }
	inline bool get_value_4() const { return ___value_4; }
	inline bool* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(bool value)
	{
		___value_4 = value;
	}

	inline static int32_t get_offset_of_resetValue_5() { return static_cast<int32_t>(offsetof(BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370, ___resetValue_5)); }
	inline bool get_resetValue_5() const { return ___resetValue_5; }
	inline bool* get_address_of_resetValue_5() { return &___resetValue_5; }
	inline void set_resetValue_5(bool value)
	{
		___resetValue_5 = value;
	}
};


// FloatValue
struct FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Single FloatValue::value
	float ___value_4;
	// System.Single FloatValue::defaultValue
	float ___defaultValue_5;
	// System.Single FloatValue::RuntimeValue
	float ___RuntimeValue_6;

public:
	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10, ___value_4)); }
	inline float get_value_4() const { return ___value_4; }
	inline float* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(float value)
	{
		___value_4 = value;
	}

	inline static int32_t get_offset_of_defaultValue_5() { return static_cast<int32_t>(offsetof(FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10, ___defaultValue_5)); }
	inline float get_defaultValue_5() const { return ___defaultValue_5; }
	inline float* get_address_of_defaultValue_5() { return &___defaultValue_5; }
	inline void set_defaultValue_5(float value)
	{
		___defaultValue_5 = value;
	}

	inline static int32_t get_offset_of_RuntimeValue_6() { return static_cast<int32_t>(offsetof(FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10, ___RuntimeValue_6)); }
	inline float get_RuntimeValue_6() const { return ___RuntimeValue_6; }
	inline float* get_address_of_RuntimeValue_6() { return &___RuntimeValue_6; }
	inline void set_RuntimeValue_6(float value)
	{
		___RuntimeValue_6 = value;
	}
};


// GenericAbility
struct GenericAbility_t6F5062DC8FA17FF2828A882D17D7D48864095C21  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Single GenericAbility::magicCost
	float ___magicCost_4;
	// System.Single GenericAbility::duration
	float ___duration_5;
	// FloatValue GenericAbility::playerMagic
	FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * ___playerMagic_6;
	// Notification GenericAbility::usePlayerMagic
	Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * ___usePlayerMagic_7;

public:
	inline static int32_t get_offset_of_magicCost_4() { return static_cast<int32_t>(offsetof(GenericAbility_t6F5062DC8FA17FF2828A882D17D7D48864095C21, ___magicCost_4)); }
	inline float get_magicCost_4() const { return ___magicCost_4; }
	inline float* get_address_of_magicCost_4() { return &___magicCost_4; }
	inline void set_magicCost_4(float value)
	{
		___magicCost_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(GenericAbility_t6F5062DC8FA17FF2828A882D17D7D48864095C21, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_playerMagic_6() { return static_cast<int32_t>(offsetof(GenericAbility_t6F5062DC8FA17FF2828A882D17D7D48864095C21, ___playerMagic_6)); }
	inline FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * get_playerMagic_6() const { return ___playerMagic_6; }
	inline FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 ** get_address_of_playerMagic_6() { return &___playerMagic_6; }
	inline void set_playerMagic_6(FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * value)
	{
		___playerMagic_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerMagic_6), (void*)value);
	}

	inline static int32_t get_offset_of_usePlayerMagic_7() { return static_cast<int32_t>(offsetof(GenericAbility_t6F5062DC8FA17FF2828A882D17D7D48864095C21, ___usePlayerMagic_7)); }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * get_usePlayerMagic_7() const { return ___usePlayerMagic_7; }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F ** get_address_of_usePlayerMagic_7() { return &___usePlayerMagic_7; }
	inline void set_usePlayerMagic_7(Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * value)
	{
		___usePlayerMagic_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___usePlayerMagic_7), (void*)value);
	}
};


// Inventory
struct Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Collections.Generic.List`1<InventoryItem> Inventory::myInventory
	List_1_tA1E3C55F496F4C3948C06B79F3A45AA85F43A3CA * ___myInventory_4;

public:
	inline static int32_t get_offset_of_myInventory_4() { return static_cast<int32_t>(offsetof(Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805, ___myInventory_4)); }
	inline List_1_tA1E3C55F496F4C3948C06B79F3A45AA85F43A3CA * get_myInventory_4() const { return ___myInventory_4; }
	inline List_1_tA1E3C55F496F4C3948C06B79F3A45AA85F43A3CA ** get_address_of_myInventory_4() { return &___myInventory_4; }
	inline void set_myInventory_4(List_1_tA1E3C55F496F4C3948C06B79F3A45AA85F43A3CA * value)
	{
		___myInventory_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myInventory_4), (void*)value);
	}
};


// InventoryItem
struct InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// UnityEngine.Sprite InventoryItem::mySprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___mySprite_4;
	// System.String InventoryItem::myName
	String_t* ___myName_5;
	// System.String InventoryItem::myDescription
	String_t* ___myDescription_6;
	// System.Boolean InventoryItem::isUsable
	bool ___isUsable_7;
	// System.Boolean InventoryItem::isUnique
	bool ___isUnique_8;
	// System.Int32 InventoryItem::numberHeld
	int32_t ___numberHeld_9;

public:
	inline static int32_t get_offset_of_mySprite_4() { return static_cast<int32_t>(offsetof(InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3, ___mySprite_4)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_mySprite_4() const { return ___mySprite_4; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_mySprite_4() { return &___mySprite_4; }
	inline void set_mySprite_4(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___mySprite_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mySprite_4), (void*)value);
	}

	inline static int32_t get_offset_of_myName_5() { return static_cast<int32_t>(offsetof(InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3, ___myName_5)); }
	inline String_t* get_myName_5() const { return ___myName_5; }
	inline String_t** get_address_of_myName_5() { return &___myName_5; }
	inline void set_myName_5(String_t* value)
	{
		___myName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myName_5), (void*)value);
	}

	inline static int32_t get_offset_of_myDescription_6() { return static_cast<int32_t>(offsetof(InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3, ___myDescription_6)); }
	inline String_t* get_myDescription_6() const { return ___myDescription_6; }
	inline String_t** get_address_of_myDescription_6() { return &___myDescription_6; }
	inline void set_myDescription_6(String_t* value)
	{
		___myDescription_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myDescription_6), (void*)value);
	}

	inline static int32_t get_offset_of_isUsable_7() { return static_cast<int32_t>(offsetof(InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3, ___isUsable_7)); }
	inline bool get_isUsable_7() const { return ___isUsable_7; }
	inline bool* get_address_of_isUsable_7() { return &___isUsable_7; }
	inline void set_isUsable_7(bool value)
	{
		___isUsable_7 = value;
	}

	inline static int32_t get_offset_of_isUnique_8() { return static_cast<int32_t>(offsetof(InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3, ___isUnique_8)); }
	inline bool get_isUnique_8() const { return ___isUnique_8; }
	inline bool* get_address_of_isUnique_8() { return &___isUnique_8; }
	inline void set_isUnique_8(bool value)
	{
		___isUnique_8 = value;
	}

	inline static int32_t get_offset_of_numberHeld_9() { return static_cast<int32_t>(offsetof(InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3, ___numberHeld_9)); }
	inline int32_t get_numberHeld_9() const { return ___numberHeld_9; }
	inline int32_t* get_address_of_numberHeld_9() { return &___numberHeld_9; }
	inline void set_numberHeld_9(int32_t value)
	{
		___numberHeld_9 = value;
	}
};


// Notification
struct Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Collections.Generic.List`1<NotificationListener> Notification::listeners
	List_1_tA951AEEA7C288514370BB3190424D0B133E8E9D5 * ___listeners_4;

public:
	inline static int32_t get_offset_of_listeners_4() { return static_cast<int32_t>(offsetof(Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F, ___listeners_4)); }
	inline List_1_tA951AEEA7C288514370BB3190424D0B133E8E9D5 * get_listeners_4() const { return ___listeners_4; }
	inline List_1_tA951AEEA7C288514370BB3190424D0B133E8E9D5 ** get_address_of_listeners_4() { return &___listeners_4; }
	inline void set_listeners_4(List_1_tA951AEEA7C288514370BB3190424D0B133E8E9D5 * value)
	{
		___listeners_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___listeners_4), (void*)value);
	}
};


// SignalSender
struct SignalSender_t0075E93BFF2201C4C328193746B0F29E902AACCA  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Collections.Generic.List`1<SignalListener> SignalSender::listeners
	List_1_t5F51915CE9F42C02C19349C9ABE34A9EA6FAD2A6 * ___listeners_4;

public:
	inline static int32_t get_offset_of_listeners_4() { return static_cast<int32_t>(offsetof(SignalSender_t0075E93BFF2201C4C328193746B0F29E902AACCA, ___listeners_4)); }
	inline List_1_t5F51915CE9F42C02C19349C9ABE34A9EA6FAD2A6 * get_listeners_4() const { return ___listeners_4; }
	inline List_1_t5F51915CE9F42C02C19349C9ABE34A9EA6FAD2A6 ** get_address_of_listeners_4() { return &___listeners_4; }
	inline void set_listeners_4(List_1_t5F51915CE9F42C02C19349C9ABE34A9EA6FAD2A6 * value)
	{
		___listeners_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___listeners_4), (void*)value);
	}
};


// SpriteValue
struct SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// UnityEngine.Sprite SpriteValue::value
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___value_4;

public:
	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C, ___value_4)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_value_4() const { return ___value_4; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___value_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_4), (void*)value);
	}
};


// StringValue
struct StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.String StringValue::value
	String_t* ___value_4;

public:
	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760, ___value_4)); }
	inline String_t* get_value_4() const { return ___value_4; }
	inline String_t** get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(String_t* value)
	{
		___value_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_4), (void*)value);
	}
};


// UnityEngine.Tilemaps.TileBase
struct TileBase_t151317678DF54EED207F0AD6F4C590272B9AA052  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// VectorValue
struct VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// UnityEngine.Vector2 VectorValue::value
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value_4;
	// UnityEngine.Vector2 VectorValue::defaultValue
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___defaultValue_5;

public:
	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD, ___value_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_value_4() const { return ___value_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___value_4 = value;
	}

	inline static int32_t get_offset_of_defaultValue_5() { return static_cast<int32_t>(offsetof(VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD, ___defaultValue_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_defaultValue_5() const { return ___defaultValue_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_defaultValue_5() { return &___defaultValue_5; }
	inline void set_defaultValue_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___defaultValue_5 = value;
	}
};


// DashAbility
struct DashAbility_t9D1464CE8BDF1A9D6CE8253D6E2AFC8ECA794DC9  : public GenericAbility_t6F5062DC8FA17FF2828A882D17D7D48864095C21
{
public:
	// System.Single DashAbility::dashForce
	float ___dashForce_8;

public:
	inline static int32_t get_offset_of_dashForce_8() { return static_cast<int32_t>(offsetof(DashAbility_t9D1464CE8BDF1A9D6CE8253D6E2AFC8ECA794DC9, ___dashForce_8)); }
	inline float get_dashForce_8() const { return ___dashForce_8; }
	inline float* get_address_of_dashForce_8() { return &___dashForce_8; }
	inline void set_dashForce_8(float value)
	{
		___dashForce_8 = value;
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// SuperTiled2Unity.SuperTile
struct SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14  : public TileBase_t151317678DF54EED207F0AD6F4C590272B9AA052
{
public:
	// System.Int32 SuperTiled2Unity.SuperTile::m_TileId
	int32_t ___m_TileId_7;
	// UnityEngine.Sprite SuperTiled2Unity.SuperTile::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_8;
	// UnityEngine.Sprite[] SuperTiled2Unity.SuperTile::m_AnimationSprites
	SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* ___m_AnimationSprites_9;
	// System.String SuperTiled2Unity.SuperTile::m_Type
	String_t* ___m_Type_10;
	// System.Single SuperTiled2Unity.SuperTile::m_Width
	float ___m_Width_11;
	// System.Single SuperTiled2Unity.SuperTile::m_Height
	float ___m_Height_12;
	// System.Single SuperTiled2Unity.SuperTile::m_TileOffsetX
	float ___m_TileOffsetX_13;
	// System.Single SuperTiled2Unity.SuperTile::m_TileOffsetY
	float ___m_TileOffsetY_14;
	// System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty> SuperTiled2Unity.SuperTile::m_CustomProperties
	List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73 * ___m_CustomProperties_15;
	// System.Collections.Generic.List`1<SuperTiled2Unity.CollisionObject> SuperTiled2Unity.SuperTile::m_CollisionObjects
	List_1_t29D2968766F8C3877BDB3964EE1C98A08FEF900E * ___m_CollisionObjects_16;

public:
	inline static int32_t get_offset_of_m_TileId_7() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14, ___m_TileId_7)); }
	inline int32_t get_m_TileId_7() const { return ___m_TileId_7; }
	inline int32_t* get_address_of_m_TileId_7() { return &___m_TileId_7; }
	inline void set_m_TileId_7(int32_t value)
	{
		___m_TileId_7 = value;
	}

	inline static int32_t get_offset_of_m_Sprite_8() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14, ___m_Sprite_8)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_8() const { return ___m_Sprite_8; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_8() { return &___m_Sprite_8; }
	inline void set_m_Sprite_8(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_AnimationSprites_9() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14, ___m_AnimationSprites_9)); }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* get_m_AnimationSprites_9() const { return ___m_AnimationSprites_9; }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77** get_address_of_m_AnimationSprites_9() { return &___m_AnimationSprites_9; }
	inline void set_m_AnimationSprites_9(SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* value)
	{
		___m_AnimationSprites_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationSprites_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_10() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14, ___m_Type_10)); }
	inline String_t* get_m_Type_10() const { return ___m_Type_10; }
	inline String_t** get_address_of_m_Type_10() { return &___m_Type_10; }
	inline void set_m_Type_10(String_t* value)
	{
		___m_Type_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_Width_11() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14, ___m_Width_11)); }
	inline float get_m_Width_11() const { return ___m_Width_11; }
	inline float* get_address_of_m_Width_11() { return &___m_Width_11; }
	inline void set_m_Width_11(float value)
	{
		___m_Width_11 = value;
	}

	inline static int32_t get_offset_of_m_Height_12() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14, ___m_Height_12)); }
	inline float get_m_Height_12() const { return ___m_Height_12; }
	inline float* get_address_of_m_Height_12() { return &___m_Height_12; }
	inline void set_m_Height_12(float value)
	{
		___m_Height_12 = value;
	}

	inline static int32_t get_offset_of_m_TileOffsetX_13() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14, ___m_TileOffsetX_13)); }
	inline float get_m_TileOffsetX_13() const { return ___m_TileOffsetX_13; }
	inline float* get_address_of_m_TileOffsetX_13() { return &___m_TileOffsetX_13; }
	inline void set_m_TileOffsetX_13(float value)
	{
		___m_TileOffsetX_13 = value;
	}

	inline static int32_t get_offset_of_m_TileOffsetY_14() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14, ___m_TileOffsetY_14)); }
	inline float get_m_TileOffsetY_14() const { return ___m_TileOffsetY_14; }
	inline float* get_address_of_m_TileOffsetY_14() { return &___m_TileOffsetY_14; }
	inline void set_m_TileOffsetY_14(float value)
	{
		___m_TileOffsetY_14 = value;
	}

	inline static int32_t get_offset_of_m_CustomProperties_15() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14, ___m_CustomProperties_15)); }
	inline List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73 * get_m_CustomProperties_15() const { return ___m_CustomProperties_15; }
	inline List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73 ** get_address_of_m_CustomProperties_15() { return &___m_CustomProperties_15; }
	inline void set_m_CustomProperties_15(List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73 * value)
	{
		___m_CustomProperties_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CustomProperties_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_CollisionObjects_16() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14, ___m_CollisionObjects_16)); }
	inline List_1_t29D2968766F8C3877BDB3964EE1C98A08FEF900E * get_m_CollisionObjects_16() const { return ___m_CollisionObjects_16; }
	inline List_1_t29D2968766F8C3877BDB3964EE1C98A08FEF900E ** get_address_of_m_CollisionObjects_16() { return &___m_CollisionObjects_16; }
	inline void set_m_CollisionObjects_16(List_1_t29D2968766F8C3877BDB3964EE1C98A08FEF900E * value)
	{
		___m_CollisionObjects_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CollisionObjects_16), (void*)value);
	}
};

struct SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_StaticFields
{
public:
	// UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile::HorizontalFlipMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___HorizontalFlipMatrix_4;
	// UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile::VerticalFlipMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___VerticalFlipMatrix_5;
	// UnityEngine.Matrix4x4 SuperTiled2Unity.SuperTile::DiagonalFlipMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___DiagonalFlipMatrix_6;

public:
	inline static int32_t get_offset_of_HorizontalFlipMatrix_4() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_StaticFields, ___HorizontalFlipMatrix_4)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_HorizontalFlipMatrix_4() const { return ___HorizontalFlipMatrix_4; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_HorizontalFlipMatrix_4() { return &___HorizontalFlipMatrix_4; }
	inline void set_HorizontalFlipMatrix_4(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___HorizontalFlipMatrix_4 = value;
	}

	inline static int32_t get_offset_of_VerticalFlipMatrix_5() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_StaticFields, ___VerticalFlipMatrix_5)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_VerticalFlipMatrix_5() const { return ___VerticalFlipMatrix_5; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_VerticalFlipMatrix_5() { return &___VerticalFlipMatrix_5; }
	inline void set_VerticalFlipMatrix_5(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___VerticalFlipMatrix_5 = value;
	}

	inline static int32_t get_offset_of_DiagonalFlipMatrix_6() { return static_cast<int32_t>(offsetof(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_StaticFields, ___DiagonalFlipMatrix_6)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_DiagonalFlipMatrix_6() const { return ___DiagonalFlipMatrix_6; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_DiagonalFlipMatrix_6() { return &___DiagonalFlipMatrix_6; }
	inline void set_DiagonalFlipMatrix_6(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___DiagonalFlipMatrix_6 = value;
	}
};


// AnimatorController
struct AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Animator AnimatorController::anim
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___anim_4;

public:
	inline static int32_t get_offset_of_anim_4() { return static_cast<int32_t>(offsetof(AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01, ___anim_4)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_anim_4() const { return ___anim_4; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_anim_4() { return &___anim_4; }
	inline void set_anim_4(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___anim_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anim_4), (void*)value);
	}
};


// AreaNameController
struct AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TextMeshProUGUI AreaNameController::nameText
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___nameText_4;
	// StringValue AreaNameController::roomNameValue
	StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 * ___roomNameValue_5;
	// System.Single AreaNameController::duration
	float ___duration_6;

public:
	inline static int32_t get_offset_of_nameText_4() { return static_cast<int32_t>(offsetof(AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61, ___nameText_4)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_nameText_4() const { return ___nameText_4; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_nameText_4() { return &___nameText_4; }
	inline void set_nameText_4(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___nameText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameText_4), (void*)value);
	}

	inline static int32_t get_offset_of_roomNameValue_5() { return static_cast<int32_t>(offsetof(AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61, ___roomNameValue_5)); }
	inline StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 * get_roomNameValue_5() const { return ___roomNameValue_5; }
	inline StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 ** get_address_of_roomNameValue_5() { return &___roomNameValue_5; }
	inline void set_roomNameValue_5(StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 * value)
	{
		___roomNameValue_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___roomNameValue_5), (void*)value);
	}

	inline static int32_t get_offset_of_duration_6() { return static_cast<int32_t>(offsetof(AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61, ___duration_6)); }
	inline float get_duration_6() const { return ___duration_6; }
	inline float* get_address_of_duration_6() { return &___duration_6; }
	inline void set_duration_6(float value)
	{
		___duration_6 = value;
	}
};


// Arrow
struct Arrow_t98BA45BD73ADFAD1B38830B6D6D48588A96AC86A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Arrow::speed
	float ___speed_4;
	// UnityEngine.Rigidbody2D Arrow::myRigidbody
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___myRigidbody_5;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(Arrow_t98BA45BD73ADFAD1B38830B6D6D48588A96AC86A, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_myRigidbody_5() { return static_cast<int32_t>(offsetof(Arrow_t98BA45BD73ADFAD1B38830B6D6D48588A96AC86A, ___myRigidbody_5)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_myRigidbody_5() const { return ___myRigidbody_5; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_myRigidbody_5() { return &___myRigidbody_5; }
	inline void set_myRigidbody_5(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___myRigidbody_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myRigidbody_5), (void*)value);
	}
};


// ContextClue
struct ContextClue_tE96BB9C19D11CBC0A0137551DAFF0EE6796339B1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.SpriteRenderer ContextClue::mySprite
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___mySprite_4;
	// System.Boolean ContextClue::clueActive
	bool ___clueActive_5;

public:
	inline static int32_t get_offset_of_mySprite_4() { return static_cast<int32_t>(offsetof(ContextClue_tE96BB9C19D11CBC0A0137551DAFF0EE6796339B1, ___mySprite_4)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_mySprite_4() const { return ___mySprite_4; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_mySprite_4() { return &___mySprite_4; }
	inline void set_mySprite_4(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___mySprite_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mySprite_4), (void*)value);
	}

	inline static int32_t get_offset_of_clueActive_5() { return static_cast<int32_t>(offsetof(ContextClue_tE96BB9C19D11CBC0A0137551DAFF0EE6796339B1, ___clueActive_5)); }
	inline bool get_clueActive_5() const { return ___clueActive_5; }
	inline bool* get_address_of_clueActive_5() { return &___clueActive_5; }
	inline void set_clueActive_5(bool value)
	{
		___clueActive_5 = value;
	}
};


// Damage
struct Damage_tF03686559EE2A75E7FB5C86FDC5B966CB31BE631  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// DestroyOverTime
struct DestroyOverTime_t72388AFBB9556818D23C5BD91F53CC8EA9FDC03F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single DestroyOverTime::destroyDelay
	float ___destroyDelay_4;

public:
	inline static int32_t get_offset_of_destroyDelay_4() { return static_cast<int32_t>(offsetof(DestroyOverTime_t72388AFBB9556818D23C5BD91F53CC8EA9FDC03F, ___destroyDelay_4)); }
	inline float get_destroyDelay_4() const { return ___destroyDelay_4; }
	inline float* get_address_of_destroyDelay_4() { return &___destroyDelay_4; }
	inline void set_destroyDelay_4(float value)
	{
		___destroyDelay_4 = value;
	}
};


// DialogController
struct DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// StringValue DialogController::stringText
	StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 * ___stringText_4;
	// Notification DialogController::dialogNotification
	Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * ___dialogNotification_5;
	// TMPro.TextMeshProUGUI DialogController::dialogText
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___dialogText_6;
	// UnityEngine.GameObject DialogController::dialogObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___dialogObject_7;
	// System.Boolean DialogController::dialogActive
	bool ___dialogActive_8;

public:
	inline static int32_t get_offset_of_stringText_4() { return static_cast<int32_t>(offsetof(DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4, ___stringText_4)); }
	inline StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 * get_stringText_4() const { return ___stringText_4; }
	inline StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 ** get_address_of_stringText_4() { return &___stringText_4; }
	inline void set_stringText_4(StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 * value)
	{
		___stringText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringText_4), (void*)value);
	}

	inline static int32_t get_offset_of_dialogNotification_5() { return static_cast<int32_t>(offsetof(DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4, ___dialogNotification_5)); }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * get_dialogNotification_5() const { return ___dialogNotification_5; }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F ** get_address_of_dialogNotification_5() { return &___dialogNotification_5; }
	inline void set_dialogNotification_5(Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * value)
	{
		___dialogNotification_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dialogNotification_5), (void*)value);
	}

	inline static int32_t get_offset_of_dialogText_6() { return static_cast<int32_t>(offsetof(DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4, ___dialogText_6)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_dialogText_6() const { return ___dialogText_6; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_dialogText_6() { return &___dialogText_6; }
	inline void set_dialogText_6(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___dialogText_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dialogText_6), (void*)value);
	}

	inline static int32_t get_offset_of_dialogObject_7() { return static_cast<int32_t>(offsetof(DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4, ___dialogObject_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_dialogObject_7() const { return ___dialogObject_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_dialogObject_7() { return &___dialogObject_7; }
	inline void set_dialogObject_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___dialogObject_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dialogObject_7), (void*)value);
	}

	inline static int32_t get_offset_of_dialogActive_8() { return static_cast<int32_t>(offsetof(DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4, ___dialogActive_8)); }
	inline bool get_dialogActive_8() const { return ___dialogActive_8; }
	inline bool* get_address_of_dialogActive_8() { return &___dialogActive_8; }
	inline void set_dialogActive_8(bool value)
	{
		___dialogActive_8 = value;
	}
};


// DialogueManager
struct DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TextMeshProUGUI DialogueManager::nameText
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___nameText_4;
	// TMPro.TextMeshProUGUI DialogueManager::dialogueText
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___dialogueText_5;
	// UnityEngine.Animator DialogueManager::animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___animator_6;
	// System.Collections.Generic.Queue`1<System.String> DialogueManager::sentences
	Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D * ___sentences_7;

public:
	inline static int32_t get_offset_of_nameText_4() { return static_cast<int32_t>(offsetof(DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D, ___nameText_4)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_nameText_4() const { return ___nameText_4; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_nameText_4() { return &___nameText_4; }
	inline void set_nameText_4(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___nameText_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameText_4), (void*)value);
	}

	inline static int32_t get_offset_of_dialogueText_5() { return static_cast<int32_t>(offsetof(DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D, ___dialogueText_5)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_dialogueText_5() const { return ___dialogueText_5; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_dialogueText_5() { return &___dialogueText_5; }
	inline void set_dialogueText_5(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___dialogueText_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dialogueText_5), (void*)value);
	}

	inline static int32_t get_offset_of_animator_6() { return static_cast<int32_t>(offsetof(DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D, ___animator_6)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_animator_6() const { return ___animator_6; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_animator_6() { return &___animator_6; }
	inline void set_animator_6(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___animator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animator_6), (void*)value);
	}

	inline static int32_t get_offset_of_sentences_7() { return static_cast<int32_t>(offsetof(DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D, ___sentences_7)); }
	inline Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D * get_sentences_7() const { return ___sentences_7; }
	inline Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D ** get_address_of_sentences_7() { return &___sentences_7; }
	inline void set_sentences_7(Queue_1_tD2C03A5990B5958D85846D872A22AA67F3E8F97D * value)
	{
		___sentences_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sentences_7), (void*)value);
	}
};


// DialogueTrigger
struct DialogueTrigger_tAA3B7D126EEF458C23E794A4C0648320248BDA39  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean DialogueTrigger::playerinRange
	bool ___playerinRange_4;
	// Dialogue DialogueTrigger::dialogue
	Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3 * ___dialogue_5;

public:
	inline static int32_t get_offset_of_playerinRange_4() { return static_cast<int32_t>(offsetof(DialogueTrigger_tAA3B7D126EEF458C23E794A4C0648320248BDA39, ___playerinRange_4)); }
	inline bool get_playerinRange_4() const { return ___playerinRange_4; }
	inline bool* get_address_of_playerinRange_4() { return &___playerinRange_4; }
	inline void set_playerinRange_4(bool value)
	{
		___playerinRange_4 = value;
	}

	inline static int32_t get_offset_of_dialogue_5() { return static_cast<int32_t>(offsetof(DialogueTrigger_tAA3B7D126EEF458C23E794A4C0648320248BDA39, ___dialogue_5)); }
	inline Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3 * get_dialogue_5() const { return ___dialogue_5; }
	inline Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3 ** get_address_of_dialogue_5() { return &___dialogue_5; }
	inline void set_dialogue_5(Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3 * value)
	{
		___dialogue_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dialogue_5), (void*)value);
	}
};


// DisableColliderForFixedTime
struct DisableColliderForFixedTime_t28CD258441252ED83EE03A583A41D73B5D87E087  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Enemy
struct Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// EnemyState Enemy::currentState
	int32_t ___currentState_4;
	// FloatValue Enemy::maxHealth
	FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * ___maxHealth_5;
	// System.Single Enemy::health
	float ___health_6;
	// System.String Enemy::enemyName
	String_t* ___enemyName_7;
	// System.Int32 Enemy::baseAttack
	int32_t ___baseAttack_8;
	// System.Single Enemy::moveSpeed
	float ___moveSpeed_9;

public:
	inline static int32_t get_offset_of_currentState_4() { return static_cast<int32_t>(offsetof(Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627, ___currentState_4)); }
	inline int32_t get_currentState_4() const { return ___currentState_4; }
	inline int32_t* get_address_of_currentState_4() { return &___currentState_4; }
	inline void set_currentState_4(int32_t value)
	{
		___currentState_4 = value;
	}

	inline static int32_t get_offset_of_maxHealth_5() { return static_cast<int32_t>(offsetof(Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627, ___maxHealth_5)); }
	inline FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * get_maxHealth_5() const { return ___maxHealth_5; }
	inline FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 ** get_address_of_maxHealth_5() { return &___maxHealth_5; }
	inline void set_maxHealth_5(FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * value)
	{
		___maxHealth_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___maxHealth_5), (void*)value);
	}

	inline static int32_t get_offset_of_health_6() { return static_cast<int32_t>(offsetof(Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627, ___health_6)); }
	inline float get_health_6() const { return ___health_6; }
	inline float* get_address_of_health_6() { return &___health_6; }
	inline void set_health_6(float value)
	{
		___health_6 = value;
	}

	inline static int32_t get_offset_of_enemyName_7() { return static_cast<int32_t>(offsetof(Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627, ___enemyName_7)); }
	inline String_t* get_enemyName_7() const { return ___enemyName_7; }
	inline String_t** get_address_of_enemyName_7() { return &___enemyName_7; }
	inline void set_enemyName_7(String_t* value)
	{
		___enemyName_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enemyName_7), (void*)value);
	}

	inline static int32_t get_offset_of_baseAttack_8() { return static_cast<int32_t>(offsetof(Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627, ___baseAttack_8)); }
	inline int32_t get_baseAttack_8() const { return ___baseAttack_8; }
	inline int32_t* get_address_of_baseAttack_8() { return &___baseAttack_8; }
	inline void set_baseAttack_8(int32_t value)
	{
		___baseAttack_8 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_9() { return static_cast<int32_t>(offsetof(Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627, ___moveSpeed_9)); }
	inline float get_moveSpeed_9() const { return ___moveSpeed_9; }
	inline float* get_address_of_moveSpeed_9() { return &___moveSpeed_9; }
	inline void set_moveSpeed_9(float value)
	{
		___moveSpeed_9 = value;
	}
};


// FlashColor
struct FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.SpriteRenderer FlashColor::mySprite
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___mySprite_4;
	// UnityEngine.Color FlashColor::flashColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___flashColor_5;
	// System.Int32 FlashColor::numberOfFlashes
	int32_t ___numberOfFlashes_6;
	// System.Single FlashColor::flashDelay
	float ___flashDelay_7;
	// System.Boolean FlashColor::isFlashing
	bool ___isFlashing_8;

public:
	inline static int32_t get_offset_of_mySprite_4() { return static_cast<int32_t>(offsetof(FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866, ___mySprite_4)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_mySprite_4() const { return ___mySprite_4; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_mySprite_4() { return &___mySprite_4; }
	inline void set_mySprite_4(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___mySprite_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mySprite_4), (void*)value);
	}

	inline static int32_t get_offset_of_flashColor_5() { return static_cast<int32_t>(offsetof(FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866, ___flashColor_5)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_flashColor_5() const { return ___flashColor_5; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_flashColor_5() { return &___flashColor_5; }
	inline void set_flashColor_5(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___flashColor_5 = value;
	}

	inline static int32_t get_offset_of_numberOfFlashes_6() { return static_cast<int32_t>(offsetof(FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866, ___numberOfFlashes_6)); }
	inline int32_t get_numberOfFlashes_6() const { return ___numberOfFlashes_6; }
	inline int32_t* get_address_of_numberOfFlashes_6() { return &___numberOfFlashes_6; }
	inline void set_numberOfFlashes_6(int32_t value)
	{
		___numberOfFlashes_6 = value;
	}

	inline static int32_t get_offset_of_flashDelay_7() { return static_cast<int32_t>(offsetof(FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866, ___flashDelay_7)); }
	inline float get_flashDelay_7() const { return ___flashDelay_7; }
	inline float* get_address_of_flashDelay_7() { return &___flashDelay_7; }
	inline void set_flashDelay_7(float value)
	{
		___flashDelay_7 = value;
	}

	inline static int32_t get_offset_of_isFlashing_8() { return static_cast<int32_t>(offsetof(FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866, ___isFlashing_8)); }
	inline bool get_isFlashing_8() const { return ___isFlashing_8; }
	inline bool* get_address_of_isFlashing_8() { return &___isFlashing_8; }
	inline void set_isFlashing_8(bool value)
	{
		___isFlashing_8 = value;
	}
};


// GameMenu
struct GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject GameMenu::creditsPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___creditsPanel_4;
	// UnityEngine.GameObject GameMenu::gamePanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___gamePanel_5;
	// System.String GameMenu::levelToLoad
	String_t* ___levelToLoad_6;
	// VectorValue GameMenu::playerPosition
	VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD * ___playerPosition_7;
	// UnityEngine.Vector2 GameMenu::playerPos
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___playerPos_8;

public:
	inline static int32_t get_offset_of_creditsPanel_4() { return static_cast<int32_t>(offsetof(GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64, ___creditsPanel_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_creditsPanel_4() const { return ___creditsPanel_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_creditsPanel_4() { return &___creditsPanel_4; }
	inline void set_creditsPanel_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___creditsPanel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___creditsPanel_4), (void*)value);
	}

	inline static int32_t get_offset_of_gamePanel_5() { return static_cast<int32_t>(offsetof(GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64, ___gamePanel_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_gamePanel_5() const { return ___gamePanel_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_gamePanel_5() { return &___gamePanel_5; }
	inline void set_gamePanel_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___gamePanel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gamePanel_5), (void*)value);
	}

	inline static int32_t get_offset_of_levelToLoad_6() { return static_cast<int32_t>(offsetof(GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64, ___levelToLoad_6)); }
	inline String_t* get_levelToLoad_6() const { return ___levelToLoad_6; }
	inline String_t** get_address_of_levelToLoad_6() { return &___levelToLoad_6; }
	inline void set_levelToLoad_6(String_t* value)
	{
		___levelToLoad_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levelToLoad_6), (void*)value);
	}

	inline static int32_t get_offset_of_playerPosition_7() { return static_cast<int32_t>(offsetof(GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64, ___playerPosition_7)); }
	inline VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD * get_playerPosition_7() const { return ___playerPosition_7; }
	inline VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD ** get_address_of_playerPosition_7() { return &___playerPosition_7; }
	inline void set_playerPosition_7(VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD * value)
	{
		___playerPosition_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerPosition_7), (void*)value);
	}

	inline static int32_t get_offset_of_playerPos_8() { return static_cast<int32_t>(offsetof(GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64, ___playerPos_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_playerPos_8() const { return ___playerPos_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_playerPos_8() { return &___playerPos_8; }
	inline void set_playerPos_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___playerPos_8 = value;
	}
};


// Health
struct Health_tB86D9293C9CF1E5B8E4C7271395F56DD4C67AE96  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 Health::maxHealth
	int32_t ___maxHealth_4;
	// System.Int32 Health::currentHealth
	int32_t ___currentHealth_5;

public:
	inline static int32_t get_offset_of_maxHealth_4() { return static_cast<int32_t>(offsetof(Health_tB86D9293C9CF1E5B8E4C7271395F56DD4C67AE96, ___maxHealth_4)); }
	inline int32_t get_maxHealth_4() const { return ___maxHealth_4; }
	inline int32_t* get_address_of_maxHealth_4() { return &___maxHealth_4; }
	inline void set_maxHealth_4(int32_t value)
	{
		___maxHealth_4 = value;
	}

	inline static int32_t get_offset_of_currentHealth_5() { return static_cast<int32_t>(offsetof(Health_tB86D9293C9CF1E5B8E4C7271395F56DD4C67AE96, ___currentHealth_5)); }
	inline int32_t get_currentHealth_5() const { return ___currentHealth_5; }
	inline int32_t* get_address_of_currentHealth_5() { return &___currentHealth_5; }
	inline void set_currentHealth_5(int32_t value)
	{
		___currentHealth_5 = value;
	}
};


// HeartManager
struct HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Image[] HeartManager::hearts
	ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* ___hearts_4;
	// UnityEngine.Sprite HeartManager::fullHeart
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___fullHeart_5;
	// UnityEngine.Sprite HeartManager::halfFullHeart
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___halfFullHeart_6;
	// UnityEngine.Sprite HeartManager::emptyHeart
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___emptyHeart_7;
	// FloatValue HeartManager::heartContainers
	FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * ___heartContainers_8;
	// FloatValue HeartManager::playerCurrentHealth
	FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * ___playerCurrentHealth_9;

public:
	inline static int32_t get_offset_of_hearts_4() { return static_cast<int32_t>(offsetof(HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25, ___hearts_4)); }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* get_hearts_4() const { return ___hearts_4; }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224** get_address_of_hearts_4() { return &___hearts_4; }
	inline void set_hearts_4(ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* value)
	{
		___hearts_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hearts_4), (void*)value);
	}

	inline static int32_t get_offset_of_fullHeart_5() { return static_cast<int32_t>(offsetof(HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25, ___fullHeart_5)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_fullHeart_5() const { return ___fullHeart_5; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_fullHeart_5() { return &___fullHeart_5; }
	inline void set_fullHeart_5(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___fullHeart_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fullHeart_5), (void*)value);
	}

	inline static int32_t get_offset_of_halfFullHeart_6() { return static_cast<int32_t>(offsetof(HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25, ___halfFullHeart_6)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_halfFullHeart_6() const { return ___halfFullHeart_6; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_halfFullHeart_6() { return &___halfFullHeart_6; }
	inline void set_halfFullHeart_6(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___halfFullHeart_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___halfFullHeart_6), (void*)value);
	}

	inline static int32_t get_offset_of_emptyHeart_7() { return static_cast<int32_t>(offsetof(HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25, ___emptyHeart_7)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_emptyHeart_7() const { return ___emptyHeart_7; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_emptyHeart_7() { return &___emptyHeart_7; }
	inline void set_emptyHeart_7(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___emptyHeart_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___emptyHeart_7), (void*)value);
	}

	inline static int32_t get_offset_of_heartContainers_8() { return static_cast<int32_t>(offsetof(HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25, ___heartContainers_8)); }
	inline FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * get_heartContainers_8() const { return ___heartContainers_8; }
	inline FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 ** get_address_of_heartContainers_8() { return &___heartContainers_8; }
	inline void set_heartContainers_8(FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * value)
	{
		___heartContainers_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___heartContainers_8), (void*)value);
	}

	inline static int32_t get_offset_of_playerCurrentHealth_9() { return static_cast<int32_t>(offsetof(HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25, ___playerCurrentHealth_9)); }
	inline FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * get_playerCurrentHealth_9() const { return ___playerCurrentHealth_9; }
	inline FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 ** get_address_of_playerCurrentHealth_9() { return &___playerCurrentHealth_9; }
	inline void set_playerCurrentHealth_9(FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * value)
	{
		___playerCurrentHealth_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerCurrentHealth_9), (void*)value);
	}
};


// Interactable
struct Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean Interactable::playerInRange
	bool ___playerInRange_4;
	// System.String Interactable::otherTag
	String_t* ___otherTag_5;
	// Notification Interactable::myNotification
	Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * ___myNotification_6;

public:
	inline static int32_t get_offset_of_playerInRange_4() { return static_cast<int32_t>(offsetof(Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7, ___playerInRange_4)); }
	inline bool get_playerInRange_4() const { return ___playerInRange_4; }
	inline bool* get_address_of_playerInRange_4() { return &___playerInRange_4; }
	inline void set_playerInRange_4(bool value)
	{
		___playerInRange_4 = value;
	}

	inline static int32_t get_offset_of_otherTag_5() { return static_cast<int32_t>(offsetof(Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7, ___otherTag_5)); }
	inline String_t* get_otherTag_5() const { return ___otherTag_5; }
	inline String_t** get_address_of_otherTag_5() { return &___otherTag_5; }
	inline void set_otherTag_5(String_t* value)
	{
		___otherTag_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___otherTag_5), (void*)value);
	}

	inline static int32_t get_offset_of_myNotification_6() { return static_cast<int32_t>(offsetof(Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7, ___myNotification_6)); }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * get_myNotification_6() const { return ___myNotification_6; }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F ** get_address_of_myNotification_6() { return &___myNotification_6; }
	inline void set_myNotification_6(Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * value)
	{
		___myNotification_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myNotification_6), (void*)value);
	}
};


// Knockback
struct Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String Knockback::otherTag
	String_t* ___otherTag_4;
	// System.Single Knockback::knockTime
	float ___knockTime_5;
	// System.Single Knockback::knockStrength
	float ___knockStrength_6;
	// System.Single Knockback::thrust
	float ___thrust_7;
	// System.Single Knockback::damage
	float ___damage_8;

public:
	inline static int32_t get_offset_of_otherTag_4() { return static_cast<int32_t>(offsetof(Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4, ___otherTag_4)); }
	inline String_t* get_otherTag_4() const { return ___otherTag_4; }
	inline String_t** get_address_of_otherTag_4() { return &___otherTag_4; }
	inline void set_otherTag_4(String_t* value)
	{
		___otherTag_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___otherTag_4), (void*)value);
	}

	inline static int32_t get_offset_of_knockTime_5() { return static_cast<int32_t>(offsetof(Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4, ___knockTime_5)); }
	inline float get_knockTime_5() const { return ___knockTime_5; }
	inline float* get_address_of_knockTime_5() { return &___knockTime_5; }
	inline void set_knockTime_5(float value)
	{
		___knockTime_5 = value;
	}

	inline static int32_t get_offset_of_knockStrength_6() { return static_cast<int32_t>(offsetof(Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4, ___knockStrength_6)); }
	inline float get_knockStrength_6() const { return ___knockStrength_6; }
	inline float* get_address_of_knockStrength_6() { return &___knockStrength_6; }
	inline void set_knockStrength_6(float value)
	{
		___knockStrength_6 = value;
	}

	inline static int32_t get_offset_of_thrust_7() { return static_cast<int32_t>(offsetof(Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4, ___thrust_7)); }
	inline float get_thrust_7() const { return ___thrust_7; }
	inline float* get_address_of_thrust_7() { return &___thrust_7; }
	inline void set_thrust_7(float value)
	{
		___thrust_7 = value;
	}

	inline static int32_t get_offset_of_damage_8() { return static_cast<int32_t>(offsetof(Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4, ___damage_8)); }
	inline float get_damage_8() const { return ___damage_8; }
	inline float* get_address_of_damage_8() { return &___damage_8; }
	inline void set_damage_8(float value)
	{
		___damage_8 = value;
	}
};


// Magic
struct Magic_t0CC50A162DDF9C4C4549E05883368ACD19D8F36C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 Magic::currentMagic
	int32_t ___currentMagic_4;
	// System.Int32 Magic::maxMagic
	int32_t ___maxMagic_5;

public:
	inline static int32_t get_offset_of_currentMagic_4() { return static_cast<int32_t>(offsetof(Magic_t0CC50A162DDF9C4C4549E05883368ACD19D8F36C, ___currentMagic_4)); }
	inline int32_t get_currentMagic_4() const { return ___currentMagic_4; }
	inline int32_t* get_address_of_currentMagic_4() { return &___currentMagic_4; }
	inline void set_currentMagic_4(int32_t value)
	{
		___currentMagic_4 = value;
	}

	inline static int32_t get_offset_of_maxMagic_5() { return static_cast<int32_t>(offsetof(Magic_t0CC50A162DDF9C4C4549E05883368ACD19D8F36C, ___maxMagic_5)); }
	inline int32_t get_maxMagic_5() const { return ___maxMagic_5; }
	inline int32_t* get_address_of_maxMagic_5() { return &___maxMagic_5; }
	inline void set_maxMagic_5(int32_t value)
	{
		___maxMagic_5 = value;
	}
};


// Movement
struct Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Movement::speed
	float ___speed_4;
	// UnityEngine.Rigidbody2D Movement::myRigidbody
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___myRigidbody_5;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_myRigidbody_5() { return static_cast<int32_t>(offsetof(Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C, ___myRigidbody_5)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_myRigidbody_5() const { return ___myRigidbody_5; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_myRigidbody_5() { return &___myRigidbody_5; }
	inline void set_myRigidbody_5(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___myRigidbody_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myRigidbody_5), (void*)value);
	}
};


// NPC
struct NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean NPC::playerinRange
	bool ___playerinRange_4;
	// System.Int32 NPC::health
	int32_t ___health_5;
	// System.Single NPC::moveSpeed
	float ___moveSpeed_6;
	// System.Single NPC::chaseRadius
	float ___chaseRadius_7;
	// System.Single NPC::idleRadius
	float ___idleRadius_8;
	// UnityEngine.Transform NPC::homePosition
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___homePosition_9;
	// UnityEngine.Transform NPC::target
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target_10;
	// UnityEngine.Rigidbody2D NPC::myRigidbody
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___myRigidbody_11;

public:
	inline static int32_t get_offset_of_playerinRange_4() { return static_cast<int32_t>(offsetof(NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6, ___playerinRange_4)); }
	inline bool get_playerinRange_4() const { return ___playerinRange_4; }
	inline bool* get_address_of_playerinRange_4() { return &___playerinRange_4; }
	inline void set_playerinRange_4(bool value)
	{
		___playerinRange_4 = value;
	}

	inline static int32_t get_offset_of_health_5() { return static_cast<int32_t>(offsetof(NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6, ___health_5)); }
	inline int32_t get_health_5() const { return ___health_5; }
	inline int32_t* get_address_of_health_5() { return &___health_5; }
	inline void set_health_5(int32_t value)
	{
		___health_5 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_6() { return static_cast<int32_t>(offsetof(NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6, ___moveSpeed_6)); }
	inline float get_moveSpeed_6() const { return ___moveSpeed_6; }
	inline float* get_address_of_moveSpeed_6() { return &___moveSpeed_6; }
	inline void set_moveSpeed_6(float value)
	{
		___moveSpeed_6 = value;
	}

	inline static int32_t get_offset_of_chaseRadius_7() { return static_cast<int32_t>(offsetof(NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6, ___chaseRadius_7)); }
	inline float get_chaseRadius_7() const { return ___chaseRadius_7; }
	inline float* get_address_of_chaseRadius_7() { return &___chaseRadius_7; }
	inline void set_chaseRadius_7(float value)
	{
		___chaseRadius_7 = value;
	}

	inline static int32_t get_offset_of_idleRadius_8() { return static_cast<int32_t>(offsetof(NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6, ___idleRadius_8)); }
	inline float get_idleRadius_8() const { return ___idleRadius_8; }
	inline float* get_address_of_idleRadius_8() { return &___idleRadius_8; }
	inline void set_idleRadius_8(float value)
	{
		___idleRadius_8 = value;
	}

	inline static int32_t get_offset_of_homePosition_9() { return static_cast<int32_t>(offsetof(NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6, ___homePosition_9)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_homePosition_9() const { return ___homePosition_9; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_homePosition_9() { return &___homePosition_9; }
	inline void set_homePosition_9(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___homePosition_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___homePosition_9), (void*)value);
	}

	inline static int32_t get_offset_of_target_10() { return static_cast<int32_t>(offsetof(NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6, ___target_10)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_target_10() const { return ___target_10; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_target_10() { return &___target_10; }
	inline void set_target_10(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___target_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_10), (void*)value);
	}

	inline static int32_t get_offset_of_myRigidbody_11() { return static_cast<int32_t>(offsetof(NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6, ___myRigidbody_11)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_myRigidbody_11() const { return ___myRigidbody_11; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_myRigidbody_11() { return &___myRigidbody_11; }
	inline void set_myRigidbody_11(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___myRigidbody_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myRigidbody_11), (void*)value);
	}
};


// NotificationListener
struct NotificationListener_t576AF20BBC693CDD899148976A9D0D3778058225  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Events.UnityEvent NotificationListener::myEvent
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___myEvent_4;
	// Notification NotificationListener::myNotification
	Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * ___myNotification_5;

public:
	inline static int32_t get_offset_of_myEvent_4() { return static_cast<int32_t>(offsetof(NotificationListener_t576AF20BBC693CDD899148976A9D0D3778058225, ___myEvent_4)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_myEvent_4() const { return ___myEvent_4; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_myEvent_4() { return &___myEvent_4; }
	inline void set_myEvent_4(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___myEvent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myEvent_4), (void*)value);
	}

	inline static int32_t get_offset_of_myNotification_5() { return static_cast<int32_t>(offsetof(NotificationListener_t576AF20BBC693CDD899148976A9D0D3778058225, ___myNotification_5)); }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * get_myNotification_5() const { return ___myNotification_5; }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F ** get_address_of_myNotification_5() { return &___myNotification_5; }
	inline void set_myNotification_5(Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * value)
	{
		___myNotification_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myNotification_5), (void*)value);
	}
};


// MegaDad.OverheadMegaDadController
struct OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// MegaDad.OverheadMegaDadController/State MegaDad.OverheadMegaDadController::m_State
	int32_t ___m_State_4;
	// UnityEngine.GameObject MegaDad.OverheadMegaDadController::m_SplashPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_SplashPrefab_8;
	// UnityEngine.Vector2 MegaDad.OverheadMegaDadController::m_Facing
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Facing_9;
	// UnityEngine.Animator MegaDad.OverheadMegaDadController::m_Animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___m_Animator_10;
	// System.Single MegaDad.OverheadMegaDadController::m_MoveTimer
	float ___m_MoveTimer_11;
	// UnityEngine.Vector2 MegaDad.OverheadMegaDadController::m_MovingFrom
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_MovingFrom_12;
	// UnityEngine.Vector2 MegaDad.OverheadMegaDadController::m_MovingTo
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_MovingTo_13;
	// UnityEngine.Vector2 MegaDad.OverheadMegaDadController::m_SpawnPoint
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_SpawnPoint_14;
	// UnityEngine.SpriteRenderer MegaDad.OverheadMegaDadController::m_Renderer
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___m_Renderer_15;

public:
	inline static int32_t get_offset_of_m_State_4() { return static_cast<int32_t>(offsetof(OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571, ___m_State_4)); }
	inline int32_t get_m_State_4() const { return ___m_State_4; }
	inline int32_t* get_address_of_m_State_4() { return &___m_State_4; }
	inline void set_m_State_4(int32_t value)
	{
		___m_State_4 = value;
	}

	inline static int32_t get_offset_of_m_SplashPrefab_8() { return static_cast<int32_t>(offsetof(OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571, ___m_SplashPrefab_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_SplashPrefab_8() const { return ___m_SplashPrefab_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_SplashPrefab_8() { return &___m_SplashPrefab_8; }
	inline void set_m_SplashPrefab_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_SplashPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SplashPrefab_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Facing_9() { return static_cast<int32_t>(offsetof(OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571, ___m_Facing_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Facing_9() const { return ___m_Facing_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Facing_9() { return &___m_Facing_9; }
	inline void set_m_Facing_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Facing_9 = value;
	}

	inline static int32_t get_offset_of_m_Animator_10() { return static_cast<int32_t>(offsetof(OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571, ___m_Animator_10)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_m_Animator_10() const { return ___m_Animator_10; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_m_Animator_10() { return &___m_Animator_10; }
	inline void set_m_Animator_10(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___m_Animator_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Animator_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_MoveTimer_11() { return static_cast<int32_t>(offsetof(OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571, ___m_MoveTimer_11)); }
	inline float get_m_MoveTimer_11() const { return ___m_MoveTimer_11; }
	inline float* get_address_of_m_MoveTimer_11() { return &___m_MoveTimer_11; }
	inline void set_m_MoveTimer_11(float value)
	{
		___m_MoveTimer_11 = value;
	}

	inline static int32_t get_offset_of_m_MovingFrom_12() { return static_cast<int32_t>(offsetof(OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571, ___m_MovingFrom_12)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_MovingFrom_12() const { return ___m_MovingFrom_12; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_MovingFrom_12() { return &___m_MovingFrom_12; }
	inline void set_m_MovingFrom_12(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_MovingFrom_12 = value;
	}

	inline static int32_t get_offset_of_m_MovingTo_13() { return static_cast<int32_t>(offsetof(OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571, ___m_MovingTo_13)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_MovingTo_13() const { return ___m_MovingTo_13; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_MovingTo_13() { return &___m_MovingTo_13; }
	inline void set_m_MovingTo_13(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_MovingTo_13 = value;
	}

	inline static int32_t get_offset_of_m_SpawnPoint_14() { return static_cast<int32_t>(offsetof(OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571, ___m_SpawnPoint_14)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_SpawnPoint_14() const { return ___m_SpawnPoint_14; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_SpawnPoint_14() { return &___m_SpawnPoint_14; }
	inline void set_m_SpawnPoint_14(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_SpawnPoint_14 = value;
	}

	inline static int32_t get_offset_of_m_Renderer_15() { return static_cast<int32_t>(offsetof(OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571, ___m_Renderer_15)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_m_Renderer_15() const { return ___m_Renderer_15; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_m_Renderer_15() { return &___m_Renderer_15; }
	inline void set_m_Renderer_15(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___m_Renderer_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Renderer_15), (void*)value);
	}
};


// PauseManager
struct PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean PauseManager::isPaused
	bool ___isPaused_4;
	// UnityEngine.GameObject PauseManager::pauseMenu
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___pauseMenu_5;
	// UnityEngine.GameObject PauseManager::inventoryMenu
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___inventoryMenu_6;
	// FloatValue PauseManager::gameSpeed
	FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * ___gameSpeed_7;
	// System.String PauseManager::menuSceneString
	String_t* ___menuSceneString_8;

public:
	inline static int32_t get_offset_of_isPaused_4() { return static_cast<int32_t>(offsetof(PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE, ___isPaused_4)); }
	inline bool get_isPaused_4() const { return ___isPaused_4; }
	inline bool* get_address_of_isPaused_4() { return &___isPaused_4; }
	inline void set_isPaused_4(bool value)
	{
		___isPaused_4 = value;
	}

	inline static int32_t get_offset_of_pauseMenu_5() { return static_cast<int32_t>(offsetof(PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE, ___pauseMenu_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_pauseMenu_5() const { return ___pauseMenu_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_pauseMenu_5() { return &___pauseMenu_5; }
	inline void set_pauseMenu_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___pauseMenu_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pauseMenu_5), (void*)value);
	}

	inline static int32_t get_offset_of_inventoryMenu_6() { return static_cast<int32_t>(offsetof(PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE, ___inventoryMenu_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_inventoryMenu_6() const { return ___inventoryMenu_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_inventoryMenu_6() { return &___inventoryMenu_6; }
	inline void set_inventoryMenu_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___inventoryMenu_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inventoryMenu_6), (void*)value);
	}

	inline static int32_t get_offset_of_gameSpeed_7() { return static_cast<int32_t>(offsetof(PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE, ___gameSpeed_7)); }
	inline FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * get_gameSpeed_7() const { return ___gameSpeed_7; }
	inline FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 ** get_address_of_gameSpeed_7() { return &___gameSpeed_7; }
	inline void set_gameSpeed_7(FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10 * value)
	{
		___gameSpeed_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameSpeed_7), (void*)value);
	}

	inline static int32_t get_offset_of_menuSceneString_8() { return static_cast<int32_t>(offsetof(PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE, ___menuSceneString_8)); }
	inline String_t* get_menuSceneString_8() const { return ___menuSceneString_8; }
	inline String_t** get_address_of_menuSceneString_8() { return &___menuSceneString_8; }
	inline void set_menuSceneString_8(String_t* value)
	{
		___menuSceneString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuSceneString_8), (void*)value);
	}
};


// PlayerHit
struct PlayerHit_t783EB4FD807A319FAB7A7B7015FB1F6A34756133  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// PlayerMoney
struct PlayerMoney_tCEB0119C0B290EA5E47264E49FB4D95AD5CD3E9A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 PlayerMoney::currentMoney
	int32_t ___currentMoney_4;
	// System.Int32 PlayerMoney::maxMoney
	int32_t ___maxMoney_5;

public:
	inline static int32_t get_offset_of_currentMoney_4() { return static_cast<int32_t>(offsetof(PlayerMoney_tCEB0119C0B290EA5E47264E49FB4D95AD5CD3E9A, ___currentMoney_4)); }
	inline int32_t get_currentMoney_4() const { return ___currentMoney_4; }
	inline int32_t* get_address_of_currentMoney_4() { return &___currentMoney_4; }
	inline void set_currentMoney_4(int32_t value)
	{
		___currentMoney_4 = value;
	}

	inline static int32_t get_offset_of_maxMoney_5() { return static_cast<int32_t>(offsetof(PlayerMoney_tCEB0119C0B290EA5E47264E49FB4D95AD5CD3E9A, ___maxMoney_5)); }
	inline int32_t get_maxMoney_5() const { return ___maxMoney_5; }
	inline int32_t* get_address_of_maxMoney_5() { return &___maxMoney_5; }
	inline void set_maxMoney_5(int32_t value)
	{
		___maxMoney_5 = value;
	}
};


// ReceiveItem
struct ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.SpriteRenderer ReceiveItem::mySprite
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___mySprite_4;
	// SpriteValue ReceiveItem::receivedSprite
	SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C * ___receivedSprite_5;
	// AnimatorController ReceiveItem::anim
	AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01 * ___anim_6;
	// StateMachine ReceiveItem::myState
	StateMachine_t823E5CFDC9E27E10A2C57DB9303909CA2D8BC49A * ___myState_7;
	// System.Boolean ReceiveItem::isActive
	bool ___isActive_8;
	// Notification ReceiveItem::dialogNotification
	Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * ___dialogNotification_9;

public:
	inline static int32_t get_offset_of_mySprite_4() { return static_cast<int32_t>(offsetof(ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7, ___mySprite_4)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_mySprite_4() const { return ___mySprite_4; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_mySprite_4() { return &___mySprite_4; }
	inline void set_mySprite_4(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___mySprite_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mySprite_4), (void*)value);
	}

	inline static int32_t get_offset_of_receivedSprite_5() { return static_cast<int32_t>(offsetof(ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7, ___receivedSprite_5)); }
	inline SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C * get_receivedSprite_5() const { return ___receivedSprite_5; }
	inline SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C ** get_address_of_receivedSprite_5() { return &___receivedSprite_5; }
	inline void set_receivedSprite_5(SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C * value)
	{
		___receivedSprite_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___receivedSprite_5), (void*)value);
	}

	inline static int32_t get_offset_of_anim_6() { return static_cast<int32_t>(offsetof(ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7, ___anim_6)); }
	inline AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01 * get_anim_6() const { return ___anim_6; }
	inline AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01 ** get_address_of_anim_6() { return &___anim_6; }
	inline void set_anim_6(AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01 * value)
	{
		___anim_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anim_6), (void*)value);
	}

	inline static int32_t get_offset_of_myState_7() { return static_cast<int32_t>(offsetof(ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7, ___myState_7)); }
	inline StateMachine_t823E5CFDC9E27E10A2C57DB9303909CA2D8BC49A * get_myState_7() const { return ___myState_7; }
	inline StateMachine_t823E5CFDC9E27E10A2C57DB9303909CA2D8BC49A ** get_address_of_myState_7() { return &___myState_7; }
	inline void set_myState_7(StateMachine_t823E5CFDC9E27E10A2C57DB9303909CA2D8BC49A * value)
	{
		___myState_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myState_7), (void*)value);
	}

	inline static int32_t get_offset_of_isActive_8() { return static_cast<int32_t>(offsetof(ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7, ___isActive_8)); }
	inline bool get_isActive_8() const { return ___isActive_8; }
	inline bool* get_address_of_isActive_8() { return &___isActive_8; }
	inline void set_isActive_8(bool value)
	{
		___isActive_8 = value;
	}

	inline static int32_t get_offset_of_dialogNotification_9() { return static_cast<int32_t>(offsetof(ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7, ___dialogNotification_9)); }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * get_dialogNotification_9() const { return ___dialogNotification_9; }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F ** get_address_of_dialogNotification_9() { return &___dialogNotification_9; }
	inline void set_dialogNotification_9(Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * value)
	{
		___dialogNotification_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dialogNotification_9), (void*)value);
	}
};


// ResetPlayerPosition
struct ResetPlayerPosition_tA80F12DC82B6A24D6CDD43EAE82E742CAA76632D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// VectorValue ResetPlayerPosition::playerPosition
	VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD * ___playerPosition_4;

public:
	inline static int32_t get_offset_of_playerPosition_4() { return static_cast<int32_t>(offsetof(ResetPlayerPosition_tA80F12DC82B6A24D6CDD43EAE82E742CAA76632D, ___playerPosition_4)); }
	inline VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD * get_playerPosition_4() const { return ___playerPosition_4; }
	inline VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD ** get_address_of_playerPosition_4() { return &___playerPosition_4; }
	inline void set_playerPosition_4(VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD * value)
	{
		___playerPosition_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerPosition_4), (void*)value);
	}
};


// ResetToPosition
struct ResetToPosition_t9E69B3C34D3A9B21F794674DA94578EB3FC0183B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector2 ResetToPosition::resetPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___resetPosition_4;

public:
	inline static int32_t get_offset_of_resetPosition_4() { return static_cast<int32_t>(offsetof(ResetToPosition_t9E69B3C34D3A9B21F794674DA94578EB3FC0183B, ___resetPosition_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_resetPosition_4() const { return ___resetPosition_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_resetPosition_4() { return &___resetPosition_4; }
	inline void set_resetPosition_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___resetPosition_4 = value;
	}
};


// Room
struct Room_t44A97854220FADE8C026E18625929E32B64B3F6A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String Room::roomName
	String_t* ___roomName_4;
	// StringValue Room::roomNameHolder
	StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 * ___roomNameHolder_5;
	// Notification Room::roomnotification
	Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * ___roomnotification_6;
	// System.String Room::playerTag
	String_t* ___playerTag_7;
	// UnityEngine.GameObject[] Room::respawnObjects
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___respawnObjects_8;
	// UnityEngine.GameObject Room::thisCamera
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___thisCamera_9;

public:
	inline static int32_t get_offset_of_roomName_4() { return static_cast<int32_t>(offsetof(Room_t44A97854220FADE8C026E18625929E32B64B3F6A, ___roomName_4)); }
	inline String_t* get_roomName_4() const { return ___roomName_4; }
	inline String_t** get_address_of_roomName_4() { return &___roomName_4; }
	inline void set_roomName_4(String_t* value)
	{
		___roomName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___roomName_4), (void*)value);
	}

	inline static int32_t get_offset_of_roomNameHolder_5() { return static_cast<int32_t>(offsetof(Room_t44A97854220FADE8C026E18625929E32B64B3F6A, ___roomNameHolder_5)); }
	inline StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 * get_roomNameHolder_5() const { return ___roomNameHolder_5; }
	inline StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 ** get_address_of_roomNameHolder_5() { return &___roomNameHolder_5; }
	inline void set_roomNameHolder_5(StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 * value)
	{
		___roomNameHolder_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___roomNameHolder_5), (void*)value);
	}

	inline static int32_t get_offset_of_roomnotification_6() { return static_cast<int32_t>(offsetof(Room_t44A97854220FADE8C026E18625929E32B64B3F6A, ___roomnotification_6)); }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * get_roomnotification_6() const { return ___roomnotification_6; }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F ** get_address_of_roomnotification_6() { return &___roomnotification_6; }
	inline void set_roomnotification_6(Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * value)
	{
		___roomnotification_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___roomnotification_6), (void*)value);
	}

	inline static int32_t get_offset_of_playerTag_7() { return static_cast<int32_t>(offsetof(Room_t44A97854220FADE8C026E18625929E32B64B3F6A, ___playerTag_7)); }
	inline String_t* get_playerTag_7() const { return ___playerTag_7; }
	inline String_t** get_address_of_playerTag_7() { return &___playerTag_7; }
	inline void set_playerTag_7(String_t* value)
	{
		___playerTag_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerTag_7), (void*)value);
	}

	inline static int32_t get_offset_of_respawnObjects_8() { return static_cast<int32_t>(offsetof(Room_t44A97854220FADE8C026E18625929E32B64B3F6A, ___respawnObjects_8)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_respawnObjects_8() const { return ___respawnObjects_8; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_respawnObjects_8() { return &___respawnObjects_8; }
	inline void set_respawnObjects_8(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___respawnObjects_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___respawnObjects_8), (void*)value);
	}

	inline static int32_t get_offset_of_thisCamera_9() { return static_cast<int32_t>(offsetof(Room_t44A97854220FADE8C026E18625929E32B64B3F6A, ___thisCamera_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_thisCamera_9() const { return ___thisCamera_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_thisCamera_9() { return &___thisCamera_9; }
	inline void set_thisCamera_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___thisCamera_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___thisCamera_9), (void*)value);
	}
};


// RoomTransfer
struct RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String RoomTransfer::roomToLoad
	String_t* ___roomToLoad_4;
	// System.String RoomTransfer::playerTag
	String_t* ___playerTag_5;
	// VectorValue RoomTransfer::playerPosInRoomValue
	VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD * ___playerPosInRoomValue_6;
	// UnityEngine.Vector2 RoomTransfer::playerPosInRoom
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___playerPosInRoom_7;

public:
	inline static int32_t get_offset_of_roomToLoad_4() { return static_cast<int32_t>(offsetof(RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC, ___roomToLoad_4)); }
	inline String_t* get_roomToLoad_4() const { return ___roomToLoad_4; }
	inline String_t** get_address_of_roomToLoad_4() { return &___roomToLoad_4; }
	inline void set_roomToLoad_4(String_t* value)
	{
		___roomToLoad_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___roomToLoad_4), (void*)value);
	}

	inline static int32_t get_offset_of_playerTag_5() { return static_cast<int32_t>(offsetof(RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC, ___playerTag_5)); }
	inline String_t* get_playerTag_5() const { return ___playerTag_5; }
	inline String_t** get_address_of_playerTag_5() { return &___playerTag_5; }
	inline void set_playerTag_5(String_t* value)
	{
		___playerTag_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerTag_5), (void*)value);
	}

	inline static int32_t get_offset_of_playerPosInRoomValue_6() { return static_cast<int32_t>(offsetof(RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC, ___playerPosInRoomValue_6)); }
	inline VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD * get_playerPosInRoomValue_6() const { return ___playerPosInRoomValue_6; }
	inline VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD ** get_address_of_playerPosInRoomValue_6() { return &___playerPosInRoomValue_6; }
	inline void set_playerPosInRoomValue_6(VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD * value)
	{
		___playerPosInRoomValue_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerPosInRoomValue_6), (void*)value);
	}

	inline static int32_t get_offset_of_playerPosInRoom_7() { return static_cast<int32_t>(offsetof(RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC, ___playerPosInRoom_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_playerPosInRoom_7() const { return ___playerPosInRoom_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_playerPosInRoom_7() { return &___playerPosInRoom_7; }
	inline void set_playerPosInRoom_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___playerPosInRoom_7 = value;
	}
};


// SOObserver
struct SOObserver_tD018D77957D756C9D4E80B373BCE27677AA0AA21  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// SOObserver SOObserver::instance
	SOObserver_tD018D77957D756C9D4E80B373BCE27677AA0AA21 * ___instance_4;
	// System.Collections.Generic.List`1<UnityEngine.ScriptableObject> SOObserver::objectsInMemory
	List_1_tEB4537E121ED7128292F5E49486823EB846576FE * ___objectsInMemory_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(SOObserver_tD018D77957D756C9D4E80B373BCE27677AA0AA21, ___instance_4)); }
	inline SOObserver_tD018D77957D756C9D4E80B373BCE27677AA0AA21 * get_instance_4() const { return ___instance_4; }
	inline SOObserver_tD018D77957D756C9D4E80B373BCE27677AA0AA21 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(SOObserver_tD018D77957D756C9D4E80B373BCE27677AA0AA21 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___instance_4), (void*)value);
	}

	inline static int32_t get_offset_of_objectsInMemory_5() { return static_cast<int32_t>(offsetof(SOObserver_tD018D77957D756C9D4E80B373BCE27677AA0AA21, ___objectsInMemory_5)); }
	inline List_1_tEB4537E121ED7128292F5E49486823EB846576FE * get_objectsInMemory_5() const { return ___objectsInMemory_5; }
	inline List_1_tEB4537E121ED7128292F5E49486823EB846576FE ** get_address_of_objectsInMemory_5() { return &___objectsInMemory_5; }
	inline void set_objectsInMemory_5(List_1_tEB4537E121ED7128292F5E49486823EB846576FE * value)
	{
		___objectsInMemory_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectsInMemory_5), (void*)value);
	}
};


// SecretTree
struct SecretTree_t2C2115D59ECF1500F65CF282870D390C7782E7D8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single SecretTree::health
	float ___health_4;

public:
	inline static int32_t get_offset_of_health_4() { return static_cast<int32_t>(offsetof(SecretTree_t2C2115D59ECF1500F65CF282870D390C7782E7D8, ___health_4)); }
	inline float get_health_4() const { return ___health_4; }
	inline float* get_address_of_health_4() { return &___health_4; }
	inline void set_health_4(float value)
	{
		___health_4 = value;
	}
};


// SignalListener
struct SignalListener_tA58702930EF45A76FD394D8D9455E41D2E0360C8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// SignalSender SignalListener::signal
	SignalSender_t0075E93BFF2201C4C328193746B0F29E902AACCA * ___signal_4;
	// UnityEngine.Events.UnityEvent SignalListener::signalEvent
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___signalEvent_5;

public:
	inline static int32_t get_offset_of_signal_4() { return static_cast<int32_t>(offsetof(SignalListener_tA58702930EF45A76FD394D8D9455E41D2E0360C8, ___signal_4)); }
	inline SignalSender_t0075E93BFF2201C4C328193746B0F29E902AACCA * get_signal_4() const { return ___signal_4; }
	inline SignalSender_t0075E93BFF2201C4C328193746B0F29E902AACCA ** get_address_of_signal_4() { return &___signal_4; }
	inline void set_signal_4(SignalSender_t0075E93BFF2201C4C328193746B0F29E902AACCA * value)
	{
		___signal_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___signal_4), (void*)value);
	}

	inline static int32_t get_offset_of_signalEvent_5() { return static_cast<int32_t>(offsetof(SignalListener_tA58702930EF45A76FD394D8D9455E41D2E0360C8, ___signalEvent_5)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_signalEvent_5() const { return ___signalEvent_5; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_signalEvent_5() { return &___signalEvent_5; }
	inline void set_signalEvent_5(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___signalEvent_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___signalEvent_5), (void*)value);
	}
};


// StateMachine
struct StateMachine_t823E5CFDC9E27E10A2C57DB9303909CA2D8BC49A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// GenericState StateMachine::myState
	int32_t ___myState_4;

public:
	inline static int32_t get_offset_of_myState_4() { return static_cast<int32_t>(offsetof(StateMachine_t823E5CFDC9E27E10A2C57DB9303909CA2D8BC49A, ___myState_4)); }
	inline int32_t get_myState_4() const { return ___myState_4; }
	inline int32_t* get_address_of_myState_4() { return &___myState_4; }
	inline void set_myState_4(int32_t value)
	{
		___myState_4 = value;
	}
};


// SuperTiled2Unity.SuperColliderComponent
struct SuperColliderComponent_t29E4A8954B61AA659D29FD8F6C89E8C21E071DD1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// SuperTiled2Unity.SuperCustomProperties
struct SuperCustomProperties_t64CCDF0B5920621763A3293FC8F0FBFD877F341F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<SuperTiled2Unity.CustomProperty> SuperTiled2Unity.SuperCustomProperties::m_Properties
	List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73 * ___m_Properties_4;

public:
	inline static int32_t get_offset_of_m_Properties_4() { return static_cast<int32_t>(offsetof(SuperCustomProperties_t64CCDF0B5920621763A3293FC8F0FBFD877F341F, ___m_Properties_4)); }
	inline List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73 * get_m_Properties_4() const { return ___m_Properties_4; }
	inline List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73 ** get_address_of_m_Properties_4() { return &___m_Properties_4; }
	inline void set_m_Properties_4(List_1_tF1490AE0664416BA626E1FF799150BFEDC50EC73 * value)
	{
		___m_Properties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Properties_4), (void*)value);
	}
};


// SuperTiled2Unity.SuperLayer
struct SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String SuperTiled2Unity.SuperLayer::m_TiledName
	String_t* ___m_TiledName_4;
	// System.Single SuperTiled2Unity.SuperLayer::m_OffsetX
	float ___m_OffsetX_5;
	// System.Single SuperTiled2Unity.SuperLayer::m_OffsetY
	float ___m_OffsetY_6;
	// System.Single SuperTiled2Unity.SuperLayer::m_Opacity
	float ___m_Opacity_7;
	// System.Boolean SuperTiled2Unity.SuperLayer::m_Visible
	bool ___m_Visible_8;

public:
	inline static int32_t get_offset_of_m_TiledName_4() { return static_cast<int32_t>(offsetof(SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C, ___m_TiledName_4)); }
	inline String_t* get_m_TiledName_4() const { return ___m_TiledName_4; }
	inline String_t** get_address_of_m_TiledName_4() { return &___m_TiledName_4; }
	inline void set_m_TiledName_4(String_t* value)
	{
		___m_TiledName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TiledName_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_OffsetX_5() { return static_cast<int32_t>(offsetof(SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C, ___m_OffsetX_5)); }
	inline float get_m_OffsetX_5() const { return ___m_OffsetX_5; }
	inline float* get_address_of_m_OffsetX_5() { return &___m_OffsetX_5; }
	inline void set_m_OffsetX_5(float value)
	{
		___m_OffsetX_5 = value;
	}

	inline static int32_t get_offset_of_m_OffsetY_6() { return static_cast<int32_t>(offsetof(SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C, ___m_OffsetY_6)); }
	inline float get_m_OffsetY_6() const { return ___m_OffsetY_6; }
	inline float* get_address_of_m_OffsetY_6() { return &___m_OffsetY_6; }
	inline void set_m_OffsetY_6(float value)
	{
		___m_OffsetY_6 = value;
	}

	inline static int32_t get_offset_of_m_Opacity_7() { return static_cast<int32_t>(offsetof(SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C, ___m_Opacity_7)); }
	inline float get_m_Opacity_7() const { return ___m_Opacity_7; }
	inline float* get_address_of_m_Opacity_7() { return &___m_Opacity_7; }
	inline void set_m_Opacity_7(float value)
	{
		___m_Opacity_7 = value;
	}

	inline static int32_t get_offset_of_m_Visible_8() { return static_cast<int32_t>(offsetof(SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C, ___m_Visible_8)); }
	inline bool get_m_Visible_8() const { return ___m_Visible_8; }
	inline bool* get_address_of_m_Visible_8() { return &___m_Visible_8; }
	inline void set_m_Visible_8(bool value)
	{
		___m_Visible_8 = value;
	}
};


// SuperTiled2Unity.SuperMap
struct SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String SuperTiled2Unity.SuperMap::m_Version
	String_t* ___m_Version_4;
	// System.String SuperTiled2Unity.SuperMap::m_TiledVersion
	String_t* ___m_TiledVersion_5;
	// SuperTiled2Unity.MapOrientation SuperTiled2Unity.SuperMap::m_Orientation
	int32_t ___m_Orientation_6;
	// SuperTiled2Unity.MapRenderOrder SuperTiled2Unity.SuperMap::m_RenderOrder
	int32_t ___m_RenderOrder_7;
	// System.Int32 SuperTiled2Unity.SuperMap::m_Width
	int32_t ___m_Width_8;
	// System.Int32 SuperTiled2Unity.SuperMap::m_Height
	int32_t ___m_Height_9;
	// System.Int32 SuperTiled2Unity.SuperMap::m_TileWidth
	int32_t ___m_TileWidth_10;
	// System.Int32 SuperTiled2Unity.SuperMap::m_TileHeight
	int32_t ___m_TileHeight_11;
	// System.Int32 SuperTiled2Unity.SuperMap::m_HexSideLength
	int32_t ___m_HexSideLength_12;
	// SuperTiled2Unity.StaggerAxis SuperTiled2Unity.SuperMap::m_StaggerAxis
	int32_t ___m_StaggerAxis_13;
	// SuperTiled2Unity.StaggerIndex SuperTiled2Unity.SuperMap::m_StaggerIndex
	int32_t ___m_StaggerIndex_14;
	// System.Boolean SuperTiled2Unity.SuperMap::m_Infinite
	bool ___m_Infinite_15;
	// UnityEngine.Color SuperTiled2Unity.SuperMap::m_BackgroundColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_BackgroundColor_16;
	// System.Int32 SuperTiled2Unity.SuperMap::m_NextObjectId
	int32_t ___m_NextObjectId_17;

public:
	inline static int32_t get_offset_of_m_Version_4() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_Version_4)); }
	inline String_t* get_m_Version_4() const { return ___m_Version_4; }
	inline String_t** get_address_of_m_Version_4() { return &___m_Version_4; }
	inline void set_m_Version_4(String_t* value)
	{
		___m_Version_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Version_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_TiledVersion_5() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_TiledVersion_5)); }
	inline String_t* get_m_TiledVersion_5() const { return ___m_TiledVersion_5; }
	inline String_t** get_address_of_m_TiledVersion_5() { return &___m_TiledVersion_5; }
	inline void set_m_TiledVersion_5(String_t* value)
	{
		___m_TiledVersion_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TiledVersion_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Orientation_6() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_Orientation_6)); }
	inline int32_t get_m_Orientation_6() const { return ___m_Orientation_6; }
	inline int32_t* get_address_of_m_Orientation_6() { return &___m_Orientation_6; }
	inline void set_m_Orientation_6(int32_t value)
	{
		___m_Orientation_6 = value;
	}

	inline static int32_t get_offset_of_m_RenderOrder_7() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_RenderOrder_7)); }
	inline int32_t get_m_RenderOrder_7() const { return ___m_RenderOrder_7; }
	inline int32_t* get_address_of_m_RenderOrder_7() { return &___m_RenderOrder_7; }
	inline void set_m_RenderOrder_7(int32_t value)
	{
		___m_RenderOrder_7 = value;
	}

	inline static int32_t get_offset_of_m_Width_8() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_Width_8)); }
	inline int32_t get_m_Width_8() const { return ___m_Width_8; }
	inline int32_t* get_address_of_m_Width_8() { return &___m_Width_8; }
	inline void set_m_Width_8(int32_t value)
	{
		___m_Width_8 = value;
	}

	inline static int32_t get_offset_of_m_Height_9() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_Height_9)); }
	inline int32_t get_m_Height_9() const { return ___m_Height_9; }
	inline int32_t* get_address_of_m_Height_9() { return &___m_Height_9; }
	inline void set_m_Height_9(int32_t value)
	{
		___m_Height_9 = value;
	}

	inline static int32_t get_offset_of_m_TileWidth_10() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_TileWidth_10)); }
	inline int32_t get_m_TileWidth_10() const { return ___m_TileWidth_10; }
	inline int32_t* get_address_of_m_TileWidth_10() { return &___m_TileWidth_10; }
	inline void set_m_TileWidth_10(int32_t value)
	{
		___m_TileWidth_10 = value;
	}

	inline static int32_t get_offset_of_m_TileHeight_11() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_TileHeight_11)); }
	inline int32_t get_m_TileHeight_11() const { return ___m_TileHeight_11; }
	inline int32_t* get_address_of_m_TileHeight_11() { return &___m_TileHeight_11; }
	inline void set_m_TileHeight_11(int32_t value)
	{
		___m_TileHeight_11 = value;
	}

	inline static int32_t get_offset_of_m_HexSideLength_12() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_HexSideLength_12)); }
	inline int32_t get_m_HexSideLength_12() const { return ___m_HexSideLength_12; }
	inline int32_t* get_address_of_m_HexSideLength_12() { return &___m_HexSideLength_12; }
	inline void set_m_HexSideLength_12(int32_t value)
	{
		___m_HexSideLength_12 = value;
	}

	inline static int32_t get_offset_of_m_StaggerAxis_13() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_StaggerAxis_13)); }
	inline int32_t get_m_StaggerAxis_13() const { return ___m_StaggerAxis_13; }
	inline int32_t* get_address_of_m_StaggerAxis_13() { return &___m_StaggerAxis_13; }
	inline void set_m_StaggerAxis_13(int32_t value)
	{
		___m_StaggerAxis_13 = value;
	}

	inline static int32_t get_offset_of_m_StaggerIndex_14() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_StaggerIndex_14)); }
	inline int32_t get_m_StaggerIndex_14() const { return ___m_StaggerIndex_14; }
	inline int32_t* get_address_of_m_StaggerIndex_14() { return &___m_StaggerIndex_14; }
	inline void set_m_StaggerIndex_14(int32_t value)
	{
		___m_StaggerIndex_14 = value;
	}

	inline static int32_t get_offset_of_m_Infinite_15() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_Infinite_15)); }
	inline bool get_m_Infinite_15() const { return ___m_Infinite_15; }
	inline bool* get_address_of_m_Infinite_15() { return &___m_Infinite_15; }
	inline void set_m_Infinite_15(bool value)
	{
		___m_Infinite_15 = value;
	}

	inline static int32_t get_offset_of_m_BackgroundColor_16() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_BackgroundColor_16)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_BackgroundColor_16() const { return ___m_BackgroundColor_16; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_BackgroundColor_16() { return &___m_BackgroundColor_16; }
	inline void set_m_BackgroundColor_16(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_BackgroundColor_16 = value;
	}

	inline static int32_t get_offset_of_m_NextObjectId_17() { return static_cast<int32_t>(offsetof(SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96, ___m_NextObjectId_17)); }
	inline int32_t get_m_NextObjectId_17() const { return ___m_NextObjectId_17; }
	inline int32_t* get_address_of_m_NextObjectId_17() { return &___m_NextObjectId_17; }
	inline void set_m_NextObjectId_17(int32_t value)
	{
		___m_NextObjectId_17 = value;
	}
};


// SuperTiled2Unity.SuperObject
struct SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 SuperTiled2Unity.SuperObject::m_Id
	int32_t ___m_Id_4;
	// System.String SuperTiled2Unity.SuperObject::m_TiledName
	String_t* ___m_TiledName_5;
	// System.String SuperTiled2Unity.SuperObject::m_Type
	String_t* ___m_Type_6;
	// System.Single SuperTiled2Unity.SuperObject::m_X
	float ___m_X_7;
	// System.Single SuperTiled2Unity.SuperObject::m_Y
	float ___m_Y_8;
	// System.Single SuperTiled2Unity.SuperObject::m_Width
	float ___m_Width_9;
	// System.Single SuperTiled2Unity.SuperObject::m_Height
	float ___m_Height_10;
	// System.Single SuperTiled2Unity.SuperObject::m_Rotation
	float ___m_Rotation_11;
	// System.UInt32 SuperTiled2Unity.SuperObject::m_TileId
	uint32_t ___m_TileId_12;
	// SuperTiled2Unity.SuperTile SuperTiled2Unity.SuperObject::m_SuperTile
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14 * ___m_SuperTile_13;
	// System.Boolean SuperTiled2Unity.SuperObject::m_Visible
	bool ___m_Visible_14;
	// System.String SuperTiled2Unity.SuperObject::m_Template
	String_t* ___m_Template_15;

public:
	inline static int32_t get_offset_of_m_Id_4() { return static_cast<int32_t>(offsetof(SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21, ___m_Id_4)); }
	inline int32_t get_m_Id_4() const { return ___m_Id_4; }
	inline int32_t* get_address_of_m_Id_4() { return &___m_Id_4; }
	inline void set_m_Id_4(int32_t value)
	{
		___m_Id_4 = value;
	}

	inline static int32_t get_offset_of_m_TiledName_5() { return static_cast<int32_t>(offsetof(SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21, ___m_TiledName_5)); }
	inline String_t* get_m_TiledName_5() const { return ___m_TiledName_5; }
	inline String_t** get_address_of_m_TiledName_5() { return &___m_TiledName_5; }
	inline void set_m_TiledName_5(String_t* value)
	{
		___m_TiledName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TiledName_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_6() { return static_cast<int32_t>(offsetof(SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21, ___m_Type_6)); }
	inline String_t* get_m_Type_6() const { return ___m_Type_6; }
	inline String_t** get_address_of_m_Type_6() { return &___m_Type_6; }
	inline void set_m_Type_6(String_t* value)
	{
		___m_Type_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_X_7() { return static_cast<int32_t>(offsetof(SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21, ___m_X_7)); }
	inline float get_m_X_7() const { return ___m_X_7; }
	inline float* get_address_of_m_X_7() { return &___m_X_7; }
	inline void set_m_X_7(float value)
	{
		___m_X_7 = value;
	}

	inline static int32_t get_offset_of_m_Y_8() { return static_cast<int32_t>(offsetof(SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21, ___m_Y_8)); }
	inline float get_m_Y_8() const { return ___m_Y_8; }
	inline float* get_address_of_m_Y_8() { return &___m_Y_8; }
	inline void set_m_Y_8(float value)
	{
		___m_Y_8 = value;
	}

	inline static int32_t get_offset_of_m_Width_9() { return static_cast<int32_t>(offsetof(SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21, ___m_Width_9)); }
	inline float get_m_Width_9() const { return ___m_Width_9; }
	inline float* get_address_of_m_Width_9() { return &___m_Width_9; }
	inline void set_m_Width_9(float value)
	{
		___m_Width_9 = value;
	}

	inline static int32_t get_offset_of_m_Height_10() { return static_cast<int32_t>(offsetof(SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21, ___m_Height_10)); }
	inline float get_m_Height_10() const { return ___m_Height_10; }
	inline float* get_address_of_m_Height_10() { return &___m_Height_10; }
	inline void set_m_Height_10(float value)
	{
		___m_Height_10 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_11() { return static_cast<int32_t>(offsetof(SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21, ___m_Rotation_11)); }
	inline float get_m_Rotation_11() const { return ___m_Rotation_11; }
	inline float* get_address_of_m_Rotation_11() { return &___m_Rotation_11; }
	inline void set_m_Rotation_11(float value)
	{
		___m_Rotation_11 = value;
	}

	inline static int32_t get_offset_of_m_TileId_12() { return static_cast<int32_t>(offsetof(SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21, ___m_TileId_12)); }
	inline uint32_t get_m_TileId_12() const { return ___m_TileId_12; }
	inline uint32_t* get_address_of_m_TileId_12() { return &___m_TileId_12; }
	inline void set_m_TileId_12(uint32_t value)
	{
		___m_TileId_12 = value;
	}

	inline static int32_t get_offset_of_m_SuperTile_13() { return static_cast<int32_t>(offsetof(SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21, ___m_SuperTile_13)); }
	inline SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14 * get_m_SuperTile_13() const { return ___m_SuperTile_13; }
	inline SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14 ** get_address_of_m_SuperTile_13() { return &___m_SuperTile_13; }
	inline void set_m_SuperTile_13(SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14 * value)
	{
		___m_SuperTile_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SuperTile_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Visible_14() { return static_cast<int32_t>(offsetof(SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21, ___m_Visible_14)); }
	inline bool get_m_Visible_14() const { return ___m_Visible_14; }
	inline bool* get_address_of_m_Visible_14() { return &___m_Visible_14; }
	inline void set_m_Visible_14(bool value)
	{
		___m_Visible_14 = value;
	}

	inline static int32_t get_offset_of_m_Template_15() { return static_cast<int32_t>(offsetof(SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21, ___m_Template_15)); }
	inline String_t* get_m_Template_15() const { return ___m_Template_15; }
	inline String_t** get_address_of_m_Template_15() { return &___m_Template_15; }
	inline void set_m_Template_15(String_t* value)
	{
		___m_Template_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Template_15), (void*)value);
	}
};


// SuperTiled2Unity.TileObjectAnimator
struct TileObjectAnimator_tEE4E35AB14D4FBC13C615D69E1AC22514EE3B091  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single SuperTiled2Unity.TileObjectAnimator::m_AnimationFramerate
	float ___m_AnimationFramerate_4;
	// UnityEngine.Sprite[] SuperTiled2Unity.TileObjectAnimator::m_AnimationSprites
	SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* ___m_AnimationSprites_5;
	// System.Single SuperTiled2Unity.TileObjectAnimator::m_Timer
	float ___m_Timer_6;
	// System.Int32 SuperTiled2Unity.TileObjectAnimator::m_AnimationIndex
	int32_t ___m_AnimationIndex_7;

public:
	inline static int32_t get_offset_of_m_AnimationFramerate_4() { return static_cast<int32_t>(offsetof(TileObjectAnimator_tEE4E35AB14D4FBC13C615D69E1AC22514EE3B091, ___m_AnimationFramerate_4)); }
	inline float get_m_AnimationFramerate_4() const { return ___m_AnimationFramerate_4; }
	inline float* get_address_of_m_AnimationFramerate_4() { return &___m_AnimationFramerate_4; }
	inline void set_m_AnimationFramerate_4(float value)
	{
		___m_AnimationFramerate_4 = value;
	}

	inline static int32_t get_offset_of_m_AnimationSprites_5() { return static_cast<int32_t>(offsetof(TileObjectAnimator_tEE4E35AB14D4FBC13C615D69E1AC22514EE3B091, ___m_AnimationSprites_5)); }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* get_m_AnimationSprites_5() const { return ___m_AnimationSprites_5; }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77** get_address_of_m_AnimationSprites_5() { return &___m_AnimationSprites_5; }
	inline void set_m_AnimationSprites_5(SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* value)
	{
		___m_AnimationSprites_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationSprites_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Timer_6() { return static_cast<int32_t>(offsetof(TileObjectAnimator_tEE4E35AB14D4FBC13C615D69E1AC22514EE3B091, ___m_Timer_6)); }
	inline float get_m_Timer_6() const { return ___m_Timer_6; }
	inline float* get_address_of_m_Timer_6() { return &___m_Timer_6; }
	inline void set_m_Timer_6(float value)
	{
		___m_Timer_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationIndex_7() { return static_cast<int32_t>(offsetof(TileObjectAnimator_tEE4E35AB14D4FBC13C615D69E1AC22514EE3B091, ___m_AnimationIndex_7)); }
	inline int32_t get_m_AnimationIndex_7() const { return ___m_AnimationIndex_7; }
	inline int32_t* get_address_of_m_AnimationIndex_7() { return &___m_AnimationIndex_7; }
	inline void set_m_AnimationIndex_7(int32_t value)
	{
		___m_AnimationIndex_7 = value;
	}
};


// pot
struct pot_tBC07980977D1BF32444BED885CFF2F3BC7FB875D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Animator pot::anim
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___anim_4;

public:
	inline static int32_t get_offset_of_anim_4() { return static_cast<int32_t>(offsetof(pot_tBC07980977D1BF32444BED885CFF2F3BC7FB875D, ___anim_4)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_anim_4() const { return ___anim_4; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_anim_4() { return &___anim_4; }
	inline void set_anim_4(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___anim_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anim_4), (void*)value);
	}
};


// testdamagescript
struct testdamagescript_tF01C9B611CFE43D79A5CF8C6BD5CFED0FACF899D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// testenemyscript
struct testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 testenemyscript::health
	int32_t ___health_4;
	// System.Single testenemyscript::moveSpeed
	float ___moveSpeed_5;
	// System.Single testenemyscript::chaseRadius
	float ___chaseRadius_6;
	// System.Single testenemyscript::attackRadius
	float ___attackRadius_7;
	// UnityEngine.Transform testenemyscript::homePosition
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___homePosition_8;
	// UnityEngine.Transform testenemyscript::target
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target_9;
	// UnityEngine.AudioSource testenemyscript::soundEffect
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___soundEffect_10;
	// UnityEngine.Rigidbody2D testenemyscript::myRigidbody
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___myRigidbody_11;

public:
	inline static int32_t get_offset_of_health_4() { return static_cast<int32_t>(offsetof(testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3, ___health_4)); }
	inline int32_t get_health_4() const { return ___health_4; }
	inline int32_t* get_address_of_health_4() { return &___health_4; }
	inline void set_health_4(int32_t value)
	{
		___health_4 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_5() { return static_cast<int32_t>(offsetof(testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3, ___moveSpeed_5)); }
	inline float get_moveSpeed_5() const { return ___moveSpeed_5; }
	inline float* get_address_of_moveSpeed_5() { return &___moveSpeed_5; }
	inline void set_moveSpeed_5(float value)
	{
		___moveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_chaseRadius_6() { return static_cast<int32_t>(offsetof(testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3, ___chaseRadius_6)); }
	inline float get_chaseRadius_6() const { return ___chaseRadius_6; }
	inline float* get_address_of_chaseRadius_6() { return &___chaseRadius_6; }
	inline void set_chaseRadius_6(float value)
	{
		___chaseRadius_6 = value;
	}

	inline static int32_t get_offset_of_attackRadius_7() { return static_cast<int32_t>(offsetof(testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3, ___attackRadius_7)); }
	inline float get_attackRadius_7() const { return ___attackRadius_7; }
	inline float* get_address_of_attackRadius_7() { return &___attackRadius_7; }
	inline void set_attackRadius_7(float value)
	{
		___attackRadius_7 = value;
	}

	inline static int32_t get_offset_of_homePosition_8() { return static_cast<int32_t>(offsetof(testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3, ___homePosition_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_homePosition_8() const { return ___homePosition_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_homePosition_8() { return &___homePosition_8; }
	inline void set_homePosition_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___homePosition_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___homePosition_8), (void*)value);
	}

	inline static int32_t get_offset_of_target_9() { return static_cast<int32_t>(offsetof(testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3, ___target_9)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_target_9() const { return ___target_9; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_target_9() { return &___target_9; }
	inline void set_target_9(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___target_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_9), (void*)value);
	}

	inline static int32_t get_offset_of_soundEffect_10() { return static_cast<int32_t>(offsetof(testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3, ___soundEffect_10)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_soundEffect_10() const { return ___soundEffect_10; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_soundEffect_10() { return &___soundEffect_10; }
	inline void set_soundEffect_10(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___soundEffect_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___soundEffect_10), (void*)value);
	}

	inline static int32_t get_offset_of_myRigidbody_11() { return static_cast<int32_t>(offsetof(testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3, ___myRigidbody_11)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_myRigidbody_11() const { return ___myRigidbody_11; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_myRigidbody_11() { return &___myRigidbody_11; }
	inline void set_myRigidbody_11(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___myRigidbody_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myRigidbody_11), (void*)value);
	}
};


// Chest
struct Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C  : public Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7
{
public:
	// AnimatorController Chest::anim
	AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01 * ___anim_7;
	// BoolValue Chest::openValue
	BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370 * ___openValue_8;
	// System.Boolean Chest::isOpen
	bool ___isOpen_9;
	// Notification Chest::chestNotification
	Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * ___chestNotification_10;
	// SpriteValue Chest::spriteValue
	SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C * ___spriteValue_11;
	// StringValue Chest::itemString
	StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 * ___itemString_12;
	// InventoryItem Chest::myItem
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3 * ___myItem_13;
	// Inventory Chest::playerInventory
	Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805 * ___playerInventory_14;

public:
	inline static int32_t get_offset_of_anim_7() { return static_cast<int32_t>(offsetof(Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C, ___anim_7)); }
	inline AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01 * get_anim_7() const { return ___anim_7; }
	inline AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01 ** get_address_of_anim_7() { return &___anim_7; }
	inline void set_anim_7(AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01 * value)
	{
		___anim_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anim_7), (void*)value);
	}

	inline static int32_t get_offset_of_openValue_8() { return static_cast<int32_t>(offsetof(Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C, ___openValue_8)); }
	inline BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370 * get_openValue_8() const { return ___openValue_8; }
	inline BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370 ** get_address_of_openValue_8() { return &___openValue_8; }
	inline void set_openValue_8(BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370 * value)
	{
		___openValue_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___openValue_8), (void*)value);
	}

	inline static int32_t get_offset_of_isOpen_9() { return static_cast<int32_t>(offsetof(Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C, ___isOpen_9)); }
	inline bool get_isOpen_9() const { return ___isOpen_9; }
	inline bool* get_address_of_isOpen_9() { return &___isOpen_9; }
	inline void set_isOpen_9(bool value)
	{
		___isOpen_9 = value;
	}

	inline static int32_t get_offset_of_chestNotification_10() { return static_cast<int32_t>(offsetof(Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C, ___chestNotification_10)); }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * get_chestNotification_10() const { return ___chestNotification_10; }
	inline Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F ** get_address_of_chestNotification_10() { return &___chestNotification_10; }
	inline void set_chestNotification_10(Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F * value)
	{
		___chestNotification_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chestNotification_10), (void*)value);
	}

	inline static int32_t get_offset_of_spriteValue_11() { return static_cast<int32_t>(offsetof(Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C, ___spriteValue_11)); }
	inline SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C * get_spriteValue_11() const { return ___spriteValue_11; }
	inline SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C ** get_address_of_spriteValue_11() { return &___spriteValue_11; }
	inline void set_spriteValue_11(SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C * value)
	{
		___spriteValue_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteValue_11), (void*)value);
	}

	inline static int32_t get_offset_of_itemString_12() { return static_cast<int32_t>(offsetof(Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C, ___itemString_12)); }
	inline StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 * get_itemString_12() const { return ___itemString_12; }
	inline StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 ** get_address_of_itemString_12() { return &___itemString_12; }
	inline void set_itemString_12(StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760 * value)
	{
		___itemString_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemString_12), (void*)value);
	}

	inline static int32_t get_offset_of_myItem_13() { return static_cast<int32_t>(offsetof(Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C, ___myItem_13)); }
	inline InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3 * get_myItem_13() const { return ___myItem_13; }
	inline InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3 ** get_address_of_myItem_13() { return &___myItem_13; }
	inline void set_myItem_13(InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3 * value)
	{
		___myItem_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myItem_13), (void*)value);
	}

	inline static int32_t get_offset_of_playerInventory_14() { return static_cast<int32_t>(offsetof(Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C, ___playerInventory_14)); }
	inline Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805 * get_playerInventory_14() const { return ___playerInventory_14; }
	inline Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805 ** get_address_of_playerInventory_14() { return &___playerInventory_14; }
	inline void set_playerInventory_14(Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805 * value)
	{
		___playerInventory_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerInventory_14), (void*)value);
	}
};


// DamageOnContact
struct DamageOnContact_tBB1847BCB421560EEED6D01940B49C791B1323A4  : public Damage_tF03686559EE2A75E7FB5C86FDC5B966CB31BE631
{
public:
	// System.String DamageOnContact::otherString
	String_t* ___otherString_4;
	// System.Int32 DamageOnContact::damageAmount
	int32_t ___damageAmount_5;

public:
	inline static int32_t get_offset_of_otherString_4() { return static_cast<int32_t>(offsetof(DamageOnContact_tBB1847BCB421560EEED6D01940B49C791B1323A4, ___otherString_4)); }
	inline String_t* get_otherString_4() const { return ___otherString_4; }
	inline String_t** get_address_of_otherString_4() { return &___otherString_4; }
	inline void set_otherString_4(String_t* value)
	{
		___otherString_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___otherString_4), (void*)value);
	}

	inline static int32_t get_offset_of_damageAmount_5() { return static_cast<int32_t>(offsetof(DamageOnContact_tBB1847BCB421560EEED6D01940B49C791B1323A4, ___damageAmount_5)); }
	inline int32_t get_damageAmount_5() const { return ___damageAmount_5; }
	inline int32_t* get_address_of_damageAmount_5() { return &___damageAmount_5; }
	inline void set_damageAmount_5(int32_t value)
	{
		___damageAmount_5 = value;
	}
};


// DungeonRoom
struct DungeonRoom_tE85F47FFBB9EEA791B4EFCF5D2CE5835B471B8ED  : public Room_t44A97854220FADE8C026E18625929E32B64B3F6A
{
public:

public:
};


// EnemyHealth
struct EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0  : public Health_tB86D9293C9CF1E5B8E4C7271395F56DD4C67AE96
{
public:
	// UnityEngine.GameObject EnemyHealth::deathEffect
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___deathEffect_6;

public:
	inline static int32_t get_offset_of_deathEffect_6() { return static_cast<int32_t>(offsetof(EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0, ___deathEffect_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_deathEffect_6() const { return ___deathEffect_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_deathEffect_6() { return &___deathEffect_6; }
	inline void set_deathEffect_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___deathEffect_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deathEffect_6), (void*)value);
	}
};


// LockedDoor
struct LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B  : public Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7
{
public:
	// System.Boolean LockedDoor::isOpen
	bool ___isOpen_7;
	// BoolValue LockedDoor::isOpenValue
	BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370 * ___isOpenValue_8;
	// Inventory LockedDoor::playerInventory
	Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805 * ___playerInventory_9;
	// InventoryItem LockedDoor::keyitem
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3 * ___keyitem_10;
	// UnityEngine.SpriteRenderer LockedDoor::doorSprite
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___doorSprite_11;
	// UnityEngine.Collider2D LockedDoor::doorCollider
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___doorCollider_12;

public:
	inline static int32_t get_offset_of_isOpen_7() { return static_cast<int32_t>(offsetof(LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B, ___isOpen_7)); }
	inline bool get_isOpen_7() const { return ___isOpen_7; }
	inline bool* get_address_of_isOpen_7() { return &___isOpen_7; }
	inline void set_isOpen_7(bool value)
	{
		___isOpen_7 = value;
	}

	inline static int32_t get_offset_of_isOpenValue_8() { return static_cast<int32_t>(offsetof(LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B, ___isOpenValue_8)); }
	inline BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370 * get_isOpenValue_8() const { return ___isOpenValue_8; }
	inline BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370 ** get_address_of_isOpenValue_8() { return &___isOpenValue_8; }
	inline void set_isOpenValue_8(BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370 * value)
	{
		___isOpenValue_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___isOpenValue_8), (void*)value);
	}

	inline static int32_t get_offset_of_playerInventory_9() { return static_cast<int32_t>(offsetof(LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B, ___playerInventory_9)); }
	inline Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805 * get_playerInventory_9() const { return ___playerInventory_9; }
	inline Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805 ** get_address_of_playerInventory_9() { return &___playerInventory_9; }
	inline void set_playerInventory_9(Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805 * value)
	{
		___playerInventory_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerInventory_9), (void*)value);
	}

	inline static int32_t get_offset_of_keyitem_10() { return static_cast<int32_t>(offsetof(LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B, ___keyitem_10)); }
	inline InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3 * get_keyitem_10() const { return ___keyitem_10; }
	inline InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3 ** get_address_of_keyitem_10() { return &___keyitem_10; }
	inline void set_keyitem_10(InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3 * value)
	{
		___keyitem_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keyitem_10), (void*)value);
	}

	inline static int32_t get_offset_of_doorSprite_11() { return static_cast<int32_t>(offsetof(LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B, ___doorSprite_11)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_doorSprite_11() const { return ___doorSprite_11; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_doorSprite_11() { return &___doorSprite_11; }
	inline void set_doorSprite_11(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___doorSprite_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___doorSprite_11), (void*)value);
	}

	inline static int32_t get_offset_of_doorCollider_12() { return static_cast<int32_t>(offsetof(LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B, ___doorCollider_12)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_doorCollider_12() const { return ___doorCollider_12; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_doorCollider_12() { return &___doorCollider_12; }
	inline void set_doorCollider_12(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___doorCollider_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___doorCollider_12), (void*)value);
	}
};


// PlayerHealth
struct PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465  : public Health_tB86D9293C9CF1E5B8E4C7271395F56DD4C67AE96
{
public:
	// FlashColor PlayerHealth::flash
	FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866 * ___flash_6;
	// System.Single PlayerHealth::health
	float ___health_7;
	// System.Int32 PlayerHealth::damageTaken
	int32_t ___damageTaken_8;
	// TMPro.TextMeshProUGUI PlayerHealth::healthDisplay
	TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * ___healthDisplay_9;

public:
	inline static int32_t get_offset_of_flash_6() { return static_cast<int32_t>(offsetof(PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465, ___flash_6)); }
	inline FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866 * get_flash_6() const { return ___flash_6; }
	inline FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866 ** get_address_of_flash_6() { return &___flash_6; }
	inline void set_flash_6(FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866 * value)
	{
		___flash_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___flash_6), (void*)value);
	}

	inline static int32_t get_offset_of_health_7() { return static_cast<int32_t>(offsetof(PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465, ___health_7)); }
	inline float get_health_7() const { return ___health_7; }
	inline float* get_address_of_health_7() { return &___health_7; }
	inline void set_health_7(float value)
	{
		___health_7 = value;
	}

	inline static int32_t get_offset_of_damageTaken_8() { return static_cast<int32_t>(offsetof(PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465, ___damageTaken_8)); }
	inline int32_t get_damageTaken_8() const { return ___damageTaken_8; }
	inline int32_t* get_address_of_damageTaken_8() { return &___damageTaken_8; }
	inline void set_damageTaken_8(int32_t value)
	{
		___damageTaken_8 = value;
	}

	inline static int32_t get_offset_of_healthDisplay_9() { return static_cast<int32_t>(offsetof(PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465, ___healthDisplay_9)); }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * get_healthDisplay_9() const { return ___healthDisplay_9; }
	inline TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 ** get_address_of_healthDisplay_9() { return &___healthDisplay_9; }
	inline void set_healthDisplay_9(TextMeshProUGUI_tCC5BE8A76E6E9AF92521A462E8D81ACFBA7C85F1 * value)
	{
		___healthDisplay_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healthDisplay_9), (void*)value);
	}
};


// PlayerMagic
struct PlayerMagic_t8FB37962E8167A179216B0F7A6339A0E553C2A38  : public Magic_t0CC50A162DDF9C4C4549E05883368ACD19D8F36C
{
public:

public:
};


// SimpleFollow
struct SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4  : public Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C
{
public:
	// System.String SimpleFollow::targetTag
	String_t* ___targetTag_6;
	// UnityEngine.Transform SimpleFollow::target
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target_7;
	// System.Single SimpleFollow::chaseRadius
	float ___chaseRadius_8;
	// System.Single SimpleFollow::attackRadius
	float ___attackRadius_9;
	// System.Single SimpleFollow::targetDistance
	float ___targetDistance_10;

public:
	inline static int32_t get_offset_of_targetTag_6() { return static_cast<int32_t>(offsetof(SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4, ___targetTag_6)); }
	inline String_t* get_targetTag_6() const { return ___targetTag_6; }
	inline String_t** get_address_of_targetTag_6() { return &___targetTag_6; }
	inline void set_targetTag_6(String_t* value)
	{
		___targetTag_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetTag_6), (void*)value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4, ___target_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_target_7() const { return ___target_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_7), (void*)value);
	}

	inline static int32_t get_offset_of_chaseRadius_8() { return static_cast<int32_t>(offsetof(SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4, ___chaseRadius_8)); }
	inline float get_chaseRadius_8() const { return ___chaseRadius_8; }
	inline float* get_address_of_chaseRadius_8() { return &___chaseRadius_8; }
	inline void set_chaseRadius_8(float value)
	{
		___chaseRadius_8 = value;
	}

	inline static int32_t get_offset_of_attackRadius_9() { return static_cast<int32_t>(offsetof(SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4, ___attackRadius_9)); }
	inline float get_attackRadius_9() const { return ___attackRadius_9; }
	inline float* get_address_of_attackRadius_9() { return &___attackRadius_9; }
	inline void set_attackRadius_9(float value)
	{
		___attackRadius_9 = value;
	}

	inline static int32_t get_offset_of_targetDistance_10() { return static_cast<int32_t>(offsetof(SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4, ___targetDistance_10)); }
	inline float get_targetDistance_10() const { return ___targetDistance_10; }
	inline float* get_address_of_targetDistance_10() { return &___targetDistance_10; }
	inline void set_targetDistance_10(float value)
	{
		___targetDistance_10 = value;
	}
};


// Slime
struct Slime_t466E85D7985922ECA15F89DCB7D9FD741F4B5485  : public Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627
{
public:
	// UnityEngine.Transform Slime::target
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___target_10;
	// System.Single Slime::chaseRadius
	float ___chaseRadius_11;
	// System.Single Slime::attackRadius
	float ___attackRadius_12;
	// UnityEngine.Transform Slime::homePosition
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___homePosition_13;
	// UnityEngine.Rigidbody2D Slime::myRigidbody
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___myRigidbody_14;

public:
	inline static int32_t get_offset_of_target_10() { return static_cast<int32_t>(offsetof(Slime_t466E85D7985922ECA15F89DCB7D9FD741F4B5485, ___target_10)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_target_10() const { return ___target_10; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_target_10() { return &___target_10; }
	inline void set_target_10(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___target_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_10), (void*)value);
	}

	inline static int32_t get_offset_of_chaseRadius_11() { return static_cast<int32_t>(offsetof(Slime_t466E85D7985922ECA15F89DCB7D9FD741F4B5485, ___chaseRadius_11)); }
	inline float get_chaseRadius_11() const { return ___chaseRadius_11; }
	inline float* get_address_of_chaseRadius_11() { return &___chaseRadius_11; }
	inline void set_chaseRadius_11(float value)
	{
		___chaseRadius_11 = value;
	}

	inline static int32_t get_offset_of_attackRadius_12() { return static_cast<int32_t>(offsetof(Slime_t466E85D7985922ECA15F89DCB7D9FD741F4B5485, ___attackRadius_12)); }
	inline float get_attackRadius_12() const { return ___attackRadius_12; }
	inline float* get_address_of_attackRadius_12() { return &___attackRadius_12; }
	inline void set_attackRadius_12(float value)
	{
		___attackRadius_12 = value;
	}

	inline static int32_t get_offset_of_homePosition_13() { return static_cast<int32_t>(offsetof(Slime_t466E85D7985922ECA15F89DCB7D9FD741F4B5485, ___homePosition_13)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_homePosition_13() const { return ___homePosition_13; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_homePosition_13() { return &___homePosition_13; }
	inline void set_homePosition_13(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___homePosition_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___homePosition_13), (void*)value);
	}

	inline static int32_t get_offset_of_myRigidbody_14() { return static_cast<int32_t>(offsetof(Slime_t466E85D7985922ECA15F89DCB7D9FD741F4B5485, ___myRigidbody_14)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_myRigidbody_14() const { return ___myRigidbody_14; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_myRigidbody_14() { return &___myRigidbody_14; }
	inline void set_myRigidbody_14(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___myRigidbody_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___myRigidbody_14), (void*)value);
	}
};


// SuperTiled2Unity.SuperGroupLayer
struct SuperGroupLayer_t22B713337D4771BFF4B9D4E850D465D8C33B18B6  : public SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C
{
public:

public:
};


// SuperTiled2Unity.SuperImageLayer
struct SuperImageLayer_t3CD24E164ADB8EE9C72A0C15676F397EBA60EDA5  : public SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C
{
public:
	// System.String SuperTiled2Unity.SuperImageLayer::m_ImageFilename
	String_t* ___m_ImageFilename_9;

public:
	inline static int32_t get_offset_of_m_ImageFilename_9() { return static_cast<int32_t>(offsetof(SuperImageLayer_t3CD24E164ADB8EE9C72A0C15676F397EBA60EDA5, ___m_ImageFilename_9)); }
	inline String_t* get_m_ImageFilename_9() const { return ___m_ImageFilename_9; }
	inline String_t** get_address_of_m_ImageFilename_9() { return &___m_ImageFilename_9; }
	inline void set_m_ImageFilename_9(String_t* value)
	{
		___m_ImageFilename_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ImageFilename_9), (void*)value);
	}
};


// SuperTiled2Unity.SuperObjectLayer
struct SuperObjectLayer_tC27DDCDAFE05ACF9AF26AC4508F65C38A46F4EBE  : public SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C
{
public:
	// UnityEngine.Color SuperTiled2Unity.SuperObjectLayer::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_9;

public:
	inline static int32_t get_offset_of_m_Color_9() { return static_cast<int32_t>(offsetof(SuperObjectLayer_tC27DDCDAFE05ACF9AF26AC4508F65C38A46F4EBE, ___m_Color_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_9() const { return ___m_Color_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_9() { return &___m_Color_9; }
	inline void set_m_Color_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_9 = value;
	}
};


// SuperTiled2Unity.SuperTileLayer
struct SuperTileLayer_tF6FD3F4831EA2605A23B810A72A9D260E6C0B74C  : public SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3181[1] = 
{
	EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0::get_offset_of_deathEffect_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3182[5] = 
{
	SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4::get_offset_of_targetTag_6(),
	SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4::get_offset_of_target_7(),
	SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4::get_offset_of_chaseRadius_8(),
	SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4::get_offset_of_attackRadius_9(),
	SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4::get_offset_of_targetDistance_10(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3183[5] = 
{
	Slime_t466E85D7985922ECA15F89DCB7D9FD741F4B5485::get_offset_of_target_10(),
	Slime_t466E85D7985922ECA15F89DCB7D9FD741F4B5485::get_offset_of_chaseRadius_11(),
	Slime_t466E85D7985922ECA15F89DCB7D9FD741F4B5485::get_offset_of_attackRadius_12(),
	Slime_t466E85D7985922ECA15F89DCB7D9FD741F4B5485::get_offset_of_homePosition_13(),
	Slime_t466E85D7985922ECA15F89DCB7D9FD741F4B5485::get_offset_of_myRigidbody_14(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3184[4] = 
{
	RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC::get_offset_of_roomToLoad_4(),
	RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC::get_offset_of_playerTag_5(),
	RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC::get_offset_of_playerPosInRoomValue_6(),
	RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC::get_offset_of_playerPosInRoom_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3185[2] = 
{
	SignalListener_tA58702930EF45A76FD394D8D9455E41D2E0360C8::get_offset_of_signal_4(),
	SignalListener_tA58702930EF45A76FD394D8D9455E41D2E0360C8::get_offset_of_signalEvent_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3186[2] = 
{
	SOObserver_tD018D77957D756C9D4E80B373BCE27677AA0AA21::get_offset_of_instance_4(),
	SOObserver_tD018D77957D756C9D4E80B373BCE27677AA0AA21::get_offset_of_objectsInMemory_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3187[1] = 
{
	AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01::get_offset_of_anim_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3189[2] = 
{
	DamageOnContact_tBB1847BCB421560EEED6D01940B49C791B1323A4::get_offset_of_otherString_4(),
	DamageOnContact_tBB1847BCB421560EEED6D01940B49C791B1323A4::get_offset_of_damageAmount_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3190[1] = 
{
	DestroyOverTime_t72388AFBB9556818D23C5BD91F53CC8EA9FDC03F::get_offset_of_destroyDelay_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3192[4] = 
{
	U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46::get_offset_of_U3CU3E1__state_0(),
	U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46::get_offset_of_U3CU3E2__current_1(),
	U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46::get_offset_of_U3CU3E4__this_2(),
	U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46::get_offset_of_U3CiU3E5__2_3(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3193[5] = 
{
	FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866::get_offset_of_mySprite_4(),
	FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866::get_offset_of_flashColor_5(),
	FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866::get_offset_of_numberOfFlashes_6(),
	FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866::get_offset_of_flashDelay_7(),
	FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866::get_offset_of_isFlashing_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3194[2] = 
{
	Health_tB86D9293C9CF1E5B8E4C7271395F56DD4C67AE96::get_offset_of_maxHealth_4(),
	Health_tB86D9293C9CF1E5B8E4C7271395F56DD4C67AE96::get_offset_of_currentHealth_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3195[3] = 
{
	Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7::get_offset_of_playerInRange_4(),
	Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7::get_offset_of_otherTag_5(),
	Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7::get_offset_of_myNotification_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3196[5] = 
{
	Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4::get_offset_of_otherTag_4(),
	Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4::get_offset_of_knockTime_5(),
	Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4::get_offset_of_knockStrength_6(),
	Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4::get_offset_of_thrust_7(),
	Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4::get_offset_of_damage_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3197[2] = 
{
	Magic_t0CC50A162DDF9C4C4549E05883368ACD19D8F36C::get_offset_of_currentMagic_4(),
	Magic_t0CC50A162DDF9C4C4549E05883368ACD19D8F36C::get_offset_of_maxMagic_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3198[2] = 
{
	Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C::get_offset_of_speed_4(),
	Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C::get_offset_of_myRigidbody_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3199[1] = 
{
	ResetToPosition_t9E69B3C34D3A9B21F794674DA94578EB3FC0183B::get_offset_of_resetPosition_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3200[8] = 
{
	GenericState_t6EE1FC6A2106ACCE59A16AF258C56AE11C51BA6E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3201[1] = 
{
	StateMachine_t823E5CFDC9E27E10A2C57DB9303909CA2D8BC49A::get_offset_of_myState_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3202[2] = 
{
	Arrow_t98BA45BD73ADFAD1B38830B6D6D48588A96AC86A::get_offset_of_speed_4(),
	Arrow_t98BA45BD73ADFAD1B38830B6D6D48588A96AC86A::get_offset_of_myRigidbody_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3203[8] = 
{
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C::get_offset_of_anim_7(),
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C::get_offset_of_openValue_8(),
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C::get_offset_of_isOpen_9(),
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C::get_offset_of_chestNotification_10(),
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C::get_offset_of_spriteValue_11(),
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C::get_offset_of_itemString_12(),
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C::get_offset_of_myItem_13(),
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C::get_offset_of_playerInventory_14(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3205[6] = 
{
	LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B::get_offset_of_isOpen_7(),
	LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B::get_offset_of_isOpenValue_8(),
	LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B::get_offset_of_playerInventory_9(),
	LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B::get_offset_of_keyitem_10(),
	LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B::get_offset_of_doorSprite_11(),
	LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B::get_offset_of_doorCollider_12(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3206[3] = 
{
	U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42::get_offset_of_U3CU3E1__state_0(),
	U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42::get_offset_of_U3CU3E2__current_1(),
	U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3207[1] = 
{
	pot_tBC07980977D1BF32444BED885CFF2F3BC7FB875D::get_offset_of_anim_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3208[6] = 
{
	Room_t44A97854220FADE8C026E18625929E32B64B3F6A::get_offset_of_roomName_4(),
	Room_t44A97854220FADE8C026E18625929E32B64B3F6A::get_offset_of_roomNameHolder_5(),
	Room_t44A97854220FADE8C026E18625929E32B64B3F6A::get_offset_of_roomnotification_6(),
	Room_t44A97854220FADE8C026E18625929E32B64B3F6A::get_offset_of_playerTag_7(),
	Room_t44A97854220FADE8C026E18625929E32B64B3F6A::get_offset_of_respawnObjects_8(),
	Room_t44A97854220FADE8C026E18625929E32B64B3F6A::get_offset_of_thisCamera_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3209[1] = 
{
	SecretTree_t2C2115D59ECF1500F65CF282870D390C7782E7D8::get_offset_of_health_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3210[2] = 
{
	ContextClue_tE96BB9C19D11CBC0A0137551DAFF0EE6796339B1::get_offset_of_mySprite_4(),
	ContextClue_tE96BB9C19D11CBC0A0137551DAFF0EE6796339B1::get_offset_of_clueActive_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3211[6] = 
{
	HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25::get_offset_of_hearts_4(),
	HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25::get_offset_of_fullHeart_5(),
	HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25::get_offset_of_halfFullHeart_6(),
	HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25::get_offset_of_emptyHeart_7(),
	HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25::get_offset_of_heartContainers_8(),
	HeartManager_t6EA5F239952535EF6D66B97412D3B00FC94DAF25::get_offset_of_playerCurrentHealth_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3212[4] = 
{
	PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465::get_offset_of_flash_6(),
	PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465::get_offset_of_health_7(),
	PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465::get_offset_of_damageTaken_8(),
	PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465::get_offset_of_healthDisplay_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3215[2] = 
{
	PlayerMoney_tCEB0119C0B290EA5E47264E49FB4D95AD5CD3E9A::get_offset_of_currentMoney_4(),
	PlayerMoney_tCEB0119C0B290EA5E47264E49FB4D95AD5CD3E9A::get_offset_of_maxMoney_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3216[6] = 
{
	ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7::get_offset_of_mySprite_4(),
	ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7::get_offset_of_receivedSprite_5(),
	ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7::get_offset_of_anim_6(),
	ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7::get_offset_of_myState_7(),
	ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7::get_offset_of_isActive_8(),
	ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7::get_offset_of_dialogNotification_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3217[1] = 
{
	ResetPlayerPosition_tA80F12DC82B6A24D6CDD43EAE82E742CAA76632D::get_offset_of_playerPosition_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3218[1] = 
{
	DashAbility_t9D1464CE8BDF1A9D6CE8253D6E2AFC8ECA794DC9::get_offset_of_dashForce_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3219[4] = 
{
	GenericAbility_t6F5062DC8FA17FF2828A882D17D7D48864095C21::get_offset_of_magicCost_4(),
	GenericAbility_t6F5062DC8FA17FF2828A882D17D7D48864095C21::get_offset_of_duration_5(),
	GenericAbility_t6F5062DC8FA17FF2828A882D17D7D48864095C21::get_offset_of_playerMagic_6(),
	GenericAbility_t6F5062DC8FA17FF2828A882D17D7D48864095C21::get_offset_of_usePlayerMagic_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3220[2] = 
{
	BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370::get_offset_of_value_4(),
	BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370::get_offset_of_resetValue_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3221[3] = 
{
	FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10::get_offset_of_value_4(),
	FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10::get_offset_of_defaultValue_5(),
	FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10::get_offset_of_RuntimeValue_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3222[1] = 
{
	Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805::get_offset_of_myInventory_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3223[6] = 
{
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3::get_offset_of_mySprite_4(),
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3::get_offset_of_myName_5(),
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3::get_offset_of_myDescription_6(),
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3::get_offset_of_isUsable_7(),
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3::get_offset_of_isUnique_8(),
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3::get_offset_of_numberHeld_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3224[1] = 
{
	Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F::get_offset_of_listeners_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3225[2] = 
{
	NotificationListener_t576AF20BBC693CDD899148976A9D0D3778058225::get_offset_of_myEvent_4(),
	NotificationListener_t576AF20BBC693CDD899148976A9D0D3778058225::get_offset_of_myNotification_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3226[1] = 
{
	SignalSender_t0075E93BFF2201C4C328193746B0F29E902AACCA::get_offset_of_listeners_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3227[1] = 
{
	SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C::get_offset_of_value_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3228[1] = 
{
	StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760::get_offset_of_value_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3229[2] = 
{
	VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD::get_offset_of_value_4(),
	VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD::get_offset_of_defaultValue_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3230[2] = 
{
	Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3::get_offset_of_name_0(),
	Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3::get_offset_of_sentences_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3231[6] = 
{
	U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104::get_offset_of_U3CU3E1__state_0(),
	U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104::get_offset_of_U3CU3E2__current_1(),
	U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104::get_offset_of_U3CU3E4__this_2(),
	U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104::get_offset_of_sentence_3(),
	U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104::get_offset_of_U3CU3E7__wrap1_4(),
	U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104::get_offset_of_U3CU3E7__wrap2_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3232[4] = 
{
	DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D::get_offset_of_nameText_4(),
	DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D::get_offset_of_dialogueText_5(),
	DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D::get_offset_of_animator_6(),
	DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D::get_offset_of_sentences_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3233[2] = 
{
	DialogueTrigger_tAA3B7D126EEF458C23E794A4C0648320248BDA39::get_offset_of_playerinRange_4(),
	DialogueTrigger_tAA3B7D126EEF458C23E794A4C0648320248BDA39::get_offset_of_dialogue_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3234[8] = 
{
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6::get_offset_of_playerinRange_4(),
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6::get_offset_of_health_5(),
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6::get_offset_of_moveSpeed_6(),
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6::get_offset_of_chaseRadius_7(),
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6::get_offset_of_idleRadius_8(),
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6::get_offset_of_homePosition_9(),
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6::get_offset_of_target_10(),
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6::get_offset_of_myRigidbody_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3236[8] = 
{
	testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3::get_offset_of_health_4(),
	testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3::get_offset_of_moveSpeed_5(),
	testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3::get_offset_of_chaseRadius_6(),
	testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3::get_offset_of_attackRadius_7(),
	testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3::get_offset_of_homePosition_8(),
	testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3::get_offset_of_target_9(),
	testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3::get_offset_of_soundEffect_10(),
	testenemyscript_t9875474DF7C937ABF6AE3A63A74E7F0A20CCCBF3::get_offset_of_myRigidbody_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3237[3] = 
{
	U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268::get_offset_of_U3CU3E1__state_0(),
	U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268::get_offset_of_U3CU3E2__current_1(),
	U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268::get_offset_of_U3CU3E4__this_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3238[3] = 
{
	AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61::get_offset_of_nameText_4(),
	AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61::get_offset_of_roomNameValue_5(),
	AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61::get_offset_of_duration_6(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3239[5] = 
{
	DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4::get_offset_of_stringText_4(),
	DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4::get_offset_of_dialogNotification_5(),
	DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4::get_offset_of_dialogText_6(),
	DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4::get_offset_of_dialogObject_7(),
	DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4::get_offset_of_dialogActive_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3240[5] = 
{
	GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64::get_offset_of_creditsPanel_4(),
	GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64::get_offset_of_gamePanel_5(),
	GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64::get_offset_of_levelToLoad_6(),
	GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64::get_offset_of_playerPosition_7(),
	GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64::get_offset_of_playerPos_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3241[5] = 
{
	PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE::get_offset_of_isPaused_4(),
	PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE::get_offset_of_pauseMenu_5(),
	PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE::get_offset_of_inventoryMenu_6(),
	PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE::get_offset_of_gameSpeed_7(),
	PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE::get_offset_of_menuSceneString_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3242[2] = 
{
	U3CU3Ec__DisplayClass22_0_tB40012BE63C6AF48A235BC43CC8750930461F581::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass22_0_tB40012BE63C6AF48A235BC43CC8750930461F581::get_offset_of_tile_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3243[1] = 
{
	U3CU3Ec__DisplayClass25_0_tE31A76DF44246018C15E08B2B2A89A39A82EF09C::get_offset_of_rotate_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3244[2] = 
{
	U3CU3Ec_t0C7724567A46A1BEEDB72095B8FD8DB98CF36CCB_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0C7724567A46A1BEEDB72095B8FD8DB98CF36CCB_StaticFields::get_offset_of_U3CU3E9__25_1_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3245[12] = 
{
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D::get_offset_of_m_ObjectId_0(),
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D::get_offset_of_m_ObjectName_1(),
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D::get_offset_of_m_ObjectType_2(),
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D::get_offset_of_m_Position_3(),
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D::get_offset_of_m_Size_4(),
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D::get_offset_of_m_Rotation_5(),
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D::get_offset_of_m_CustomProperties_6(),
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D::get_offset_of_m_PhysicsLayer_7(),
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D::get_offset_of_m_IsTrigger_8(),
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D::get_offset_of_m_Points_9(),
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D::get_offset_of_m_IsClosed_10(),
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D::get_offset_of_m_CollisionShapeType_11(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3246[5] = 
{
	CollisionShapeType_t34C44C75A983937AB6627ECC5D8320190959B81F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3247[3] = 
{
	CustomProperty_t40EF9CDA2A043DA7BCEA9D002CC1828000C6C847::get_offset_of_m_Name_0(),
	CustomProperty_t40EF9CDA2A043DA7BCEA9D002CC1828000C6C847::get_offset_of_m_Type_1(),
	CustomProperty_t40EF9CDA2A043DA7BCEA9D002CC1828000C6C847::get_offset_of_m_Value_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3248[1] = 
{
	U3CU3Ec__DisplayClass0_0_t4F7D9550667533CDDCC7B1CD0856AC36267DCADA::get_offset_of_propertyName_0(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3255[12] = 
{
	FlipFlags_tDDEF8286047324FD6813C734F022AD7E4F092CF3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3256[3] = 
{
	GridOrientation_tF9D49F7BE3EDD94E3DD1A3484B562BEF1242161F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3257[5] = 
{
	MapOrientation_t7480808E4755AB116CCCD60DE53E0D64874DF328::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3258[5] = 
{
	MapRenderOrder_tF57E92C1303B5D80078A07DA3D34CE54BDE3B2C4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3262[3] = 
{
	StaggerAxis_t10EC3EF8051D955A4D3189BA79E3D2EA479DFA7D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3263[3] = 
{
	StaggerIndex_tD65C5729BADB7D3368D075B554927BB766A744DD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3265[1] = 
{
	SuperCustomProperties_t64CCDF0B5920621763A3293FC8F0FBFD877F341F::get_offset_of_m_Properties_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3267[1] = 
{
	SuperImageLayer_t3CD24E164ADB8EE9C72A0C15676F397EBA60EDA5::get_offset_of_m_ImageFilename_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3268[5] = 
{
	SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C::get_offset_of_m_TiledName_4(),
	SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C::get_offset_of_m_OffsetX_5(),
	SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C::get_offset_of_m_OffsetY_6(),
	SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C::get_offset_of_m_Opacity_7(),
	SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C::get_offset_of_m_Visible_8(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3269[14] = 
{
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_Version_4(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_TiledVersion_5(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_Orientation_6(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_RenderOrder_7(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_Width_8(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_Height_9(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_TileWidth_10(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_TileHeight_11(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_HexSideLength_12(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_StaggerAxis_13(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_StaggerIndex_14(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_Infinite_15(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_BackgroundColor_16(),
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96::get_offset_of_m_NextObjectId_17(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3270[12] = 
{
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21::get_offset_of_m_Id_4(),
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21::get_offset_of_m_TiledName_5(),
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21::get_offset_of_m_Type_6(),
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21::get_offset_of_m_X_7(),
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21::get_offset_of_m_Y_8(),
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21::get_offset_of_m_Width_9(),
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21::get_offset_of_m_Height_10(),
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21::get_offset_of_m_Rotation_11(),
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21::get_offset_of_m_TileId_12(),
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21::get_offset_of_m_SuperTile_13(),
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21::get_offset_of_m_Visible_14(),
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21::get_offset_of_m_Template_15(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3271[1] = 
{
	SuperObjectLayer_tC27DDCDAFE05ACF9AF26AC4508F65C38A46F4EBE::get_offset_of_m_Color_9(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3272[13] = 
{
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_StaticFields::get_offset_of_HorizontalFlipMatrix_4(),
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_StaticFields::get_offset_of_VerticalFlipMatrix_5(),
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_StaticFields::get_offset_of_DiagonalFlipMatrix_6(),
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14::get_offset_of_m_TileId_7(),
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14::get_offset_of_m_Sprite_8(),
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14::get_offset_of_m_AnimationSprites_9(),
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14::get_offset_of_m_Type_10(),
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14::get_offset_of_m_Width_11(),
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14::get_offset_of_m_Height_12(),
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14::get_offset_of_m_TileOffsetX_13(),
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14::get_offset_of_m_TileOffsetY_14(),
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14::get_offset_of_m_CustomProperties_15(),
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14::get_offset_of_m_CollisionObjects_16(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3274[4] = 
{
	TileObjectAnimator_tEE4E35AB14D4FBC13C615D69E1AC22514EE3B091::get_offset_of_m_AnimationFramerate_4(),
	TileObjectAnimator_tEE4E35AB14D4FBC13C615D69E1AC22514EE3B091::get_offset_of_m_AnimationSprites_5(),
	TileObjectAnimator_tEE4E35AB14D4FBC13C615D69E1AC22514EE3B091::get_offset_of_m_Timer_6(),
	TileObjectAnimator_tEE4E35AB14D4FBC13C615D69E1AC22514EE3B091::get_offset_of_m_AnimationIndex_7(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3275[4] = 
{
	State_tA675A320CDE12DA6B4F48FB0632C8C2D639D6A30::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3276[2] = 
{
	U3CU3Ec_t069EA630E146EBD7BB9CE47F83C828A52666D434_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t069EA630E146EBD7BB9CE47F83C828A52666D434_StaticFields::get_offset_of_U3CU3E9__14_0_1(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3277[5] = 
{
	U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C::get_offset_of_U3CU3E1__state_0(),
	U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C::get_offset_of_U3CU3E2__current_1(),
	U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C::get_offset_of_U3CU3E4__this_2(),
	U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C::get_offset_of_U3CposU3E5__2_3(),
	U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C::get_offset_of_U3CiU3E5__3_4(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3278[12] = 
{
	OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571::get_offset_of_m_State_4(),
	0,
	0,
	0,
	OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571::get_offset_of_m_SplashPrefab_8(),
	OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571::get_offset_of_m_Facing_9(),
	OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571::get_offset_of_m_Animator_10(),
	OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571::get_offset_of_m_MoveTimer_11(),
	OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571::get_offset_of_m_MovingFrom_12(),
	OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571::get_offset_of_m_MovingTo_13(),
	OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571::get_offset_of_m_SpawnPoint_14(),
	OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571::get_offset_of_m_Renderer_15(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
