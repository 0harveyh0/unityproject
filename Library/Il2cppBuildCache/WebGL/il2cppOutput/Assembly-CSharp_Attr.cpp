﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// SuperTiled2Unity.ReadOnlyAttribute
struct ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// SuperTiled2Unity.ReadOnlyAttribute
struct ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042 (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * __this, int32_t ___minLines0, int32_t ___maxLines1, const RuntimeMethod* method);
// System.Void SuperTiled2Unity.ReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_tA3BCDD4327DA28EB58F404C4F16EF439D2996561_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_anim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_myState(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_WeaponAttackDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_myItem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_currentAbility(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_PlayerMovement_WeaponCo_m7E38E79787A651A1F512F9E0CF42E98A2398D396(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_0_0_0_var), NULL);
	}
}
static void PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_PlayerMovement_AbilityCo_mB527E62AFDABEA91E50C6841F440F178C87D971D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_0_0_0_var), NULL);
	}
}
static void PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_PlayerMovement_Knockco_m9FB0A3F5655B26872CF8F08DE922AAC0BA2CD9AB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_0_0_0_var), NULL);
	}
}
static void PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_PlayerMovement_SecondAttackCo_m35F4438A058472BE31F0537E3AA967A0F8E26349(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_0_0_0_var), NULL);
	}
}
static void U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_CustomAttributesCacheGenerator_U3CWeaponCoU3Ed__19__ctor_mF6774EDF96E29D4CF1DE6E59A385EF4733577499(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_CustomAttributesCacheGenerator_U3CWeaponCoU3Ed__19_System_IDisposable_Dispose_m336B4209188172D5D2E7DC91BA51B13E1B53AD56(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_CustomAttributesCacheGenerator_U3CWeaponCoU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE75159A118D1CCD0D21C44DA7A1BCCEFEED035B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_CustomAttributesCacheGenerator_U3CWeaponCoU3Ed__19_System_Collections_IEnumerator_Reset_m92D841A85EA7636FE60CE098FB0E509F112F57D3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_CustomAttributesCacheGenerator_U3CWeaponCoU3Ed__19_System_Collections_IEnumerator_get_Current_mFA45A158917C1D00D0414546D330B5C50AF98810(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_CustomAttributesCacheGenerator_U3CAbilityCoU3Ed__20__ctor_m0176682625B409EBAF4204EB4659BDF4D41F4BA5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_CustomAttributesCacheGenerator_U3CAbilityCoU3Ed__20_System_IDisposable_Dispose_m7077FA91B9383678A31E38756736A526CF59A3FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_CustomAttributesCacheGenerator_U3CAbilityCoU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1B45335C8F68A00353045AFA6267AE71F07182E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_CustomAttributesCacheGenerator_U3CAbilityCoU3Ed__20_System_Collections_IEnumerator_Reset_m8E3B2891A317FB08FA139BD950964EAD077B40C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_CustomAttributesCacheGenerator_U3CAbilityCoU3Ed__20_System_Collections_IEnumerator_get_Current_mE6CEEBAB1AB8EE0C1A7052E7E832277A232B1DD5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__21__ctor_m0683E9332889422FD23C62E94DCA9758C00552BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__21_System_IDisposable_Dispose_m3209EEDB656EB5486A1CF3DA9F24C350C4707499(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF02042846E4388A7D23AEE7B092211A67F45B9B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__21_System_Collections_IEnumerator_Reset_mFB645C76CFD1ECF92A92F52F985A9D3459F302A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__21_System_Collections_IEnumerator_get_Current_m54FA11747AF9038FFEE415DA8CB553F911104081(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_CustomAttributesCacheGenerator_U3CSecondAttackCoU3Ed__22__ctor_mF3BB48058B987D2323D3877A4B73BB38EF8FCA12(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_CustomAttributesCacheGenerator_U3CSecondAttackCoU3Ed__22_System_IDisposable_Dispose_mD085A289E795223BE81314ED9425C51047B43990(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_CustomAttributesCacheGenerator_U3CSecondAttackCoU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m566C8E4E0AABFF749D23CB669FA3AE323A002871(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_CustomAttributesCacheGenerator_U3CSecondAttackCoU3Ed__22_System_Collections_IEnumerator_Reset_m6FAADB162A5C47DAB3951676C0EE2937468C2E00(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_CustomAttributesCacheGenerator_U3CSecondAttackCoU3Ed__22_System_Collections_IEnumerator_get_Current_m23AC4D1FAA25D82B0CE7ACFC49A43B7DC624AD7D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Sound_tF983595F9C621A86B56E05F9778810369E90A0FE_CustomAttributesCacheGenerator_volume(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void Sound_tF983595F9C621A86B56E05F9778810369E90A0FE_CustomAttributesCacheGenerator_pitch(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 3.0f, NULL);
	}
}
static void Sound_tF983595F9C621A86B56E05F9778810369E90A0FE_CustomAttributesCacheGenerator_source(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_Knockco_mD90ED67E0DA4A148DE3CB5FA33EB2187AAFB2F7A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_0_0_0_var), NULL);
	}
}
static void U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__9__ctor_m2F874F51BAFBD6E0A502C95F49D0FDE4B00B3596(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__9_System_IDisposable_Dispose_mA9E234FEC09DB70E40A923A8EE315EE24373BBC3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC2DEAF9142B8BC177D269B901C8ACE2F35A5B64(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__9_System_Collections_IEnumerator_Reset_mB44AF6F2AE44CC8C9F91C86FFEF604A906770458(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__9_System_Collections_IEnumerator_get_Current_m3412A2D0FB64EECBA79C5949DCBD517BE46F9176(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0_CustomAttributesCacheGenerator_deathEffect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4_CustomAttributesCacheGenerator_targetTag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4_CustomAttributesCacheGenerator_chaseRadius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4_CustomAttributesCacheGenerator_attackRadius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC_CustomAttributesCacheGenerator_roomToLoad(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC_CustomAttributesCacheGenerator_playerTag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC_CustomAttributesCacheGenerator_playerPosInRoomValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC_CustomAttributesCacheGenerator_playerPosInRoom(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SOObserver_tD018D77957D756C9D4E80B373BCE27677AA0AA21_CustomAttributesCacheGenerator_objectsInMemory(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01_CustomAttributesCacheGenerator_anim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DamageOnContact_tBB1847BCB421560EEED6D01940B49C791B1323A4_CustomAttributesCacheGenerator_otherString(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DamageOnContact_tBB1847BCB421560EEED6D01940B49C791B1323A4_CustomAttributesCacheGenerator_damageAmount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DestroyOverTime_t72388AFBB9556818D23C5BD91F53CC8EA9FDC03F_CustomAttributesCacheGenerator_destroyDelay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866_CustomAttributesCacheGenerator_mySprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866_CustomAttributesCacheGenerator_flashColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866_CustomAttributesCacheGenerator_numberOfFlashes(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866_CustomAttributesCacheGenerator_flashDelay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866_CustomAttributesCacheGenerator_FlashColor_FlashCo_m07901743C6FAEF83D1AB61EF87A1D1AC9BB8B387(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_0_0_0_var), NULL);
	}
}
static void U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_CustomAttributesCacheGenerator_U3CFlashCoU3Ed__6__ctor_m282505D8A6C582DC83B04A78C33D051B74AB39E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_CustomAttributesCacheGenerator_U3CFlashCoU3Ed__6_System_IDisposable_Dispose_m88C743246933E0B24A5BB46D1E89032441C80EA7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_CustomAttributesCacheGenerator_U3CFlashCoU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m842202646B2172656DBB62CC371E6D6665ADD6A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_CustomAttributesCacheGenerator_U3CFlashCoU3Ed__6_System_Collections_IEnumerator_Reset_m4AC4B01410D3911850D0217901498DB1FD07A134(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_CustomAttributesCacheGenerator_U3CFlashCoU3Ed__6_System_Collections_IEnumerator_get_Current_mC61263B1374CD2BE6E68CBC0148D574A19638B3E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Health_tB86D9293C9CF1E5B8E4C7271395F56DD4C67AE96_CustomAttributesCacheGenerator_maxHealth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x61\x6C\x74\x68\x20\x76\x61\x6C\x75\x65\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x20\x61\x6E\x64\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x68\x65\x61\x6C\x74\x68\x20\xA\x20\x53\x65\x74\x20\x74\x68\x69\x73\x20\x74\x6F\x20\x6F\x6E\x65\x20\x66\x6F\x72\x20\x70\x6F\x74\x73"), NULL);
	}
}
static void Health_tB86D9293C9CF1E5B8E4C7271395F56DD4C67AE96_CustomAttributesCacheGenerator_currentHealth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7_CustomAttributesCacheGenerator_playerInRange(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7_CustomAttributesCacheGenerator_otherTag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7_CustomAttributesCacheGenerator_myNotification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4_CustomAttributesCacheGenerator_otherTag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4_CustomAttributesCacheGenerator_knockTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4_CustomAttributesCacheGenerator_knockStrength(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Magic_t0CC50A162DDF9C4C4549E05883368ACD19D8F36C_CustomAttributesCacheGenerator_currentMagic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Magic_t0CC50A162DDF9C4C4549E05883368ACD19D8F36C_CustomAttributesCacheGenerator_maxMagic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C_CustomAttributesCacheGenerator_myRigidbody(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ResetToPosition_t9E69B3C34D3A9B21F794674DA94578EB3FC0183B_CustomAttributesCacheGenerator_resetPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_anim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_openValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_isOpen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_chestNotification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_spriteValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_itemString(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_myItem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_playerInventory(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B_CustomAttributesCacheGenerator_isOpen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B_CustomAttributesCacheGenerator_isOpenValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B_CustomAttributesCacheGenerator_playerInventory(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B_CustomAttributesCacheGenerator_keyitem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B_CustomAttributesCacheGenerator_doorSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B_CustomAttributesCacheGenerator_doorCollider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void pot_tBC07980977D1BF32444BED885CFF2F3BC7FB875D_CustomAttributesCacheGenerator_pot_breakCo_m136505C681494DEFE0A2FD7B11005EFF45467773(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_0_0_0_var), NULL);
	}
}
static void U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_CustomAttributesCacheGenerator_U3CbreakCoU3Ed__4__ctor_m4C5D4FA0324868E76681FE2DD7569F941DE6ED2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_CustomAttributesCacheGenerator_U3CbreakCoU3Ed__4_System_IDisposable_Dispose_mBE51471B28DFE63D563EB50A7557E492221EBCB7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_CustomAttributesCacheGenerator_U3CbreakCoU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4532DAB64070BD2B8B9F4D6D27B2E56D29695783(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_CustomAttributesCacheGenerator_U3CbreakCoU3Ed__4_System_Collections_IEnumerator_Reset_m3907F5C4D0C4E2A92262694229C6B6FC3A64A405(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_CustomAttributesCacheGenerator_U3CbreakCoU3Ed__4_System_Collections_IEnumerator_get_Current_m1077B64857782AE047E326DB80336EF7C7D3215B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Room_t44A97854220FADE8C026E18625929E32B64B3F6A_CustomAttributesCacheGenerator_roomName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x6F\x6D\x20\x4E\x6F\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E\x73"), NULL);
	}
}
static void Room_t44A97854220FADE8C026E18625929E32B64B3F6A_CustomAttributesCacheGenerator_roomNameHolder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Room_t44A97854220FADE8C026E18625929E32B64B3F6A_CustomAttributesCacheGenerator_roomnotification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Room_t44A97854220FADE8C026E18625929E32B64B3F6A_CustomAttributesCacheGenerator_playerTag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Room_t44A97854220FADE8C026E18625929E32B64B3F6A_CustomAttributesCacheGenerator_respawnObjects(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Room_t44A97854220FADE8C026E18625929E32B64B3F6A_CustomAttributesCacheGenerator_thisCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ContextClue_tE96BB9C19D11CBC0A0137551DAFF0EE6796339B1_CustomAttributesCacheGenerator_mySprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ContextClue_tE96BB9C19D11CBC0A0137551DAFF0EE6796339B1_CustomAttributesCacheGenerator_clueActive(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465_CustomAttributesCacheGenerator_flash(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerMoney_tCEB0119C0B290EA5E47264E49FB4D95AD5CD3E9A_CustomAttributesCacheGenerator_currentMoney(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerMoney_tCEB0119C0B290EA5E47264E49FB4D95AD5CD3E9A_CustomAttributesCacheGenerator_maxMoney(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7_CustomAttributesCacheGenerator_mySprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7_CustomAttributesCacheGenerator_receivedSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7_CustomAttributesCacheGenerator_anim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7_CustomAttributesCacheGenerator_myState(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7_CustomAttributesCacheGenerator_isActive(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7_CustomAttributesCacheGenerator_dialogNotification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x61\x6C\x6F\x67\x20\x53\x74\x75\x66\x66"), NULL);
	}
}
static void ResetPlayerPosition_tA80F12DC82B6A24D6CDD43EAE82E742CAA76632D_CustomAttributesCacheGenerator_playerPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DashAbility_t9D1464CE8BDF1A9D6CE8253D6E2AFC8ECA794DC9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x20\x4F\x62\x6A\x65\x63\x74\x73\x2F\x41\x62\x69\x6C\x69\x74\x69\x65\x73\x2F\x44\x61\x73\x68\x20\x41\x62\x69\x6C\x69\x74\x79"), NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x73\x68\x20\x41\x62\x69\x6C\x69\x74\x79"), NULL);
	}
}
static void GenericAbility_t6F5062DC8FA17FF2828A882D17D7D48864095C21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x20\x4F\x62\x6A\x65\x63\x74\x73\x2F\x41\x62\x69\x6C\x69\x74\x69\x65\x73\x2F\x47\x65\x6E\x65\x72\x69\x63\x20\x41\x62\x69\x6C\x69\x74\x79"), NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x47\x65\x6E\x65\x72\x69\x63\x20\x41\x62\x69\x6C\x69\x74\x79"), NULL);
	}
}
static void BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x20\x4F\x62\x6A\x65\x63\x74\x73\x2F\x42\x6F\x6F\x6C\x20\x56\x61\x6C\x75\x65"), NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x42\x6F\x6F\x6C\x20\x56\x61\x6C\x75\x65"), NULL);
	}
}
static void BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370_CustomAttributesCacheGenerator_resetValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x20\x4F\x62\x6A\x65\x63\x74\x73\x2F\x56\x61\x6C\x75\x65\x73\x2F\x46\x6C\x6F\x61\x74\x20\x56\x61\x6C\x75\x65"), NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x46\x6C\x6F\x61\x74\x20\x56\x61\x6C\x75\x65"), NULL);
	}
}
static void FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10_CustomAttributesCacheGenerator_defaultValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10_CustomAttributesCacheGenerator_RuntimeValue(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2F\x50\x6C\x61\x79\x65\x72\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79"), NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x50\x6C\x61\x79\x65\x72\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79"), NULL);
	}
}
static void InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x20\x4F\x62\x6A\x65\x63\x74\x73\x2F\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x49\x74\x65\x6D"), NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x49\x74\x65\x6D"), NULL);
	}
}
static void Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x20\x4F\x62\x6A\x65\x63\x74\x73\x2F\x4E\x6F\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E"), NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x4E\x6F\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void NotificationListener_t576AF20BBC693CDD899148976A9D0D3778058225_CustomAttributesCacheGenerator_myNotification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SignalSender_t0075E93BFF2201C4C328193746B0F29E902AACCA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
	}
}
static void SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x20\x4F\x62\x6A\x65\x63\x74\x73\x2F\x53\x70\x72\x69\x74\x65\x20\x56\x61\x6C\x75\x65"), NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x53\x70\x72\x69\x74\x65\x20\x56\x61\x6C\x75\x65"), NULL);
	}
}
static void StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x20\x4F\x62\x6A\x65\x63\x74\x73\x2F\x53\x74\x72\x69\x6E\x67\x56\x61\x6C\x75\x65"), NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x53\x74\x72\x69\x6E\x67\x20\x56\x61\x6C\x75\x65"), NULL);
	}
}
static void VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x20\x4F\x62\x6A\x65\x63\x74\x73\x2F\x56\x65\x63\x74\x6F\x72\x20\x56\x61\x6C\x75\x65"), NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x56\x65\x63\x74\x6F\x72\x20\x56\x61\x6C\x75\x65"), NULL);
	}
}
static void VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD_CustomAttributesCacheGenerator_defaultValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3_CustomAttributesCacheGenerator_sentences(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 3LL, 10LL, NULL);
	}
}
static void DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D_CustomAttributesCacheGenerator_DialogueManager_TypeSentence_m74B6A598B53C2B9725C6343F801FD272F6F60CE4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_0_0_0_var), NULL);
	}
}
static void U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__7__ctor_m9DA545853895CDAE646DBA7EFF77CAB01870125A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__7_System_IDisposable_Dispose_mBB525A4327EF1046B3FECA28648E6B831051994A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33A483BEE1B98A623207D8191EDA472659750BED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__7_System_Collections_IEnumerator_Reset_mFAE38B6F7B55A3EAA896B235FC31ECE3D8A63A01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__7_System_Collections_IEnumerator_get_Current_mDF5ED2D5A6FBBBA1248735893AD2A53E944050BD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61_CustomAttributesCacheGenerator_nameText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61_CustomAttributesCacheGenerator_roomNameValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61_CustomAttributesCacheGenerator_duration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61_CustomAttributesCacheGenerator_AreaNameController_NameCo_mCB01D701882E7B4DE795086037035E22C1310D2C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_0_0_0_var), NULL);
	}
}
static void U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_CustomAttributesCacheGenerator_U3CNameCoU3Ed__4__ctor_m0295877D75B0907AB6E571ACF7EDFBD4C62C510A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_CustomAttributesCacheGenerator_U3CNameCoU3Ed__4_System_IDisposable_Dispose_mA2CE24FE422BB68D8D07A65F547562F8A0C4CCDC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_CustomAttributesCacheGenerator_U3CNameCoU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF84216422D657C1B0CA4B2D9DB13DC626D5632A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_CustomAttributesCacheGenerator_U3CNameCoU3Ed__4_System_Collections_IEnumerator_Reset_m466BA511DD3CA1F9BF5F805759C3A2351DFA8667(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_CustomAttributesCacheGenerator_U3CNameCoU3Ed__4_System_Collections_IEnumerator_get_Current_m334CB1DAD5F575544F8E8E9F879F7A42DF38CDDF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4_CustomAttributesCacheGenerator_stringText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4_CustomAttributesCacheGenerator_dialogNotification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4_CustomAttributesCacheGenerator_dialogText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4_CustomAttributesCacheGenerator_dialogObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4_CustomAttributesCacheGenerator_dialogActive(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64_CustomAttributesCacheGenerator_creditsPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64_CustomAttributesCacheGenerator_gamePanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64_CustomAttributesCacheGenerator_levelToLoad(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64_CustomAttributesCacheGenerator_playerPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64_CustomAttributesCacheGenerator_playerPos(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE_CustomAttributesCacheGenerator_isPaused(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE_CustomAttributesCacheGenerator_pauseMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE_CustomAttributesCacheGenerator_inventoryMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE_CustomAttributesCacheGenerator_gameSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE_CustomAttributesCacheGenerator_menuSceneString(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D_CustomAttributesCacheGenerator_m_Points(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D_CustomAttributesCacheGenerator_m_IsClosed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D_CustomAttributesCacheGenerator_m_CollisionShapeType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_tB40012BE63C6AF48A235BC43CC8750930461F581_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass25_0_tE31A76DF44246018C15E08B2B2A89A39A82EF09C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t0C7724567A46A1BEEDB72095B8FD8DB98CF36CCB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CustomPropertyListExtensions_t6D2E5CC1B16B9EB6B57704EF873A5C12550BF269_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CustomPropertyListExtensions_t6D2E5CC1B16B9EB6B57704EF873A5C12550BF269_CustomAttributesCacheGenerator_CustomPropertyListExtensions_TryGetProperty_m610D3ACC325AB44067452C515DD10062DC01F9AA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t4F7D9550667533CDDCC7B1CD0856AC36267DCADA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator_CustomPropertyExtensions_GetValueAsString_mE3789EE7A117E66D8FE55E977B31F18FB7480346(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator_CustomPropertyExtensions_GetValueAsColor_mC872F887E4560B76E54E12FC539BB7AF618B4E37(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator_CustomPropertyExtensions_GetValueAsInt_mCA843CA55CE6C08B5C1621B71EDC8010E32A9499(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator_CustomPropertyExtensions_GetValueAsFloat_m3183E8A5D90D817BEE59EF0F5BA438B532D63540(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator_CustomPropertyExtensions_GetValueAsBool_mFACA71E29D4012D9680FCD905CE064F0441CA334(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator_CustomPropertyExtensions_GetValueAsEnum_m606C30493F293B6426F3C5191C7F80E84052BBB7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void EnumerableExtensions_tD5F4F8A027DFBE97FAEFED9B93132CBC30CF92C3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void EnumerableExtensions_tD5F4F8A027DFBE97FAEFED9B93132CBC30CF92C3_CustomAttributesCacheGenerator_EnumerableExtensions_IsEmpty_m421ACFEF63FAC686AB627EE8CC43C0713814741F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GameObjectExtensions_tCE982D9EF1C18C5AEA8B9B5186D90F62E9B27DE1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GameObjectExtensions_tCE982D9EF1C18C5AEA8B9B5186D90F62E9B27DE1_CustomAttributesCacheGenerator_GameObjectExtensions_GetComponentInAncestor_m215A90C0435745210AABF8A53FA1C105C4AD1807(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GameObjectExtensions_tCE982D9EF1C18C5AEA8B9B5186D90F62E9B27DE1_CustomAttributesCacheGenerator_GameObjectExtensions_GetComponentInAncestor_m4B28210D1D33FE5BCC0739810772957D45A2C83A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GameObjectExtensions_tCE982D9EF1C18C5AEA8B9B5186D90F62E9B27DE1_CustomAttributesCacheGenerator_GameObjectExtensions_TryGetCustomPropertySafe_m5F5E5234B522E79EF3DC4BF7EF336F04A64E774D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826_CustomAttributesCacheGenerator_StringExtensions_ToColor_mC4A382B8EBE0EB3814C58E82CBA3C9A29556AB13(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826_CustomAttributesCacheGenerator_StringExtensions_ToEnum_m148B1D54369ECADB45D8F8756D153C89B58B2A91(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826_CustomAttributesCacheGenerator_StringExtensions_ToFloat_m43913E60F4BC0CB3F25B5F58E76F99FD262FE170(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826_CustomAttributesCacheGenerator_StringExtensions_ToInt_m6D40614BFE9C46CCAA31108B53F17E0C5B00A6EB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826_CustomAttributesCacheGenerator_StringExtensions_ToBool_m87339C631EFBEDFEF969558D94869FFE57A9B6F5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_TryGetProperty_mDAF7C872C57BFAAD0ADAC8D1E441A6CFF717ECBD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsString_m83D3B395862333224EAFB46EBF43FB8C2C828EAF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsString_m643DC10D0EA82D87CA2E0DEE4C2FB0C652D0CBF2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsBool_m37C447EF97E56A3D152BD60D7F948CF666971847(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsBool_m69DBF9E59581BDF7FC26AFD307E139B22BAA0E64(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsInt_m3D59F7D85ABF8C86A01982D46A31913CB8AC4096(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsInt_m1168F2B3C96A451AB03EDFA45AF55FED065FB4A1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsFloat_m87A33A3D138C5E9D28AEA46C33A9D0C578772424(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsFloat_mCF862373A55454F0D2CD9F1DA72419E096B51D2F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsColor_m8E2692F364FDB1E5B322CA17704ECA066E7CB9D5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsColor_mA472B0F8E5E6EB492DE9921DAD17FD714E2560F1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsEnum_m3DF474A30A1BF8D967A5C9BE7F0A6D092724988F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsEnum_mD8872CC53876E727D4F20069189936A15DB72FB4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SuperImageLayer_t3CD24E164ADB8EE9C72A0C15676F397EBA60EDA5_CustomAttributesCacheGenerator_m_ImageFilename(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C_CustomAttributesCacheGenerator_m_TiledName(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C_CustomAttributesCacheGenerator_m_OffsetX(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C_CustomAttributesCacheGenerator_m_OffsetY(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C_CustomAttributesCacheGenerator_m_Opacity(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C_CustomAttributesCacheGenerator_m_Visible(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_Version(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_TiledVersion(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_Orientation(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_RenderOrder(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_Width(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_Height(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_TileWidth(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_TileHeight(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_HexSideLength(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_StaggerAxis(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_StaggerIndex(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_Infinite(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_BackgroundColor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_NextObjectId(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Id(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_TiledName(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Type(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_X(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Y(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Width(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Height(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Rotation(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_TileId(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_SuperTile(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Visible(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Template(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperObjectLayer_tC27DDCDAFE05ACF9AF26AC4508F65C38A46F4EBE_CustomAttributesCacheGenerator_m_Color(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_TileId(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_Sprite(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_AnimationSprites(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_Type(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_Width(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_Height(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_TileOffsetX(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_TileOffsetY(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 * tmp = (ReadOnlyAttribute_t1D7BDA98B780208E67473E23D599699C1F5A7541 *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mCF398F8718270BE45CA1C61226312D861C29C4DE(tmp, NULL);
	}
}
static void OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571_CustomAttributesCacheGenerator_OverheadMegaDadController_Drowned_mDB9569C7C77F6BE33B8B56A17356BB10476F36C6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_t069EA630E146EBD7BB9CE47F83C828A52666D434_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_CustomAttributesCacheGenerator_U3CDrownedU3Ed__21__ctor_m9B1840C68AA02443082D4BB4593B8C2DDBC9C2AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_CustomAttributesCacheGenerator_U3CDrownedU3Ed__21_System_IDisposable_Dispose_m4356657D60E5593B70E506308D2484CCC65E9031(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_CustomAttributesCacheGenerator_U3CDrownedU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD91C1CB91A7DEB29CFD48D50B5BC5E486A12F7E2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_CustomAttributesCacheGenerator_U3CDrownedU3Ed__21_System_Collections_IEnumerator_Reset_mCA29722363B203EF658DFC6D6D82610E82821BD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_CustomAttributesCacheGenerator_U3CDrownedU3Ed__21_System_Collections_IEnumerator_get_Current_m060EBCDCC94B24D967E22B5569D5BF7564394C0C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[261] = 
{
	U3CU3Ec__DisplayClass4_0_tA3BCDD4327DA28EB58F404C4F16EF439D2996561_CustomAttributesCacheGenerator,
	U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_CustomAttributesCacheGenerator,
	U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_CustomAttributesCacheGenerator,
	U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_CustomAttributesCacheGenerator,
	U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_CustomAttributesCacheGenerator,
	U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_CustomAttributesCacheGenerator,
	U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_CustomAttributesCacheGenerator,
	U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_CustomAttributesCacheGenerator,
	DashAbility_t9D1464CE8BDF1A9D6CE8253D6E2AFC8ECA794DC9_CustomAttributesCacheGenerator,
	GenericAbility_t6F5062DC8FA17FF2828A882D17D7D48864095C21_CustomAttributesCacheGenerator,
	BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370_CustomAttributesCacheGenerator,
	FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10_CustomAttributesCacheGenerator,
	Inventory_tF12A20E68964E098DC4A1C249738A1FAA55B5805_CustomAttributesCacheGenerator,
	InventoryItem_tB22B9629D39BEBFB67295E76B28F6EE94796FAF3_CustomAttributesCacheGenerator,
	Notification_t8B0FDC10D2E2E24559928C990E4097CB3EF6284F_CustomAttributesCacheGenerator,
	SignalSender_t0075E93BFF2201C4C328193746B0F29E902AACCA_CustomAttributesCacheGenerator,
	SpriteValue_tDE769B1F79630DB383024EC72609FB00298E5A6C_CustomAttributesCacheGenerator,
	StringValue_tE50BE699D26295B973FBDFCB58BC5E2FAE5FE760_CustomAttributesCacheGenerator,
	VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD_CustomAttributesCacheGenerator,
	U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_CustomAttributesCacheGenerator,
	U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_tB40012BE63C6AF48A235BC43CC8750930461F581_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass25_0_tE31A76DF44246018C15E08B2B2A89A39A82EF09C_CustomAttributesCacheGenerator,
	U3CU3Ec_t0C7724567A46A1BEEDB72095B8FD8DB98CF36CCB_CustomAttributesCacheGenerator,
	CustomPropertyListExtensions_t6D2E5CC1B16B9EB6B57704EF873A5C12550BF269_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t4F7D9550667533CDDCC7B1CD0856AC36267DCADA_CustomAttributesCacheGenerator,
	CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator,
	EnumerableExtensions_tD5F4F8A027DFBE97FAEFED9B93132CBC30CF92C3_CustomAttributesCacheGenerator,
	GameObjectExtensions_tCE982D9EF1C18C5AEA8B9B5186D90F62E9B27DE1_CustomAttributesCacheGenerator,
	StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826_CustomAttributesCacheGenerator,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator,
	U3CU3Ec_t069EA630E146EBD7BB9CE47F83C828A52666D434_CustomAttributesCacheGenerator,
	U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_CustomAttributesCacheGenerator,
	PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_anim,
	PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_myState,
	PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_WeaponAttackDuration,
	PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_myItem,
	PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_currentAbility,
	Sound_tF983595F9C621A86B56E05F9778810369E90A0FE_CustomAttributesCacheGenerator_volume,
	Sound_tF983595F9C621A86B56E05F9778810369E90A0FE_CustomAttributesCacheGenerator_pitch,
	Sound_tF983595F9C621A86B56E05F9778810369E90A0FE_CustomAttributesCacheGenerator_source,
	EnemyHealth_tCE899729F13DE9E2D589DB582CE4B74DECF5BAB0_CustomAttributesCacheGenerator_deathEffect,
	SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4_CustomAttributesCacheGenerator_targetTag,
	SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4_CustomAttributesCacheGenerator_chaseRadius,
	SimpleFollow_tFA0F2DA35622D312E71A1EAB8F97F76371E5F5A4_CustomAttributesCacheGenerator_attackRadius,
	RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC_CustomAttributesCacheGenerator_roomToLoad,
	RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC_CustomAttributesCacheGenerator_playerTag,
	RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC_CustomAttributesCacheGenerator_playerPosInRoomValue,
	RoomTransfer_tF430EA8B4B49CBFE2E8C1BF5973D79856423E6EC_CustomAttributesCacheGenerator_playerPosInRoom,
	SOObserver_tD018D77957D756C9D4E80B373BCE27677AA0AA21_CustomAttributesCacheGenerator_objectsInMemory,
	AnimatorController_t6DA00D9242951B6D6564822E15883EDC274A3E01_CustomAttributesCacheGenerator_anim,
	DamageOnContact_tBB1847BCB421560EEED6D01940B49C791B1323A4_CustomAttributesCacheGenerator_otherString,
	DamageOnContact_tBB1847BCB421560EEED6D01940B49C791B1323A4_CustomAttributesCacheGenerator_damageAmount,
	DestroyOverTime_t72388AFBB9556818D23C5BD91F53CC8EA9FDC03F_CustomAttributesCacheGenerator_destroyDelay,
	FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866_CustomAttributesCacheGenerator_mySprite,
	FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866_CustomAttributesCacheGenerator_flashColor,
	FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866_CustomAttributesCacheGenerator_numberOfFlashes,
	FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866_CustomAttributesCacheGenerator_flashDelay,
	Health_tB86D9293C9CF1E5B8E4C7271395F56DD4C67AE96_CustomAttributesCacheGenerator_maxHealth,
	Health_tB86D9293C9CF1E5B8E4C7271395F56DD4C67AE96_CustomAttributesCacheGenerator_currentHealth,
	Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7_CustomAttributesCacheGenerator_playerInRange,
	Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7_CustomAttributesCacheGenerator_otherTag,
	Interactable_tEEFC60C29F907276ABC0BDA6621F8089144774E7_CustomAttributesCacheGenerator_myNotification,
	Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4_CustomAttributesCacheGenerator_otherTag,
	Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4_CustomAttributesCacheGenerator_knockTime,
	Knockback_t33E1ADB9DDA7C243242BB3418BE2ABF302D753A4_CustomAttributesCacheGenerator_knockStrength,
	Magic_t0CC50A162DDF9C4C4549E05883368ACD19D8F36C_CustomAttributesCacheGenerator_currentMagic,
	Magic_t0CC50A162DDF9C4C4549E05883368ACD19D8F36C_CustomAttributesCacheGenerator_maxMagic,
	Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C_CustomAttributesCacheGenerator_speed,
	Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C_CustomAttributesCacheGenerator_myRigidbody,
	ResetToPosition_t9E69B3C34D3A9B21F794674DA94578EB3FC0183B_CustomAttributesCacheGenerator_resetPosition,
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_anim,
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_openValue,
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_isOpen,
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_chestNotification,
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_spriteValue,
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_itemString,
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_myItem,
	Chest_tF5FAEEFD66F59BDFBFCE78BE5BCAB9FCA9F1B66C_CustomAttributesCacheGenerator_playerInventory,
	LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B_CustomAttributesCacheGenerator_isOpen,
	LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B_CustomAttributesCacheGenerator_isOpenValue,
	LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B_CustomAttributesCacheGenerator_playerInventory,
	LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B_CustomAttributesCacheGenerator_keyitem,
	LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B_CustomAttributesCacheGenerator_doorSprite,
	LockedDoor_t8C131334B39BA9DD903BD81DDAA2F9DF45D7695B_CustomAttributesCacheGenerator_doorCollider,
	Room_t44A97854220FADE8C026E18625929E32B64B3F6A_CustomAttributesCacheGenerator_roomName,
	Room_t44A97854220FADE8C026E18625929E32B64B3F6A_CustomAttributesCacheGenerator_roomNameHolder,
	Room_t44A97854220FADE8C026E18625929E32B64B3F6A_CustomAttributesCacheGenerator_roomnotification,
	Room_t44A97854220FADE8C026E18625929E32B64B3F6A_CustomAttributesCacheGenerator_playerTag,
	Room_t44A97854220FADE8C026E18625929E32B64B3F6A_CustomAttributesCacheGenerator_respawnObjects,
	Room_t44A97854220FADE8C026E18625929E32B64B3F6A_CustomAttributesCacheGenerator_thisCamera,
	ContextClue_tE96BB9C19D11CBC0A0137551DAFF0EE6796339B1_CustomAttributesCacheGenerator_mySprite,
	ContextClue_tE96BB9C19D11CBC0A0137551DAFF0EE6796339B1_CustomAttributesCacheGenerator_clueActive,
	PlayerHealth_tC1C9C8425A66FBA2BCAEC86CBFC5FEAF4E260465_CustomAttributesCacheGenerator_flash,
	PlayerMoney_tCEB0119C0B290EA5E47264E49FB4D95AD5CD3E9A_CustomAttributesCacheGenerator_currentMoney,
	PlayerMoney_tCEB0119C0B290EA5E47264E49FB4D95AD5CD3E9A_CustomAttributesCacheGenerator_maxMoney,
	ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7_CustomAttributesCacheGenerator_mySprite,
	ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7_CustomAttributesCacheGenerator_receivedSprite,
	ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7_CustomAttributesCacheGenerator_anim,
	ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7_CustomAttributesCacheGenerator_myState,
	ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7_CustomAttributesCacheGenerator_isActive,
	ReceiveItem_t5EDEEBD72CCE222425BEF7DB6679987F172B04A7_CustomAttributesCacheGenerator_dialogNotification,
	ResetPlayerPosition_tA80F12DC82B6A24D6CDD43EAE82E742CAA76632D_CustomAttributesCacheGenerator_playerPosition,
	BoolValue_t8B3B72144A9105F84EBBA372F0B032F29D634370_CustomAttributesCacheGenerator_resetValue,
	FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10_CustomAttributesCacheGenerator_defaultValue,
	FloatValue_tB14BFE8FF46D537497C432C640C19E8F597B6E10_CustomAttributesCacheGenerator_RuntimeValue,
	NotificationListener_t576AF20BBC693CDD899148976A9D0D3778058225_CustomAttributesCacheGenerator_myNotification,
	VectorValue_tE3EC719A9C5C3126C08E080C21E0C3F1C61372DD_CustomAttributesCacheGenerator_defaultValue,
	Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3_CustomAttributesCacheGenerator_sentences,
	AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61_CustomAttributesCacheGenerator_nameText,
	AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61_CustomAttributesCacheGenerator_roomNameValue,
	AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61_CustomAttributesCacheGenerator_duration,
	DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4_CustomAttributesCacheGenerator_stringText,
	DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4_CustomAttributesCacheGenerator_dialogNotification,
	DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4_CustomAttributesCacheGenerator_dialogText,
	DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4_CustomAttributesCacheGenerator_dialogObject,
	DialogController_t046B8136C6FBFDF80C4F242646A843FABBFDE3D4_CustomAttributesCacheGenerator_dialogActive,
	GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64_CustomAttributesCacheGenerator_creditsPanel,
	GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64_CustomAttributesCacheGenerator_gamePanel,
	GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64_CustomAttributesCacheGenerator_levelToLoad,
	GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64_CustomAttributesCacheGenerator_playerPosition,
	GameMenu_tE4F3645715725D5408136E5530CCD951D1E72A64_CustomAttributesCacheGenerator_playerPos,
	PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE_CustomAttributesCacheGenerator_isPaused,
	PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE_CustomAttributesCacheGenerator_pauseMenu,
	PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE_CustomAttributesCacheGenerator_inventoryMenu,
	PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE_CustomAttributesCacheGenerator_gameSpeed,
	PauseManager_t67F876F4C11A409A35B4CB5AFC7A909F79555ADE_CustomAttributesCacheGenerator_menuSceneString,
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D_CustomAttributesCacheGenerator_m_Points,
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D_CustomAttributesCacheGenerator_m_IsClosed,
	CollisionObject_tF83A839F20625CF65EB0EE7A6D900560F5225F9D_CustomAttributesCacheGenerator_m_CollisionShapeType,
	SuperImageLayer_t3CD24E164ADB8EE9C72A0C15676F397EBA60EDA5_CustomAttributesCacheGenerator_m_ImageFilename,
	SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C_CustomAttributesCacheGenerator_m_TiledName,
	SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C_CustomAttributesCacheGenerator_m_OffsetX,
	SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C_CustomAttributesCacheGenerator_m_OffsetY,
	SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C_CustomAttributesCacheGenerator_m_Opacity,
	SuperLayer_t154519435846842C01EB7DBDE5B71691A4AD411C_CustomAttributesCacheGenerator_m_Visible,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_Version,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_TiledVersion,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_Orientation,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_RenderOrder,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_Width,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_Height,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_TileWidth,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_TileHeight,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_HexSideLength,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_StaggerAxis,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_StaggerIndex,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_Infinite,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_BackgroundColor,
	SuperMap_tD6F26202A44E6CBBA9907010DBCD5B97C2358A96_CustomAttributesCacheGenerator_m_NextObjectId,
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Id,
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_TiledName,
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Type,
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_X,
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Y,
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Width,
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Height,
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Rotation,
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_TileId,
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_SuperTile,
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Visible,
	SuperObject_t917A0508B2A79389E3DDFF0D1F1A2184F0F1FA21_CustomAttributesCacheGenerator_m_Template,
	SuperObjectLayer_tC27DDCDAFE05ACF9AF26AC4508F65C38A46F4EBE_CustomAttributesCacheGenerator_m_Color,
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_TileId,
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_Sprite,
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_AnimationSprites,
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_Type,
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_Width,
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_Height,
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_TileOffsetX,
	SuperTile_t8D47C3202924FFC4BC5827E7E5081D82781C3B14_CustomAttributesCacheGenerator_m_TileOffsetY,
	PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_PlayerMovement_WeaponCo_m7E38E79787A651A1F512F9E0CF42E98A2398D396,
	PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_PlayerMovement_AbilityCo_mB527E62AFDABEA91E50C6841F440F178C87D971D,
	PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_PlayerMovement_Knockco_m9FB0A3F5655B26872CF8F08DE922AAC0BA2CD9AB,
	PlayerMovement_t324642B864F0A1AE4225A972674F8FD21DCA5F09_CustomAttributesCacheGenerator_PlayerMovement_SecondAttackCo_m35F4438A058472BE31F0537E3AA967A0F8E26349,
	U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_CustomAttributesCacheGenerator_U3CWeaponCoU3Ed__19__ctor_mF6774EDF96E29D4CF1DE6E59A385EF4733577499,
	U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_CustomAttributesCacheGenerator_U3CWeaponCoU3Ed__19_System_IDisposable_Dispose_m336B4209188172D5D2E7DC91BA51B13E1B53AD56,
	U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_CustomAttributesCacheGenerator_U3CWeaponCoU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE75159A118D1CCD0D21C44DA7A1BCCEFEED035B,
	U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_CustomAttributesCacheGenerator_U3CWeaponCoU3Ed__19_System_Collections_IEnumerator_Reset_m92D841A85EA7636FE60CE098FB0E509F112F57D3,
	U3CWeaponCoU3Ed__19_tA0E5E02FB72E1B273ED4E6ECA3AD68A2AD4CAC45_CustomAttributesCacheGenerator_U3CWeaponCoU3Ed__19_System_Collections_IEnumerator_get_Current_mFA45A158917C1D00D0414546D330B5C50AF98810,
	U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_CustomAttributesCacheGenerator_U3CAbilityCoU3Ed__20__ctor_m0176682625B409EBAF4204EB4659BDF4D41F4BA5,
	U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_CustomAttributesCacheGenerator_U3CAbilityCoU3Ed__20_System_IDisposable_Dispose_m7077FA91B9383678A31E38756736A526CF59A3FC,
	U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_CustomAttributesCacheGenerator_U3CAbilityCoU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1B45335C8F68A00353045AFA6267AE71F07182E1,
	U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_CustomAttributesCacheGenerator_U3CAbilityCoU3Ed__20_System_Collections_IEnumerator_Reset_m8E3B2891A317FB08FA139BD950964EAD077B40C2,
	U3CAbilityCoU3Ed__20_t6CA84154936E400DABD0EE8FD9069E8D71D75523_CustomAttributesCacheGenerator_U3CAbilityCoU3Ed__20_System_Collections_IEnumerator_get_Current_mE6CEEBAB1AB8EE0C1A7052E7E832277A232B1DD5,
	U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__21__ctor_m0683E9332889422FD23C62E94DCA9758C00552BA,
	U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__21_System_IDisposable_Dispose_m3209EEDB656EB5486A1CF3DA9F24C350C4707499,
	U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF02042846E4388A7D23AEE7B092211A67F45B9B,
	U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__21_System_Collections_IEnumerator_Reset_mFB645C76CFD1ECF92A92F52F985A9D3459F302A5,
	U3CKnockcoU3Ed__21_tCC3BE94052F0FF13B7CC527E5D0AF6DC8B5879B5_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__21_System_Collections_IEnumerator_get_Current_m54FA11747AF9038FFEE415DA8CB553F911104081,
	U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_CustomAttributesCacheGenerator_U3CSecondAttackCoU3Ed__22__ctor_mF3BB48058B987D2323D3877A4B73BB38EF8FCA12,
	U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_CustomAttributesCacheGenerator_U3CSecondAttackCoU3Ed__22_System_IDisposable_Dispose_mD085A289E795223BE81314ED9425C51047B43990,
	U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_CustomAttributesCacheGenerator_U3CSecondAttackCoU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m566C8E4E0AABFF749D23CB669FA3AE323A002871,
	U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_CustomAttributesCacheGenerator_U3CSecondAttackCoU3Ed__22_System_Collections_IEnumerator_Reset_m6FAADB162A5C47DAB3951676C0EE2937468C2E00,
	U3CSecondAttackCoU3Ed__22_t6A68C8F49F988A8A4A802360529BBA6A5B7A7B38_CustomAttributesCacheGenerator_U3CSecondAttackCoU3Ed__22_System_Collections_IEnumerator_get_Current_m23AC4D1FAA25D82B0CE7ACFC49A43B7DC624AD7D,
	Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_Knockco_mD90ED67E0DA4A148DE3CB5FA33EB2187AAFB2F7A,
	U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__9__ctor_m2F874F51BAFBD6E0A502C95F49D0FDE4B00B3596,
	U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__9_System_IDisposable_Dispose_mA9E234FEC09DB70E40A923A8EE315EE24373BBC3,
	U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC2DEAF9142B8BC177D269B901C8ACE2F35A5B64,
	U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__9_System_Collections_IEnumerator_Reset_mB44AF6F2AE44CC8C9F91C86FFEF604A906770458,
	U3CKnockcoU3Ed__9_t59804E77141A4870B43723052CD5C29B34693080_CustomAttributesCacheGenerator_U3CKnockcoU3Ed__9_System_Collections_IEnumerator_get_Current_m3412A2D0FB64EECBA79C5949DCBD517BE46F9176,
	FlashColor_tDB22C7B71B88295F01BE9109B428329D4402E866_CustomAttributesCacheGenerator_FlashColor_FlashCo_m07901743C6FAEF83D1AB61EF87A1D1AC9BB8B387,
	U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_CustomAttributesCacheGenerator_U3CFlashCoU3Ed__6__ctor_m282505D8A6C582DC83B04A78C33D051B74AB39E1,
	U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_CustomAttributesCacheGenerator_U3CFlashCoU3Ed__6_System_IDisposable_Dispose_m88C743246933E0B24A5BB46D1E89032441C80EA7,
	U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_CustomAttributesCacheGenerator_U3CFlashCoU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m842202646B2172656DBB62CC371E6D6665ADD6A5,
	U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_CustomAttributesCacheGenerator_U3CFlashCoU3Ed__6_System_Collections_IEnumerator_Reset_m4AC4B01410D3911850D0217901498DB1FD07A134,
	U3CFlashCoU3Ed__6_t1CE021145AA2B5BE10C4CAF1D4B101EB9567CC46_CustomAttributesCacheGenerator_U3CFlashCoU3Ed__6_System_Collections_IEnumerator_get_Current_mC61263B1374CD2BE6E68CBC0148D574A19638B3E,
	pot_tBC07980977D1BF32444BED885CFF2F3BC7FB875D_CustomAttributesCacheGenerator_pot_breakCo_m136505C681494DEFE0A2FD7B11005EFF45467773,
	U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_CustomAttributesCacheGenerator_U3CbreakCoU3Ed__4__ctor_m4C5D4FA0324868E76681FE2DD7569F941DE6ED2C,
	U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_CustomAttributesCacheGenerator_U3CbreakCoU3Ed__4_System_IDisposable_Dispose_mBE51471B28DFE63D563EB50A7557E492221EBCB7,
	U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_CustomAttributesCacheGenerator_U3CbreakCoU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4532DAB64070BD2B8B9F4D6D27B2E56D29695783,
	U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_CustomAttributesCacheGenerator_U3CbreakCoU3Ed__4_System_Collections_IEnumerator_Reset_m3907F5C4D0C4E2A92262694229C6B6FC3A64A405,
	U3CbreakCoU3Ed__4_tA43C9DE4D78EAB74CA7ECF45D98F64600BDC9E42_CustomAttributesCacheGenerator_U3CbreakCoU3Ed__4_System_Collections_IEnumerator_get_Current_m1077B64857782AE047E326DB80336EF7C7D3215B,
	DialogueManager_t609C696D1E27CEA7A0895EDADECF76865D33931D_CustomAttributesCacheGenerator_DialogueManager_TypeSentence_m74B6A598B53C2B9725C6343F801FD272F6F60CE4,
	U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__7__ctor_m9DA545853895CDAE646DBA7EFF77CAB01870125A,
	U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__7_System_IDisposable_Dispose_mBB525A4327EF1046B3FECA28648E6B831051994A,
	U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33A483BEE1B98A623207D8191EDA472659750BED,
	U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__7_System_Collections_IEnumerator_Reset_mFAE38B6F7B55A3EAA896B235FC31ECE3D8A63A01,
	U3CTypeSentenceU3Ed__7_t658255A739F52D7D8E1F4D4380E624ED7E953104_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__7_System_Collections_IEnumerator_get_Current_mDF5ED2D5A6FBBBA1248735893AD2A53E944050BD,
	AreaNameController_t7AD102592B9CDB75432D61A33D431B4DBA415D61_CustomAttributesCacheGenerator_AreaNameController_NameCo_mCB01D701882E7B4DE795086037035E22C1310D2C,
	U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_CustomAttributesCacheGenerator_U3CNameCoU3Ed__4__ctor_m0295877D75B0907AB6E571ACF7EDFBD4C62C510A,
	U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_CustomAttributesCacheGenerator_U3CNameCoU3Ed__4_System_IDisposable_Dispose_mA2CE24FE422BB68D8D07A65F547562F8A0C4CCDC,
	U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_CustomAttributesCacheGenerator_U3CNameCoU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF84216422D657C1B0CA4B2D9DB13DC626D5632A5,
	U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_CustomAttributesCacheGenerator_U3CNameCoU3Ed__4_System_Collections_IEnumerator_Reset_m466BA511DD3CA1F9BF5F805759C3A2351DFA8667,
	U3CNameCoU3Ed__4_t628F4ABB7202557D11A73F309110113DEE361268_CustomAttributesCacheGenerator_U3CNameCoU3Ed__4_System_Collections_IEnumerator_get_Current_m334CB1DAD5F575544F8E8E9F879F7A42DF38CDDF,
	CustomPropertyListExtensions_t6D2E5CC1B16B9EB6B57704EF873A5C12550BF269_CustomAttributesCacheGenerator_CustomPropertyListExtensions_TryGetProperty_m610D3ACC325AB44067452C515DD10062DC01F9AA,
	CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator_CustomPropertyExtensions_GetValueAsString_mE3789EE7A117E66D8FE55E977B31F18FB7480346,
	CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator_CustomPropertyExtensions_GetValueAsColor_mC872F887E4560B76E54E12FC539BB7AF618B4E37,
	CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator_CustomPropertyExtensions_GetValueAsInt_mCA843CA55CE6C08B5C1621B71EDC8010E32A9499,
	CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator_CustomPropertyExtensions_GetValueAsFloat_m3183E8A5D90D817BEE59EF0F5BA438B532D63540,
	CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator_CustomPropertyExtensions_GetValueAsBool_mFACA71E29D4012D9680FCD905CE064F0441CA334,
	CustomPropertyExtensions_t612F9270191C365740C962E60FE035C5A9A976F4_CustomAttributesCacheGenerator_CustomPropertyExtensions_GetValueAsEnum_m606C30493F293B6426F3C5191C7F80E84052BBB7,
	EnumerableExtensions_tD5F4F8A027DFBE97FAEFED9B93132CBC30CF92C3_CustomAttributesCacheGenerator_EnumerableExtensions_IsEmpty_m421ACFEF63FAC686AB627EE8CC43C0713814741F,
	GameObjectExtensions_tCE982D9EF1C18C5AEA8B9B5186D90F62E9B27DE1_CustomAttributesCacheGenerator_GameObjectExtensions_GetComponentInAncestor_m215A90C0435745210AABF8A53FA1C105C4AD1807,
	GameObjectExtensions_tCE982D9EF1C18C5AEA8B9B5186D90F62E9B27DE1_CustomAttributesCacheGenerator_GameObjectExtensions_GetComponentInAncestor_m4B28210D1D33FE5BCC0739810772957D45A2C83A,
	GameObjectExtensions_tCE982D9EF1C18C5AEA8B9B5186D90F62E9B27DE1_CustomAttributesCacheGenerator_GameObjectExtensions_TryGetCustomPropertySafe_m5F5E5234B522E79EF3DC4BF7EF336F04A64E774D,
	StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826_CustomAttributesCacheGenerator_StringExtensions_ToColor_mC4A382B8EBE0EB3814C58E82CBA3C9A29556AB13,
	StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826_CustomAttributesCacheGenerator_StringExtensions_ToEnum_m148B1D54369ECADB45D8F8756D153C89B58B2A91,
	StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826_CustomAttributesCacheGenerator_StringExtensions_ToFloat_m43913E60F4BC0CB3F25B5F58E76F99FD262FE170,
	StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826_CustomAttributesCacheGenerator_StringExtensions_ToInt_m6D40614BFE9C46CCAA31108B53F17E0C5B00A6EB,
	StringExtensions_t11C287992435A04AA098AE0982D74EADAB082826_CustomAttributesCacheGenerator_StringExtensions_ToBool_m87339C631EFBEDFEF969558D94869FFE57A9B6F5,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_TryGetProperty_mDAF7C872C57BFAAD0ADAC8D1E441A6CFF717ECBD,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsString_m83D3B395862333224EAFB46EBF43FB8C2C828EAF,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsString_m643DC10D0EA82D87CA2E0DEE4C2FB0C652D0CBF2,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsBool_m37C447EF97E56A3D152BD60D7F948CF666971847,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsBool_m69DBF9E59581BDF7FC26AFD307E139B22BAA0E64,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsInt_m3D59F7D85ABF8C86A01982D46A31913CB8AC4096,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsInt_m1168F2B3C96A451AB03EDFA45AF55FED065FB4A1,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsFloat_m87A33A3D138C5E9D28AEA46C33A9D0C578772424,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsFloat_mCF862373A55454F0D2CD9F1DA72419E096B51D2F,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsColor_m8E2692F364FDB1E5B322CA17704ECA066E7CB9D5,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsColor_mA472B0F8E5E6EB492DE9921DAD17FD714E2560F1,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsEnum_m3DF474A30A1BF8D967A5C9BE7F0A6D092724988F,
	SuperTileExtensions_t1A85F82E13B01A04F2B5890695674F4D6121FF7B_CustomAttributesCacheGenerator_SuperTileExtensions_GetPropertyValueAsEnum_mD8872CC53876E727D4F20069189936A15DB72FB4,
	OverheadMegaDadController_t3A95F6DFEEC8498F6E7CCBA29C83B4EA7122C571_CustomAttributesCacheGenerator_OverheadMegaDadController_Drowned_mDB9569C7C77F6BE33B8B56A17356BB10476F36C6,
	U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_CustomAttributesCacheGenerator_U3CDrownedU3Ed__21__ctor_m9B1840C68AA02443082D4BB4593B8C2DDBC9C2AD,
	U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_CustomAttributesCacheGenerator_U3CDrownedU3Ed__21_System_IDisposable_Dispose_m4356657D60E5593B70E506308D2484CCC65E9031,
	U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_CustomAttributesCacheGenerator_U3CDrownedU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD91C1CB91A7DEB29CFD48D50B5BC5E486A12F7E2,
	U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_CustomAttributesCacheGenerator_U3CDrownedU3Ed__21_System_Collections_IEnumerator_Reset_mCA29722363B203EF658DFC6D6D82610E82821BD9,
	U3CDrownedU3Ed__21_t6ED9305B9EC189E6896AFF441D1DA83193BED27C_CustomAttributesCacheGenerator_U3CDrownedU3Ed__21_System_Collections_IEnumerator_get_Current_m060EBCDCC94B24D967E22B5569D5BF7564394C0C,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
