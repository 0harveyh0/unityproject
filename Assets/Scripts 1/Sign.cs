using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sign : MonoBehaviour
{
    public GameObject dialogueBox;
    public Text dialogueText;
    public Text dialogue;
    public Text SecondDialogue;
    public bool playerinRange;
    private bool TwoText = true;
    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Text();
    }

    private void Text()
	{
        if (Input.GetKeyDown(KeyCode.Space) && playerinRange && TwoText == false)
        {
            if (dialogueBox.activeInHierarchy)
            {
                dialogueBox.SetActive(false);
            }

            else
            {
                dialogueBox.SetActive(true);
                dialogueText.text = dialogue;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Space) && playerinRange && TwoText == true)
		{
            if (dialogueBox.activeInHierarchy)
            {
                dialogueBox.SetActive(false);
            }
            else 
            {
                dialogueBox.SetActive(true);
                dialogueText.text = dialogue;
                if (Input.GetKeyDown(KeyCode.A))
                {
                    dialogueBox.SetActive(false);
                    dialogue = SecondDialogue;
                    dialogueBox.SetActive(true);

                }
            }
        }
            





        
    }



    private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
            playerinRange = true;
		}
	}

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            playerinRange = false;
            dialogueBox.SetActive(false);
        }
    }
}
