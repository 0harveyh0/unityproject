using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TwoTextDialogue : MonoBehaviour
{
    public GameObject dialogueBox;
    public GameObject SecDialogueBox;
    public Text dialogueText;
    public Text SecDialogueText;
    public Text dialogue;
    public Text SecondDialogue;
    public bool playerinRange;
    private bool dialogueBool;
 


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Text();
    }

    private void Text()
    {
        if (Input.GetKeyDown(KeyCode.Space) && playerinRange)
        {
            if (dialogueBox.activeInHierarchy)
            {
                dialogueBox.SetActive(false);
            }
            else if (SecDialogueBox.activeInHierarchy)
			{
                SecDialogueBox.SetActive(false);
			}
            else
            {
                dialogueBox.SetActive(true);
                dialogueText.text = dialogue;
                dialogueBool = true;
                

                while(dialogueBool)
                {
                    if (Input.GetKeyDown(KeyCode.KeypadEnter))
                    {
                        Debug.Log("Second Text");

                        dialogueBox.SetActive(false);
                        SecDialogueText.text = dialogue;
                        SecDialogueBox.SetActive(true);

                    }

                    dialogueBool = false;

                }
				

            }
        }
    }



    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            playerinRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            playerinRange = false;
            dialogueBox.SetActive(false);
        }
    }
}
