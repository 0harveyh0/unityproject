using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextManager : MonoBehaviour
{

    public Text[] texts; 
    public GameObject dialogueBox;

    public static TextManager instance;
    private Text activeBox = null;

    private int index = 0;

    // Start is called before the first frame update
    void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);



    }

	private void Update()
	{
        if (Input.GetKeyDown(KeyCode.Space))
        {
            index++;
            activeBox = texts[index];

        }

    }



}


