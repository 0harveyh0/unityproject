using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Timeline;


public class NPC : MonoBehaviour
{
    public bool playerinRange;
    public int health = 100;
    public float moveSpeed;
    public float chaseRadius;
    public float idleRadius;
    public Transform homePosition;
    public Transform target;
    private Rigidbody2D myRigidbody;

    void Start()
    {

        myRigidbody = GetComponent<Rigidbody2D>();
        target = GameObject.FindWithTag("Player").transform;
    }


    void FixedUpdate()
    {


        CheckDistance();

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && playerinRange)
        {
            
        }

    }

    void CheckDistance()
    {
        if (Vector3.Distance(target.position,
            transform.position) <= chaseRadius
            && Vector3.Distance(target.position,
            transform.position) > idleRadius)
        {
            Vector3 temp = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
            myRigidbody.MovePosition(temp);

        }
        else
        {
            Vector3 temp = Vector3.MoveTowards(transform.position, homePosition.position, moveSpeed * Time.deltaTime);
            myRigidbody.MovePosition(temp);

        }

    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Projectile"))
        {
            TakeDamage(2);
        }
        else if (other.CompareTag("Player"))
        {
            playerinRange = true;
        }
    }



    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Die();
        }
    }


    // Update is called once per frame
    void Die()
    {
        Destroy(gameObject);
    }
}
