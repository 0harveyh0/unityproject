using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testenemyscript : MonoBehaviour
{

    public int health;
    public float moveSpeed;
    public float chaseRadius;
    public float attackRadius;
    public Transform homePosition;
    public Transform target;
    public AudioSource soundEffect;
    private Rigidbody2D myRigidbody;



    void Start()
    {
        soundEffect = GetComponent<AudioSource>();
        myRigidbody = GetComponent<Rigidbody2D>();
        target = GameObject.FindWithTag("Player").transform;
    }


	void FixedUpdate()
	{
        CheckDistance();
	}



	void CheckDistance()
    {
        if (Vector3.Distance(target.position,
            transform.position) <= chaseRadius
            && Vector3.Distance(target.position,
            transform.position) > attackRadius)
        {
            Vector3 temp = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
            myRigidbody.MovePosition(temp);

        }
		else
		{
           Vector3 temp = Vector3.MoveTowards(transform.position ,homePosition.position, moveSpeed * Time.deltaTime);
            myRigidbody.MovePosition(temp);
        
        }

    }


    private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Projectile"))
		{
            TakeDamage(2);
            soundEffect.Play();
        }
	}
    
    
    
    public void TakeDamage (int damage)
	{
        health -= damage;

        if(health <= 0)
		{
            Die();
		}
	}


    // Update is called once per frame
    void Die()
    {
        
        Destroy(gameObject);
    }
}
