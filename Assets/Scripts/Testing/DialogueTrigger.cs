using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
	public bool playerinRange;
	public Dialogue dialogue;


	private void OnTriggerEnter2D(Collider2D collision)
	{

		if (collision.CompareTag("Player"))
		{
			playerinRange = true;
		}
	

	}


	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space) && playerinRange)
		{
			TriggerDialogue();
		}
	}

	public void TriggerDialogue()
	{
		FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
	}
}
