﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class PlayerHealth : Health
{
    [SerializeField] private FlashColor flash;
    public float health;
    public int damageTaken;
    public TextMeshProUGUI healthDisplay;



	void Start()
	{
        healthDisplay = GameObject.Find("Health").GetComponent<TextMeshProUGUI>();
	}


    void Update()
	{
        healthDisplay.text = health.ToString();
	}


	public override void Damage(int damage)
    {
        base.Damage(damage);
        if(currentHealth > 0)
        {
            if (flash)
            {
                flash.StartFlash();
            }
        }
    }

	void FixedUpdate()
	{
		
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Enemy"))
		{
            TakeDamage(damageTaken);
		}

	}


    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Die();
        }
    }


    // Update is called once per frame
    void Die()
    {
        SceneManager.LoadScene("death");
     
    }
}

