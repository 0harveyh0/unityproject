using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecretTree : MonoBehaviour
{

	public float health;



	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Projectile"))
		{
			TakeDamage(2);
		}
	}

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Die();
        }
    }


    // Update is called once per frame
    void Die()
    {
        Destroy(gameObject);
    }

}
