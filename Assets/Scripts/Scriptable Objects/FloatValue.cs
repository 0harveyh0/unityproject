﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Scriptable Objects/Values/Float Value", fileName ="New Float Value")]
public class FloatValue : ScriptableObject,ISerializationCallbackReceiver
{
    public float value;
    [SerializeField] public float defaultValue;
    [HideInInspector]
    public float RuntimeValue;
  
    public void OnAfterDeserialize()
	{
        RuntimeValue = defaultValue;
	}
    
    
    public void OnBeforeSerialize()
	{

	}

    private void OnEnable()
    {
        value = defaultValue;
    }
}
