using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public enum PlayerState
{
    walk,
    attack,
    interact,
    stagger,
    idle
}


public class PlayerMovement : Movement
{
    [SerializeField] private AnimatorController anim;
    [SerializeField] private StateMachine myState;
    [SerializeField] private float WeaponAttackDuration;
    [SerializeField] private ReceiveItem myItem;
    [SerializeField] private GenericAbility currentAbility;
    
    public SignalSender playerHealthSignal;
    public FloatValue currentHealth;
    private Vector2 tempMovement = Vector2.down;
    private Vector2 facingDirection = Vector2.down;
    public GameObject projectile;
    public PlayerState currentState;
    public Animator animator;


	// Start is called before the first frame update
	void Start()
    {
        myState.ChangeState(GenericState.idle);

    }

    // Update is called once per frame
    void Update()
    {
 



        if (myState.myState == GenericState.receiveItem)
        {
            if (Input.GetButtonDown("Check"))
            {
                myState.ChangeState(GenericState.idle);
                anim.SetAnimParameter("receiveItem", false);
                myItem.ChangeSpriteState();
            }
            return;
        }
        if (!isRestrictedState(myState.myState))
        {
            GetInput();
            SetAnimation();
        }


        if(Input.GetButtonDown("Second Weapon") && currentState != PlayerState.attack && currentState != PlayerState.stagger)
		{

		}

    }

    bool isRestrictedState(GenericState currentState)
    {
        if (currentState == GenericState.attack || currentState == GenericState.ability)
        {
            return true;
        }
        return false;
    }



    void SetState(GenericState newState)
    {
        myState.ChangeState(newState);
    }


    public void Knock(float knockTime, float damage)
    {
        currentHealth.RuntimeValue -= damage;
        playerHealthSignal.Raise();

        if (currentHealth.RuntimeValue > 0)
		{
            StartCoroutine(Knockco(knockTime));
		}
		else
		{
            this.gameObject.SetActive(false);
		}
    }


    void GetInput()
    {
        if (Input.GetButtonDown("Weapon Attack") && currentState != PlayerState.attack && currentState != PlayerState.stagger)
        {
            StartCoroutine(WeaponCo());
            tempMovement = Vector2.zero;
            Motion(tempMovement);
        }
        else if (Input.GetButtonDown("Ability"))
        {
            if (currentAbility)
            {
                StartCoroutine(AbilityCo(currentAbility.duration));
            }
        }
        else if (Input.GetButtonDown("Second Weapon") && currentState != PlayerState.attack && currentState != PlayerState.stagger)
		{
            StartCoroutine(SecondAttackCo());
		}

        else if (myState.myState != GenericState.attack)
        {
            tempMovement.x = Input.GetAxisRaw("Horizontal");
            tempMovement.y = Input.GetAxisRaw("Vertical");
            Motion(tempMovement);
        }
        else
        {
            tempMovement = Vector2.zero;
            Motion(tempMovement);
        }
    }

    void SetAnimation()
    {
        if (tempMovement.magnitude > 0)
        {
            anim.SetAnimParameter("moveX", Mathf.Round(tempMovement.x));
            anim.SetAnimParameter("moveY", Mathf.Round(tempMovement.y));
            anim.SetAnimParameter("moving", true);
            SetState(GenericState.walk);
            facingDirection = tempMovement;
        }
        else
        {
            anim.SetAnimParameter("moving", false);
            if (myState.myState != GenericState.attack)
            {
                SetState(GenericState.idle);
            }
        }
    }

    public IEnumerator WeaponCo()
    {
        myState.ChangeState(GenericState.attack);
        anim.SetAnimParameter("attacking", true);
        yield return new WaitForSeconds(WeaponAttackDuration);
        myState.ChangeState(GenericState.idle);
        anim.SetAnimParameter("attacking", false);
    }
    public IEnumerator AbilityCo(float abilityDuration)
    {
        myState.ChangeState(GenericState.ability);
        currentAbility.Ability(transform.position, facingDirection, anim.anim, myRigidbody);
        yield return new WaitForSeconds(abilityDuration);
        myState.ChangeState(GenericState.idle);
    }
    private IEnumerator Knockco(float knockTime)
    {
        if (myRigidbody != null)
        {
            yield return new WaitForSeconds(knockTime);
            myRigidbody.velocity = Vector2.zero;
            currentState = PlayerState.idle;
            myRigidbody.velocity = Vector2.zero;


        }
    }
    private IEnumerator SecondAttackCo()
	{
        //animator.SetBool("attacking", true);
        currentState = PlayerState.attack;
        yield return null;
        MakeArrow();
        //animator.SetBool("attacking", false);
        yield return new WaitForSeconds(.3f);
        if(currentState != PlayerState.interact)
		{
            currentState = PlayerState.walk;
		}
	}

    private void MakeArrow()
	{
        Vector2 temp = new Vector2(animator.GetFloat("moveX"), animator.GetFloat("moveY"));
        Arrow arrow = Instantiate(projectile, transform.position, Quaternion.identity).GetComponent<Arrow>();
        arrow.Setup(temp, ChooseArrowDirection());

	}
    Vector3 ChooseArrowDirection()
	{
        float temp = Mathf.Atan2(animator.GetFloat("moveY"), animator.GetFloat("moveX")) * Mathf.Rad2Deg;
        return new Vector3(0, 0, temp);
	}


}
